import os

__author__ = 'Adam'
#from abc import ABCMeta
#from abc import abstractmethod
import numpy as np
import dicom
import time
import weakref
import math
import random

class Database_4D(object):
    """Abstract base class for 4D database objects"""
   # __metaclass__ = ABCMeta

    def __init__(self, file_path, frames_3d_count):
        super(Database_4D, self).__init__()
        self.file_path = file_path
        self.frames_3d_count = frames_3d_count
        #self.weakValueDict = weakref.WeakValueDictionary()
        self.weakValueDict = {} # normal dictionary
        self.curent_database_3D = None
        self.curent_database_3D_index = 0
        self.frames3DnamesList = [] # list with names of 3d bases (frames of video 3D)

    def get_frame(self, index):
        '''return frame from memory, if frame isn't in it, it's being loaded'''
        # Frame cacheing with weak references
        if (index >= self.frames_3d_count) or (index < 0):
            raise Exception('frame3D index out of bounds exception')
        if self.weakValueDict.has_key(index): # if has key check if value is not None
            f = self.weakValueDict[index]
            if f is not None and f() is not None :
                self.curent_database_3D_index = index
                fr = f()
                self.curent_database_3D = fr
                return fr # return cashed database frame
        # read frame from file
        frame = self.read_frame(index)
        self.add_frame_to_dict(frame, index) # add new frame to infinite size dictionary
        self.curent_database_3D_index = index
        return frame # strong reference prevent from delete


    def add_frame_to_dict(self, newFrame, index):
        ''' add frame to dictionary and if memory is insuficient size remove not used bases
        :param newFrame: frame to bee add
        '''
        if len(self.weakValueDict)>3: # if more than 4 bases remove one of it (because weak references not working in python)
            while True:
                frameIndexToDelete = random.choice(list(self.weakValueDict.keys()))
                if frameIndexToDelete != self.curent_database_3D_index:
                    del self.weakValueDict[frameIndexToDelete]
                    break
        self.weakValueDict[index] = weakref.ref(newFrame, self.callback)
        self.curent_database_3D = newFrame;

    def callback(self, reference):
        """Invoked when referenced object is deleted"""
        # print 'deleted frame'
        pass

    def get_frames_3d_count(self):
        return self.frames_3d_count

    #@abstractmethod
    def read_frame(self, frame_index):
        pass

    #@abstractmethod
    def get_x_size(self):
        pass

    #@abstractmethod
    def get_y_size(self):
        pass

    #@abstractmethod
    def get_z_size(self):
        pass

    #@abstractmethod
    def get_bits_per_voxel(self):
        pass

    #@abstractmethod
    def get_pixelSpacing_x(self):
        pass

    #@abstractmethod
    def get_pixelSpacing_y(self):
        pass

    #@abstractmethod
    def get_pixelSpacing_z(self):
        pass


class Frame3D(object):

    def __init__(self, fr):
         super(Frame3D, self).__init__()
         self.fr = fr


class DICOM_4D(Database_4D):

    def __init__(self, file_path1, frames_3d_count):
        super(DICOM_4D, self).__init__(file_path1, frames_3d_count)
        ds = dicom.read_file(self.file_path)    # save dicom filenames list from dicomdir
        self.path = self.file_path[:-8] # end is cut
        self.dicom_dir_directories_list = []
        dicomFrame3DFileNames = []
        currentFolderName = ""
        for record in ds.DirectoryRecordSequence:
            if record.DirectoryRecordType == "IMAGE":
                extractedFilePath = record.ReferencedFileID
                if isinstance(extractedFilePath, basestring): #only one frame 3D (data files in dicomdir folder)
                    dicomFrame3DFileNames.append(extractedFilePath)
                else: #when more 3D bases per file (packed in folders)
                    if currentFolderName == "":
                        currentFolderName = extractedFilePath[0]
                    if currentFolderName != extractedFilePath[0]:
                        # when wee have next folder
                        if dicomFrame3DFileNames: # if list not empty
                            #add names set to list
                            self.dicom_dir_directories_list.append(dicomFrame3DFileNames)
                            self.frames3DnamesList.append(currentFolderName)
                            dicomFrame3DFileNames = []
                        currentFolderName = extractedFilePath[0]
                    #add files to fileset
                    dicomFrame3DFileNames.append(extractedFilePath[1])
        if dicomFrame3DFileNames: #when list not empty
            self.dicom_dir_directories_list.append(dicomFrame3DFileNames) #add last file names set to frames 3D list
            self.frames3DnamesList.append(currentFolderName)


                # self.dicom_dir_directories_list.append(record.ReferencedFileID.lower())
        # check image measurements and amount of 2D images in 3D frame
        self.frames_3d_count = len(self.dicom_dir_directories_list)
        self.images_2d_count = len(self.dicom_dir_directories_list[0])
        #must read on image 2d because need to extract data from existing image
        dcm = dicom.read_file(self.path+self.frames3DnamesList[0]+self.path[-1]+self.dicom_dir_directories_list[0][0])
        self.image_x = dcm.Rows
        self.image_y = dcm.Columns
        self.image_Z = self.images_2d_count
        self.bits_per_voxel = dcm.BitsStored
        self.bits_allocated = dcm.BitsAllocated
        if self.bits_per_voxel not in [8, 12, 16]:
            raise Exception('unsupported '+self.bits_per_voxel+' bits per voxel images')
        (x, y) = dcm.PixelSpacing
        self.pixel_spacing_x = x
        self.pixel_spacing_y = y
        print self.pixel_spacing_x
        print self.pixel_spacing_y

    def read_frame(self, frame_index):
        """Read and return data from file as numpy ndarray 3D.
        """
        image_list = []
        start_time = time.time()
        if self.bits_per_voxel == 8:
           for file_path1 in self.dicom_dir_directories_list[frame_index]: # poprawic
               for file_path1 in self.dicom_dir_directories_list:
                dcm = dicom.read_file(self.path+file_path1)
                numpy_array_data = dcm.pixel_array # read data using pyDicom
                # numpy_array_data.dtype = np.uint8 #cast ndarray content ndarray to single bytes
                image_list.append(numpy_array_data)
        if self.bits_per_voxel == 12: #poprawic
            if self.bits_allocated == 16:# if data on 12 bits
                shift = self.bits_allocated - self.bits_stored # calculate shift
                shift = math.pow(2, shift) # two to power shift
                dcm = dicom.read_file(self.path+file_path1)
                numpy_array_data = dcm.pixel_array
                numpy_array_data = numpy_array_data.reshape(-1)
                numpy_array_data /= shift  # divide by shift
                numpy_array_data.dtype = np.uint8
                numpy_array_data = numpy_array_data[::2]
                image_list.append(numpy_array_data)
            elif self.bits_allocated == 12:#if data on 16 bits
                for file_path1 in self.dicom_dir_directories_list[frame_index]:
                    dcm = dicom.read_file(self.path+file_path1)
                    numpy_array_data1 = dcm.pixel_array .reshape(-1)
                    numpy_array_data2 = np.empty_like(numpy_array_data1) #allocate space for new copy of data array
                    numpy_array_data2[:] = numpy_array_data1   #deep copy of data array
                    numpy_array_data1 = numpy_array_data1[2::3] #get evry third byte of data start on 3 byte
                    # numpy_array_data2 /= 16
                    numpy_array_data2 = numpy_array_data1[::3] #get evry third byte of data start on 0 byte
                    numpy_array_data = np.vstack((numpy_array_data1, numpy_array_data2)).T # combine data bytes into one table
                    numpy_array_data = numpy_array_data.reshape(-1) # make one dimensional array
                    numpy_array_data.dtype = np.uint8 # set type
                    image_list.append(numpy_array_data)
        if self.bits_per_voxel == 16:
            folderName = self.frames3DnamesList[frame_index]
            for file_path1 in self.dicom_dir_directories_list[frame_index]:
                dcm = dicom.read_file(self.path+folderName+self.path[-1]+file_path1)
                numpy_array_data = dcm.pixel_array # get data retrieved by pyDicom
                # cut data to get 1 byte per voxel and save data in previously reserved place
                # numpy_array_data = numpy_array_data.reshape(-1) # conversion needs 1D array (from 2D)
                # numpy_array_data /= 16  # division of every element by 16
                # numpy_array_data.dtype = np.uint8 # project ndarray onto bytes
                # numpy_array_data = numpy_array_data[::2] # select every second element (of byte)
                image_list.append(numpy_array_data)

        # check and if needed align arrays to correct 4 bytes format adding missing data
        if self.image_x%4 !=0 or self.image_y%4 !=0:
            self.image_x = (self.image_x+3)/4*4
            self.image_y = (self.image_y+3)/4*4
            self.image_Z = (self.image_Z+3)/4*4
            numpy_array_3D = np.zeros((self.image_z, self.image_y, self.image_x), dtype=image_list[0].dtype)#allocate all needed memory
            for i in range(0, len(image_list)):
                pass # dorobic te opcje
        else:
            if len(image_list)%4 != 0 : # if images list is not adjusted to 4 bytes, add missing empty images
                self.image_Z = (self.image_Z+3)/4*4
                additionalImagesCCount = self.image_Z - len(image_list)
                for i in range(0, additionalImagesCCount):
                    image_list.append(np.zeros(image_list[0].shape, dtype=image_list[0].dtype)) # add empty miages the same format and size
            # concatenate all tables in one 3D array
            numpy_array_3D = np.concatenate(image_list, axis=0)

        # numpy_array_3D = numpy_array_3D.reshape(-1) #conversion need 1D array (change from 2D)
        numpy_array_3D.dtype = np.uint16 # cast to bytes
        numpy_array_3D = numpy_array_3D.reshape((self.get_z_size(), self.get_y_size(), self.get_x_size()))
        durationTime = (time.time()-start_time)
        print durationTime # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        return  numpy_array_3D # return final database 3D

    def get_x_size(self):
        return self.image_x

    def get_y_size(self):
        return self.image_y

    def get_z_size(self):
        return self.image_Z

    def get_3D_frames_count(self):
        return self.frames_3d_count

    def get_bits_per_voxel(self):
        return self.bits_per_voxel

    def get_pixelSpacing_x(self):
        return self.pixel_spacing_x

    def get_pixelSpacing_y(self):
        return self.pixel_spacing_y


class RAW_4D(Database_4D):

    def __init__(self, file_path1, frames_3d_count, params):
        super(RAW_4D, self).__init__(file_path1, frames_3d_count)
        self.image_x = params["x_size"]
        self.image_y = params["y_size"]
        self.image_Z = params["z_size"]
        self.bits_per_voxel = params["bits_per_voxel"]
        self.pixel_spacing_x = params["x_size"]
        self.pixel_spacing_y = params["y_size"]

        #os.path.dirname(path)

        #fileName = os.path.basename(fullFileName)

        if type(self.file_path) is list:
            for entry in self.file_path:
                self.frames3DnamesList.append(os.path.basename(entry))
            self.path = os.path.dirname(self.file_path[0])
            self.frames_3d_count = len(self.frames3DnamesList)
        else:
            self.frames3DnamesList.append(os.path.basename(self.file_path))
            self.path = os.path.dirname(self.file_path)

    def read_frame(self, frame_index):
        """
        Read and return data from whole 3D frame with index.
        Data is returned as ndarray in 3D
        """
        start_time = time.time()

        if self.bits_per_voxel == 8:
            # EXPERIMENTAL! WASN'T TESTED!!!
            dt = np.uint8
            frame_path = os.path.join(self.path, self.frames3DnamesList[frame_index])
            ds = np.fromfile(frame_path, dtype=dt)
            ds2 = np.array(ds, dtype=np.uint16)
            ds2 *= 256
            numpy_array_3D = ds2.copy().reshape((self.get_z_size(), self.get_y_size(), self.get_x_size()))

        elif self.bits_per_voxel == 16:
            dt = np.uint16
            frame_path = os.path.join(self.path, self.frames3DnamesList[frame_index])
            ds = np.fromfile(frame_path, dtype=dt)
            numpy_array_3D = ds.copy()
            # numpy_array_3D /= 16
            # numpy_array_3D = numpy_array_3D.reshape(-1)
            # numpy_array_3D.dtype = np.uint8
            # numpy_array_3D = numpy_array_3D[::2].reshape((self.get_z_size(), self.get_y_size(), self.get_x_size()))
            numpy_array_3D = numpy_array_3D.reshape((self.get_z_size(), self.get_y_size(), self.get_x_size()))

        durationTime = (time.time()-start_time)
        print durationTime
        return  numpy_array_3D # zwracamy gotowa baze 3D

    def get_x_size(self):
        return self.image_x

    def get_y_size(self):
        return self.image_y

    def get_z_size(self):
        return self.image_Z

    def get_3D_frames_count(self):
        return self.frames_3d_count

    def get_bits_per_voxel(self):
        return self.bits_per_voxel

    def get_pixelSpacing_x(self):
        return self.pixel_spacing_x

    def get_pixelSpacing_y(self):
        return self.pixel_spacing_y


