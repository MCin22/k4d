from DataProcessing import MipAlgorithm, SliceAlgorithm, SegmentationAlgorithm, VesselCreator

__author__ = 'MCin22 && Adam'
from RAWdialog import Ui_DialogMultipleRAWLoader
from DataController import CoreAppController, PropertyBinder

import sys
from PySide import QtGui, QtCore
from PySide.QtGui import QMessageBox
from UIWrappers import PropertyLoadFile, PropertySlider, PropertyCheckBox, PropertySpinBox, PropertyDoubleSpinBox, ActionQAction, ActionButton, PropertyQTree, SegmentationPanel, MipPanel, SlicePanel, \
    PropertyQLine, PropertyQList, PropertySpinBoxXYZ, PropertySliderXYZ, \
    PropertyLabel, PropertyRawFiles, PropertyQListCalcifs, PropertyExportButton, PropertyPointSpinBox, \
    PropertyPointPresenter, PanelHolder


class Application(QtGui.QWidget, CoreAppController, PropertyBinder):
    '''class adding UI to existing logic'''
    def __init__(self):
        super(Application, self).__init__()
        super(CoreAppController, self).__init__()
        PropertyBinder.__init__(self)
        self.mainWindow = QtGui.QMainWindow()
        self.mipPanel = None
        self.slicePanel = None
        self.segmentationPanel = None
        self.tabWidget = None
        self.add_property_handler(self.on_controlKeyPressed_update_ui,"controlKeyPressed")
        self.add_property_handler(self.on_mainMenuTabIndex_update_ui,"mainMenuTabIndex")
        self.add_property_handler(self.on_application_update_ui,"application")


    def setup_app(self):
        super(Application, self).setup_app()
        self.add_property_binder("controlKeyPressed", self)
        self.add_property_binder("mainMenuTabIndex", self)
        self.add_property_binder("application", self)
        self.on_get_value_from_UI(self,"application")

    def on_mainMenuTabIndex_update_ui(self, newValueForUI, propertyController):
        if newValueForUI != None:
            self.tabWidget.setCurrentIndex(newValueForUI)
            if newValueForUI < 3:
                self.panelHoder.set_active_panel_index(newValueForUI)

    def on_controlKeyPressed_update_ui(self, newValueForUI, propertyController):
        pass

    def on_application_update_ui(self, newValueForUI, propertyController):
        pass

    def on_create_UI(self):
        self.setupUi(self.mainWindow)
        pass

    def keyPressEvent(self, event):
        if (event.type() != QtCore.QEvent.KeyPress): return
        key = event.key()
        if key == QtCore.Qt.Key_Control:
            self.on_get_value_from_UI(True,"controlKeyPressed")

    def keyReleaseEvent(self, event):
        if (event.type() != QtCore.QEvent.KeyRelease): return
        key = event.key()
        if key == QtCore.Qt.Key_Control:
            self.on_get_value_from_UI(False,"controlKeyPressed")

    def setupUi(self, MainWindow):
        MainWindow.resize(800, 620)



        self.tabWidget = QtGui.QTabWidget(self)
        self.tabWidget.setMinimumWidth(300)
        self.tabWidget.setMaximumSize(QtCore.QSize(300, 16777215))
        self.tabWidget.setTabPosition(QtGui.QTabWidget.West)
        self.tabWidget.setTabShape(QtGui.QTabWidget.Triangular)

        self.mip_tab = QtGui.QWidget()
        # self.mip_selection_point_type_checkBox = PropertyCheckBox(self.mip_tab)
        # self.mip_selection_point_type_checkBox.setGeometry(QtCore.QRect(10, 50, 261, 21))
        # self.add_property_binder("mipSelectionPointType", self.mip_selection_point_type_checkBox)
        self.remove_background_button = ActionButton(self.mip_tab)
        self.remove_background_button.setGeometry(QtCore.QRect(10, 20, 261, 28))
        self.add_action_binder("selectMipBackgroundToRemove", self.remove_background_button)
        self.tabWidget.addTab(self.mip_tab, "MIP")

        self.slices_tab = QtGui.QWidget()
        self.slices_X_label = QtGui.QLabel(self.slices_tab)
        self.slices_X_label.setGeometry(QtCore.QRect(10, 20, 31, 21))
        self.slices_X_spinBox = PropertySpinBoxXYZ(self.slices_tab,2)
        self.slices_X_spinBox.setGeometry(QtCore.QRect(160, 20, 111, 22))
        self.slices_X_spinBox.setRange(0, 500)
        self.add_property_binder("imageSeedPoint", self.slices_X_spinBox)
        self.x_slider = PropertySliderXYZ(self.slices_tab,2)
        self.x_slider.setGeometry(QtCore.QRect(10, 50, 261, 22))
        self.x_slider.setOrientation(QtCore.Qt.Horizontal)
        self.x_slider.setRange(0, 500)
        self.add_property_binder("imageSeedPoint", self.x_slider)

        self.slices_Y_label = QtGui.QLabel(self.slices_tab)
        self.slices_Y_label.setGeometry(QtCore.QRect(10, 80, 31, 21))
        self.slices_Y_spinBox = PropertySpinBoxXYZ(self.slices_tab,1)
        self.slices_Y_spinBox.setGeometry(QtCore.QRect(160, 80, 111, 22))
        self.slices_Y_spinBox.setRange(0, 500)
        self.add_property_binder("imageSeedPoint", self.slices_Y_spinBox)
        self.y_slider = PropertySliderXYZ(self.slices_tab,1)
        self.y_slider.setGeometry(QtCore.QRect(10, 110, 261, 22))
        self.y_slider.setOrientation(QtCore.Qt.Horizontal)
        self.y_slider.setRange(0, 500)
        self.add_property_binder("imageSeedPoint", self.y_slider)

        self.slices_Z_label = QtGui.QLabel(self.slices_tab)
        self.slices_Z_label.setGeometry(QtCore.QRect(10, 140, 31, 21))
        self.slices_Z_spinBox = PropertySpinBoxXYZ(self.slices_tab,0)
        self.slices_Z_spinBox.setGeometry(QtCore.QRect(160, 140, 111, 22))
        self.slices_Z_spinBox.setRange(0, 500)
        self.add_property_binder("imageSeedPoint", self.slices_Z_spinBox)
        self.z_slider = PropertySliderXYZ(self.slices_tab,0)
        self.z_slider.setGeometry(QtCore.QRect(10, 170, 261, 22))
        self.z_slider.setOrientation(QtCore.Qt.Horizontal)
        self.z_slider.setRange(0, 500)
        self.add_property_binder("imageSeedPoint", self.z_slider)

        self.slices_valueXYZ_label = QtGui.QLabel(self.slices_tab)
        self.slices_valueXYZ_label.setGeometry(QtCore.QRect(10, 210, 53, 16))
        self.slices_valueXYZ_lineEdit = PropertyQLine(self.slices_tab)
        self.add_property_binder("sliceMousePointerCurrentValue", self.slices_valueXYZ_lineEdit)
        self.slices_valueXYZ_lineEdit.setEnabled(False)
        self.slices_valueXYZ_lineEdit.setGeometry(QtCore.QRect(160, 210, 111, 22))
        self.slices_HU_XYZ_label = QtGui.QLabel(self.slices_tab)
        self.slices_HU_XYZ_label.setGeometry(QtCore.QRect(10, 240, 53, 16))
        self.slices_HU_XYZ_lineEdit = PropertyQLine(self.slices_tab)
        self.slices_HU_XYZ_lineEdit.setEnabled(False)
        self.slices_HU_XYZ_lineEdit.setGeometry(QtCore.QRect(160, 240, 111, 22))

        self.slices_display_visited_voxels_checkBox = PropertyCheckBox(self.slices_tab)
        self.slices_display_visited_voxels_checkBox.setGeometry(QtCore.QRect(10, 270, 261, 21))
        self.add_property_binder("displayVisitedVoxels", self.slices_display_visited_voxels_checkBox)

        self.slices_display_prev_segmentation_checkBox = PropertyCheckBox(self.slices_tab)
        self.slices_display_prev_segmentation_checkBox.setGeometry(QtCore.QRect(10, 300, 261, 21))
        self.add_property_binder("displayPrevSegmentation", self.slices_display_prev_segmentation_checkBox)

        self.slices_display_next_segmentation_checkBox = PropertyCheckBox(self.slices_tab)
        self.slices_display_next_segmentation_checkBox.setGeometry(QtCore.QRect(10, 330, 261, 21))
        self.add_property_binder("displayNextSegmentation", self.slices_display_next_segmentation_checkBox)

        self.slices_display_reference_segmentation_checkBox = PropertyCheckBox(self.slices_tab)
        self.slices_display_reference_segmentation_checkBox.setGeometry(QtCore.QRect(10, 360, 261, 21))
        self.add_property_binder("displayReferenceSegmentation", self.slices_display_reference_segmentation_checkBox)


        self.tabWidget.addTab(self.slices_tab, "Slices")
        self.results_tab = QtGui.QWidget()
        self.results_change_vessel_name_button = ActionButton(self.results_tab)
        self.results_change_vessel_name_button.clicked.connect(self.results_change_vessel_name_button_clicked)
        self.results_change_vessel_name_button.setGeometry(QtCore.QRect(10, 205, 101, 24))
        self.results_invert_points_button = ActionButton(self.results_tab)
        self.add_action_binder("invertPoints", self.results_invert_points_button)
        self.results_invert_points_button.setGeometry(QtCore.QRect(10, 170, 101, 24))
        self.results_merge_button = ActionButton(self.results_tab)
        self.add_action_binder("mergeVessels", self.results_merge_button)
        self.results_merge_button.setGeometry(QtCore.QRect(10, 145, 101, 24))
        self.results_cut_button = ActionButton(self.results_tab)
        self.add_action_binder("cutVessel", self.results_cut_button)
        self.results_cut_button.setGeometry(QtCore.QRect(10, 120, 101, 24))
        self.results_remove_button = ActionButton(self.results_tab)
        self.add_action_binder("removeSelectedVessels", self.results_remove_button)
        self.results_remove_button.setGeometry(QtCore.QRect(10, 95, 101, 24))
        self.results_inverse_button = ActionButton(self.results_tab)
        self.add_action_binder("inverseSelectionVessels", self.results_inverse_button)
        self.results_inverse_button.setGeometry(QtCore.QRect(10, 70, 101, 24))
        self.results_deselect_all_button = ActionButton(self.results_tab)
        self.add_action_binder("deselectAllVessels", self.results_deselect_all_button)
        self.results_deselect_all_button.setGeometry(QtCore.QRect(10, 45, 101, 24))
        self.results_select_all_button = ActionButton(self.results_tab)
        self.add_action_binder("selectAllVessels", self.results_select_all_button)
        self.results_select_all_button.setGeometry(QtCore.QRect(10, 20, 101, 24))
        self.results_treeWidget = PropertyQTree(self.results_tab)
        self.add_property_binder("currentFrameIndex",self.results_treeWidget)
        self.add_property_binder("segmentationResult", self.results_treeWidget)
        self.add_property_binder("vesselName", self.results_treeWidget)

        self.results_treeWidget.setGeometry(QtCore.QRect(120, 20, 151, 177))
        self.results_treeWidget.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.results_treeWidget.setDragDropMode(QtGui.QAbstractItemView.DragDrop)
        self.results_treeWidget.setDefaultDropAction(QtCore.Qt.MoveAction)
        self.results_treeWidget.setHeaderHidden(True)

        self.results_current_base_lineEdit = PropertyQLine(self.results_tab)
        self.results_current_base_lineEdit.setGeometry(QtCore.QRect(120, 206, 151, 22))
        self.add_property_binder("vesselName", self.results_current_base_lineEdit)

        self.results_display_points_checkBox = PropertyCheckBox(self.results_tab)
        self.results_display_points_checkBox.setGeometry(QtCore.QRect(10, 240, 151, 21))
        self.add_property_binder("displayResultPoints", self.results_display_points_checkBox)

        self.results_display_visited_voxels_checkBox = PropertyCheckBox(self.results_tab)
        self.results_display_visited_voxels_checkBox.setGeometry(QtCore.QRect(10, 270, 261, 21))
        self.add_property_binder("displayVisitedVoxels", self.results_display_visited_voxels_checkBox)

        self.results_vessel_point_label = QtGui.QLabel(self.results_tab)
        self.results_vessel_point_label.setGeometry(QtCore.QRect(139, 240, 261, 21))

        self.results_vessel_point_spinner = PropertyPointSpinBox(self.results_tab)
        self.results_vessel_point_spinner.setGeometry(QtCore.QRect(195, 240, 77, 21))
        self.add_property_binder("activeSegmentationVessel", self.results_vessel_point_spinner)
        self.add_property_binder("currentVesselPointIndex", self.results_vessel_point_spinner)

        self.results_display_prev_segmentation_checkBox = PropertyCheckBox(self.results_tab)
        self.results_display_prev_segmentation_checkBox.setGeometry(QtCore.QRect(10, 300, 261, 21))
        self.add_property_binder("displayPrevSegmentation", self.results_display_prev_segmentation_checkBox)

        self.results_display_next_segmentation_checkBox = PropertyCheckBox(self.results_tab)
        self.results_display_next_segmentation_checkBox.setGeometry(QtCore.QRect(10, 330, 261, 21))
        self.add_property_binder("displayNextSegmentation", self.results_display_next_segmentation_checkBox)

        self.results_display_reference_segmentation_checkBox = PropertyCheckBox(self.results_tab)
        self.results_display_reference_segmentation_checkBox.setGeometry(QtCore.QRect(10, 360, 261, 21))
        self.add_property_binder("displayReferenceSegmentation", self.results_display_reference_segmentation_checkBox)

        self.results_line2 = QtGui.QFrame(self.results_tab)
        self.results_line2.setGeometry(QtCore.QRect(10, 385, 261, 16))
        self.results_line2.setFrameShape(QtGui.QFrame.HLine)
        self.results_line2.setFrameShadow(QtGui.QFrame.Sunken)

        self.results_point_params = PropertyPointPresenter(self.results_tab)
        self.results_point_params.setGeometry(QtCore.QRect(10, 400, 261, 120))
        self.add_property_binder("segmentationSelectedVeselPoint", self.results_point_params)

        self.results_calcif_listView = PropertyQListCalcifs(self.results_tab)
        self.results_calcif_listView.setGeometry(QtCore.QRect(10, 520, 261, 82))
        self.add_property_binder("currentFrameIndex", self.results_calcif_listView)
        self.add_property_binder("segmentationResult", self.results_calcif_listView)

        self.results_calcif_list_label = QtGui.QLabel(self.results_tab)
        self.results_calcif_list_label.setGeometry(QtCore.QRect(10, 490, 101, 28))
        self.results_line = QtGui.QFrame(self.results_tab)
        self.results_line.setGeometry(QtCore.QRect(10, 480, 261, 16))
        self.results_line.setFrameShape(QtGui.QFrame.HLine)
        self.results_line.setFrameShadow(QtGui.QFrame.Sunken)

        self.results_show_calcifs = PropertyCheckBox(self.results_tab)
        self.results_show_calcifs.setGeometry(QtCore.QRect(160, 493, 261, 21))
        self.add_property_binder("showCalcifs", self.results_show_calcifs)

        self.results_line3 = QtGui.QFrame(self.results_tab)
        self.results_line3.setGeometry(QtCore.QRect(10, 605, 261, 16))
        self.results_line3.setFrameShape(QtGui.QFrame.HLine)
        self.results_line3.setFrameShadow(QtGui.QFrame.Sunken)

        self.results_export_binary_data_button = PropertyExportButton(self.results_tab, "Export segmentation as binary data")
        self.add_property_binder("exportBinarySegmentationFilePath", self.results_export_binary_data_button)
        self.results_export_binary_data_button.setGeometry(QtCore.QRect(10, 620, 131, 24))

        self.results_export_text_data_button = PropertyExportButton(self.results_tab, "Export segmentation as text data")
        self.add_property_binder("exportTextSegmentationFilePath", self.results_export_text_data_button)
        self.results_export_text_data_button.setGeometry(QtCore.QRect(141, 620, 131, 24))

        self.tabWidget.addTab(self.results_tab, "Results")

        self.params_tab = QtGui.QWidget()
        self.params_step_factor_label = QtGui.QLabel(self.params_tab)
        self.params_step_factor_label.setGeometry(QtCore.QRect(10, 165, 336, 22))
        self.params_adapt_vessel_level_checkBox = PropertyCheckBox(self.params_tab)
        self.params_adapt_vessel_level_checkBox.setGeometry(QtCore.QRect(10, 220, 261, 20))
        self.add_property_binder("adapting_vessel_level", self.params_adapt_vessel_level_checkBox)
        self.params_vessel_level_label = QtGui.QLabel(self.params_tab)
        self.params_vessel_level_label.setGeometry(QtCore.QRect(10, 20, 336, 22))
        self.params_adapt_bg_level_checkBox = PropertyCheckBox(self.params_tab)
        self.params_adapt_bg_level_checkBox.setGeometry(QtCore.QRect(10, 250, 261, 20))
        self.add_property_binder("adapting_bg_level", self.params_adapt_bg_level_checkBox)
        self.params_remove_calcification_checkBox = PropertyCheckBox(self.params_tab)
        self.params_remove_calcification_checkBox.setGeometry(QtCore.QRect(10, 310, 261, 20))
        self.add_property_binder("calcification_removing", self.params_remove_calcification_checkBox)
        self.params_window_size_label = QtGui.QLabel(self.params_tab)
        self.params_window_size_label.setGeometry(QtCore.QRect(10, 136, 336, 22))
        self.params_min_vessel_level_label = QtGui.QLabel(self.params_tab)
        self.params_min_vessel_level_label.setGeometry(QtCore.QRect(10, 78, 336, 22))
        self.params_max_vessel_level_label = QtGui.QLabel(self.params_tab)
        self.params_max_vessel_level_label.setGeometry(QtCore.QRect(10, 107, 336, 22))
        self.params_BG_level_label = QtGui.QLabel(self.params_tab)
        self.params_BG_level_label.setGeometry(QtCore.QRect(10, 49, 336, 22))
        self.params_adapt_window_size_checkBox = PropertyCheckBox(self.params_tab)
        self.params_adapt_window_size_checkBox.setGeometry(QtCore.QRect(10, 280, 261, 20))
        self.add_property_binder("adapting_window_size", self.params_adapt_window_size_checkBox)
        self.params_BG_level_spinBox = PropertySpinBox(self.params_tab)
        self.params_BG_level_spinBox.setGeometry(QtCore.QRect(160, 50, 111, 22))
        self.params_BG_level_spinBox.setRange(0, 4000)
        self.add_property_binder("initial_bg_level", self.params_BG_level_spinBox)
        self.params_max_vessel_level_spinBox = PropertySpinBox(self.params_tab)
        self.params_max_vessel_level_spinBox.setGeometry(QtCore.QRect(160, 110, 111, 22))
        self.params_max_vessel_level_spinBox.setRange(0, 4000)
        self.add_property_binder("max_vessel_level", self.params_max_vessel_level_spinBox)
        self.params_window_size_spinBox = PropertySpinBox(self.params_tab)
        self.params_window_size_spinBox.setGeometry(QtCore.QRect(160, 140, 111, 22))
        self.params_window_size_spinBox.setRange(1, 12)
        self.add_property_binder("initial_mask_size", self.params_window_size_spinBox)
        self.params_vessel_level_spinBox = PropertySpinBox(self.params_tab)
        self.params_vessel_level_spinBox.setGeometry(QtCore.QRect(160, 20, 111, 22))
        self.params_vessel_level_spinBox.setRange(0, 4000)
        self.add_property_binder("initial_vessel_level", self.params_vessel_level_spinBox)
        self.params_min_vessel_level_spinBox = PropertySpinBox(self.params_tab)
        self.params_min_vessel_level_spinBox.setGeometry(QtCore.QRect(160, 80, 111, 22))
        self.params_min_vessel_level_spinBox.setRange(0, 4000)
        self.add_property_binder("min_vessel_level", self.params_min_vessel_level_spinBox)
        self.params_step_factor_spinBox = PropertyDoubleSpinBox(self.params_tab)
        self.params_step_factor_spinBox.setGeometry(QtCore.QRect(160, 170, 111, 22))
        self.params_step_factor_spinBox.setMaximum(1.0)
        self.params_step_factor_spinBox.setSingleStep(0.05)
        self.add_property_binder("step_factor", self.params_step_factor_spinBox)
        self.params_line = QtGui.QFrame(self.params_tab)
        self.params_line.setGeometry(QtCore.QRect(10, 200, 261, 16))
        self.params_line.setFrameShape(QtGui.QFrame.HLine)
        self.params_line.setFrameShadow(QtGui.QFrame.Sunken)

        self.params_line2 = QtGui.QFrame(self.params_tab)
        self.params_line2.setGeometry(QtCore.QRect(10, 335, 261, 16))
        self.params_line2.setFrameShape(QtGui.QFrame.HLine)
        self.params_line2.setFrameShadow(QtGui.QFrame.Sunken)

        self.params_wl_label = QtGui.QLabel(self.params_tab)
        self.params_wl_label.setGeometry(QtCore.QRect(10, 355, 336, 22))

        self.params_wh_label = QtGui.QLabel(self.params_tab)
        self.params_wh_label.setGeometry(QtCore.QRect(10, 385, 336, 22))

        self.params_wl_slider = PropertySlider(self.params_tab)
        self.params_wl_slider.setOrientation(QtCore.Qt.Horizontal)
        self.params_wl_slider.setRange(0, 4095)
        self.params_wl_slider.setGeometry(QtCore.QRect(50, 355, 221, 20))
        self.add_property_binder("wl_level", self.params_wl_slider)

        self.params_wh_slider = PropertySlider(self.params_tab)
        self.params_wh_slider.setOrientation(QtCore.Qt.Horizontal)
        self.params_wh_slider.setRange(0, 4095)
        self.params_wh_slider.setGeometry(QtCore.QRect(50, 385, 221, 20))
        self.add_property_binder("wh_level", self.params_wh_slider)

        self.params_wl_spinner = PropertySpinBox(self.params_tab)
        self.params_wl_spinner.setRange(0, 4095)
        self.params_wl_spinner.setGeometry(QtCore.QRect(50, 410, 111, 20))
        self.add_property_binder("wl_level", self.params_wl_spinner)

        self.params_wh_spinner = PropertySpinBox(self.params_tab)
        self.params_wh_spinner.setRange(0, 4095)
        self.params_wh_spinner.setGeometry(QtCore.QRect(160, 410, 111, 20))
        self.add_property_binder("wh_level", self.params_wh_spinner)

        self.tabWidget.addTab(self.params_tab, "Params")

        self.bases_tab = QtGui.QWidget()
        self.bases_listView = PropertyQList(self.bases_tab)
        self.bases_listView.setGeometry(QtCore.QRect(10, 20, 261, 231))
        self.add_property_binder("database3DframesNamesList", self.bases_listView)
        self.add_property_binder("currentFrameIndex", self.bases_listView)
        self.add_property_binder("referenceFrameIndex", self.bases_listView)
        self.bases_next_base_button = ActionButton(self.bases_tab)
        self.bases_next_base_button.setGeometry(QtCore.QRect(140, 320, 131, 28))
        self.add_action_binder("nextDatabase", self.bases_next_base_button)
        self.bases_prev_base_button = ActionButton(self.bases_tab)
        self.bases_prev_base_button.setGeometry(QtCore.QRect(10, 320, 131, 28))
        self.add_action_binder("previousDatabase", self.bases_prev_base_button)
        self.bases_set_reference_base_button = ActionButton(self.bases_tab)
        self.bases_set_reference_base_button.setGeometry(QtCore.QRect(10, 350, 261, 28))
        self.add_action_binder("setReferenceSegmentationIndex", self.bases_set_reference_base_button)
        self.bases_current_base_label = QtGui.QLabel(self.bases_tab)
        self.bases_current_base_label.setGeometry(QtCore.QRect(10, 260, 91, 21))
        self.bases_current_base_lineEdit = PropertyQLine(self.bases_tab)
        self.bases_current_base_lineEdit.setEnabled(False)
        self.bases_current_base_lineEdit.setGeometry(QtCore.QRect(10, 285, 261, 22))
        self.add_property_binder("currentFrameName", self.bases_current_base_lineEdit)

        self.tabWidget.addTab(self.bases_tab, "Bases")

        self.tabWidget.currentChanged.connect(self.onChangeTabSignal)




        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 959, 26))
        self.menuFile = QtGui.QMenu(self.menubar)
        self.menuHelp = QtGui.QMenu(self.menubar)
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        MainWindow.setStatusBar(self.statusbar)
        self.toolBar = QtGui.QToolBar(MainWindow)
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)

        self.actionLicense = QtGui.QAction(MainWindow)

        self.actionAbout = QtGui.QAction(MainWindow)
        self.actionAbout.triggered.connect(self.about)

        # self.MultipleRAWDialog = Ui_DialogMultipleRAWLoader(self)
        # self.add_property_binder("rawDataFilesPth", self.MultipleRAWDialog)

        self.actionLoad_Dicom = PropertyLoadFile(MainWindow, 'Open Dicom File', False)
        self.add_property_binder("dicomDataFilePth", self.actionLoad_Dicom)
        self.get_property("dicomDataFilePth").set_UI_update_listener(self.prepareNewDatabasePanels)

        self.actionLoad_multiRAW_data = PropertyRawFiles(MainWindow, 'Open Multiple RAW Files', Ui_DialogMultipleRAWLoader(self))
        self.add_property_binder("rawDataFilesPth", self.actionLoad_multiRAW_data)
        self.get_property("rawDataFilesPth").set_UI_update_listener(self.prepareNewDatabasePanels)

        self.actionOpen_project = PropertyLoadFile(MainWindow, 'Open Project File', False)
        self.add_property_binder("loadProjectFilePath", self.actionOpen_project)

        self.actionSave_project = ActionQAction(MainWindow)
        self.add_action_binder("saveProject", self.actionSave_project)

        self.actionSave_project_as = PropertyLoadFile(MainWindow, 'Save As Project File', True)
        self.add_property_binder("saveAsProjectFilePath", self.actionSave_project_as)

        self.actionExit = ActionQAction(MainWindow)
        self.add_action_binder("exitApplication", self.actionExit)
        self.actionExit.setShortcut('Ctrl+Q')
        self.actionExit.triggered.connect(MainWindow.close)

        self.actionSeedDetectVessel = ActionQAction(MainWindow)
        self.add_action_binder("detectVessel", self.actionSeedDetectVessel)

        self.resetZoom = ActionQAction(MainWindow)
        self.add_action_binder("resetZoom", self.resetZoom)

        self.prevBase = ActionQAction(MainWindow)
        self.add_action_binder("previousDatabase", self.prevBase)

        self.nextBase = ActionQAction(MainWindow)
        self.add_action_binder("nextDatabase", self.nextBase)

        self.menuFile.addAction(self.actionOpen_project)
        self.menuFile.addAction(self.actionLoad_Dicom)
        self.menuFile.addAction(self.actionLoad_multiRAW_data)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionSave_project)
        self.menuFile.addAction(self.actionSave_project_as)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionExit)
        self.menuHelp.addAction(self.actionLicense)
        self.menuHelp.addAction(self.actionAbout)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())

        self.toolBar.addAction(self.actionSeedDetectVessel)
        self.toolBar.addAction(self.resetZoom)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.prevBase)
        self.toolBar.addAction(self.nextBase)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        mipAlgorithm = MipAlgorithm()
        self.mipPanel = MipPanel(mipAlgorithm)
        self.sliceAlgorithm = SliceAlgorithm()
        self.segmentationAlgorithm = SegmentationAlgorithm()
        self.segmentationPanel = SegmentationPanel(self.segmentationAlgorithm)
        self.vesselCreator = VesselCreator(self.segmentationAlgorithm)
        self.slicePanel = SlicePanel(self.sliceAlgorithm)
        self.panelHoder = PanelHolder()
        self.panelHoder.add_panel(self.mipPanel)
        self.panelHoder.add_panel(self.slicePanel)
        self.panelHoder.add_panel(self.segmentationPanel)
        self.add_property_binder("zoomValue", self.panelHoder)
        self.add_property_binder("controlKeyPressed", self.panelHoder)

        self.add_property_binder("imageSeedPoint", self.slicePanel)
        self.add_property_binder("sliceMousePointerCurrentValue", self.slicePanel)
        self.add_property_binder("currentFrameIndex", self.slicePanel)
        self.add_property_binder("wl_level", self.slicePanel)
        self.add_property_binder("wh_level", self.slicePanel)

        self.add_property_binder("displayVisitedVoxels", self.slicePanel)
        self.add_property_binder("displayPrevSegmentation", self.slicePanel)
        self.add_property_binder("displayNextSegmentation", self.slicePanel)
        self.add_property_binder("displayReferenceSegmentation", self.slicePanel)
        self.add_property_binder("segmentationResult",self.slicePanel)

        self.set_property_value("MipAlgorithm", self.mipPanel)
        self.set_property_value("sliceAlgorithm", self.slicePanel)
        self.set_property_value("segmentationAlgorithm", self.segmentationPanel)
        self.set_property_value("vesselCreator", self.vesselCreator)

        self.add_property_binder("currentFrameIndex",self.segmentationPanel)
        self.add_property_binder("segmentationResult",self.segmentationPanel)
        self.add_property_binder("segmentationSelectedVeselPoint",self.segmentationPanel)
        self.add_property_binder("displayResultPoints", self.segmentationPanel)
        self.add_property_binder("displayVisitedVoxels", self.segmentationPanel)
        self.add_property_binder("vesselName", self.segmentationPanel)
        self.add_property_binder("showCalcifs", self.segmentationPanel)
        self.add_property_binder("currentVesselPointX", self.segmentationPanel)
        self.add_property_binder("currentVesselPointY", self.segmentationPanel)
        self.add_property_binder("currentVesselPointZ", self.segmentationPanel)
        self.add_property_binder("currentVesselPointAlpha", self.segmentationPanel)
        self.add_property_binder("currentVesselPointBeta", self.segmentationPanel)
        self.add_property_binder("currentVesselPointRadius", self.segmentationPanel)
        self.add_property_binder("displayPrevSegmentation", self.segmentationPanel)
        self.add_property_binder("displayNextSegmentation", self.segmentationPanel)
        self.add_property_binder("displayReferenceSegmentation", self.segmentationPanel)
        self.add_property_binder("currentVesselPointIndex", self.segmentationPanel)
        self.add_property_binder("activeSegmentationVessel", self.segmentationPanel)

        self.add_property_binder("imageSeedPoint", self.mipPanel)
        self.add_property_binder("mipRemoveBackground", self.mipPanel)
        self.add_property_binder("database3DframesNamesList", self.mipPanel)
        self.add_property_binder("currentFrameIndex", self.mipPanel)

        self.add_property_binder("database4D", self.mipPanel)
        self.add_property_binder("database4D", self.slicePanel)
        self.add_property_binder("vesselCreator", self.slicePanel)
        self.add_property_binder("database4D", self.segmentationPanel)

        self.add_property_binder("lastClickedImageNumber", self.mipPanel)
        self.add_property_binder("mipSelectionPointType", self.mipPanel)
        self.add_property_binder("wl_level", self.mipPanel)
        self.add_property_binder("wh_level", self.mipPanel)

        button = QtGui.QPushButton("&Download")
        button.setSizePolicy(QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Expanding)

        self.gridLayout = QtGui.QGridLayout()
        self.setLayout(self.gridLayout)

        self.gridLayout.addWidget(self.panelHoder, 0, 0)
        self.gridLayout.addWidget(self.tabWidget, 0, 1)
        MainWindow.setCentralWidget(self)



    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle("K4D")

        # self.mip_selection_point_type_checkBox.setText("Set selection type")
        self.remove_background_button.setText("Remove selected background")

        self.slices_X_label.setText("X: ")
        self.slices_Y_label.setText("Y: ")
        self.slices_Z_label.setText("Z: ")
        self.slices_valueXYZ_label.setText("Value:")
        self.slices_valueXYZ_lineEdit.setText("0")
        self.slices_HU_XYZ_label.setText("HU:")
        self.slices_HU_XYZ_lineEdit.setText("0")
        self.slices_display_visited_voxels_checkBox.setText("Display voxels")
        self.slices_display_prev_segmentation_checkBox.setText("Display previous segmentation")
        self.slices_display_next_segmentation_checkBox.setText("Display next segmentation")
        self.slices_display_reference_segmentation_checkBox.setText("Display reference segmentation")

        self.results_change_vessel_name_button.setText("Change name")
        self.results_invert_points_button.setText("Invert points")
        self.results_merge_button.setText("Merge")
        self.results_cut_button.setText("Cut vessel")
        self.results_remove_button.setText("Remove")
        self.results_inverse_button.setText("Inverse")
        self.results_deselect_all_button.setText("Deselect all")
        self.results_select_all_button.setText("Select all")
        self.results_calcif_list_label.setText("Calcifs/bones list:")
        self.results_display_points_checkBox.setText("Display points")
        self.results_display_visited_voxels_checkBox.setText("Display voxels")
        self.results_show_calcifs.setText("Show calcifs")
        # self.results_display_prev_point_button.setText("Prev point")
        # self.results_display_next_point_button.setText("Next point")
        self.results_vessel_point_label.setText("Point id:")
        self.results_display_prev_segmentation_checkBox.setText("Display previous segmentation")
        self.results_display_next_segmentation_checkBox.setText("Display next segmentation")
        self.results_display_reference_segmentation_checkBox.setText("Display reference segmentation")
        self.results_export_binary_data_button.setText("Export as binary data")
        self.results_export_text_data_button.setText("Export as text data")

        self.params_step_factor_label.setText("Step:")
        self.params_adapt_vessel_level_checkBox.setText("Adapt vessel level")
        self.params_vessel_level_label.setText("Vessel level:")
        self.params_adapt_bg_level_checkBox.setText("Adapt BG level")
        self.params_remove_calcification_checkBox.setText("Remove calcifications/bones")
        self.params_window_size_label.setText("Window size:")
        self.params_min_vessel_level_label.setText("Min. vessel level:")
        self.params_max_vessel_level_label.setText("Max. vessel level:")
        self.params_BG_level_label.setText("BG level:")
        self.params_adapt_window_size_checkBox.setText("Adapt window size")
        self.params_wl_label.setText("WL:")
        self.params_wh_label.setText("WH:")

        self.bases_next_base_button.setText("Next")
        self.bases_prev_base_button.setText("Prev")
        self.bases_current_base_label.setText("Current base:")
        self.bases_current_base_lineEdit.setText("Current base name")
        self.bases_set_reference_base_button.setText("Set current as reference base")

        self.menuFile.setTitle("File")
        self.menuHelp.setTitle("Help")
        self.toolBar.setWindowTitle("toolBar")
        self.actionLicense.setText("License")
        self.actionLicense.setToolTip("Show license")
        self.actionAbout.setText("About")
        self.actionAbout.setToolTip("Show informations about program")
        self.actionLoad_Dicom.setText("Load Dicom data")
        self.actionLoad_Dicom.setToolTip("Load Dicom data into the base")
        self.actionLoad_multiRAW_data.setText("Load multiple RAW data")
        self.actionLoad_multiRAW_data.setToolTip("Load multiple RAW data into the base")
        self.actionOpen_project.setText("Open project")
        self.actionOpen_project.setToolTip("Open existing project")
        self.actionSave_project.setText("Save project")
        self.actionSave_project.setToolTip("Save changes to the project")
        self.actionSave_project_as.setText("Save project as")
        self.actionSave_project_as.setToolTip("Save project as a new file")
        self.actionExit.setText("Exit")
        self.actionExit.setShortcut("Ctrl+Q")
        self.actionSeedDetectVessel.setText("Start segmentation")
        self.actionSeedDetectVessel.setToolTip("Start detecting vessels from selected point")
        self.resetZoom.setText("Reset zoom")
        self.resetZoom.setToolTip("Reset zoom value to default value")
        self.prevBase.setText("Previous database")
        self.prevBase.setToolTip("Go to the previous database")
        self.nextBase.setText("Next database")
        self.nextBase.setToolTip("Go to the next database")
        # self.actionShowSlice.setText("Show slice")
        # self.actionShowSlice.setToolTip("Show slice")

    def results_change_vessel_name_button_clicked(self):
        self.set_property_value("vesselName", self.results_current_base_lineEdit.text())

    def about(self):
        QMessageBox.about(None, "About K4D 0.0.1 ",
            u"""<b>K4D is a program created to assist with coronarography</b>
            <p>All rights reserved in accordance with GPL v2 or later
            <p>This application was created as a project for thesis for the Technical University of Bialystok """)

    def onChangeTabSignal(self, selected_index):
        self.set_property_value("mainMenuTabIndex", selected_index)



    def prepareNewDatabasePanels(self):
        '''invoked when new database was loaded'''
        myDataBase4D = self.get_property_value("database4D")
        if myDataBase4D is None:
            return
        self.x_slider.setRange(0, myDataBase4D.get_x_size() - 1)
        self.y_slider.setRange(0, myDataBase4D.get_y_size() - 1)
        self.z_slider.setRange(0, myDataBase4D.get_z_size() - 1)
        self.slices_X_spinBox.setRange(0, myDataBase4D.get_x_size() - 1)
        self.slices_Y_spinBox.setRange(0, myDataBase4D.get_y_size() - 1)
        self.slices_Z_spinBox.setRange(0, myDataBase4D.get_z_size() - 1)


    def application_exit(self):
        pass



# public static method main
def main():
    app = QtGui.QApplication(sys.argv)

    MainWindow = QtGui.QMainWindow()
    ap = Application()
    ap.setup_app()
    ap.mainWindow.show()
    app.exec_()
    sys.exit()

if __name__ == '__main__':
    main()