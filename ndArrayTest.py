
import numpy as np






print "start"

x1 = np.array([[1, 2, 3], [4, 5, 6]], np.int8)
x2 = np.array([[1, 2, 3], [4, 5, 7]], np.int8)
x3 = np.array([[1, 2, 3], [4, 5, 8]], np.int8)
x4 = np.array([[1, 2, 3], [4, 5, 9]], np.int8)
x5 = np.array([[1, 2, 3], [4, 5, 10]], np.int8)

image_list = []
image_list.append(x1)
image_list.append(x2)
image_list.append(x3)
image_list.append(x4)
image_list.append(x5)

numpy_array_3D = np.concatenate(image_list, axis=0)
numpy_array_3D = numpy_array_3D.reshape(-1) #conversion need 1D array (change from 2D)
numpy_array_3D.dtype = np.uint8 # cast to bytes
numpy_array_3D = numpy_array_3D.reshape((5, 2, 3))


print numpy_array_3D[2,1,2]

print "subtract"
result = np.subtract([[[1]]], numpy_array_3D)
print result[2,1,2]
