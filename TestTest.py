__author__ = 'MCin22'
import numpy as np
from collections import deque

class VesselPoint(object):
    def __init__(self):
        self.x = 0
        self.y = 0
        self.z = 0
        self.radius = 0
        self.vessel_level = 0
        self.bg_level = 0
        self.alpha = 0
        self.beta = 0
        self.mask_size = 0

    def equals(self, p):
        return self.x == p.x and self.y == p.y and self.z == p.z

    def int_point(self):
        return Point(int(self.x), int(self.y), int(self.z))

class Point(object):
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def equals(self, p):
        return self.x == p.x and self.y == p.y and self.z == p.z

    def int_point(self):
        return Point(int(self.x), int(self.y), int(self.z))

class VesselBranch(object):
    def __init__(self):
        self.points = []
        self.parent = None
        self.children = []

class Calcification(object):
    def __init__(self):
        self.max_x = self.max_y = self.max_z = -1
        self.min_x = self.min_y = self.min_z = np.inf
        self.voxels = []

class Image3D(object):
    def __init__(self, x, y, z, bits_per_voxel):
        self.size_x = x
        self.size_y = y
        self.size_z = z
        self.bpv = bits_per_voxel
        self.vessel_tree = []   # set of vessel tree roots - results of tracking
        self.calcification_voxels = None  # bit array; voxels of detected calcifications
        self.calcifications = None  # list of detected calcifications
        self.vessel_voxels = None   # bit array; voxels belonging to already detected vessels
        self.data = None    # image voxels (z, y, x)

        self.read_masque()  # read masques from files
        self.init_sin_cos()

    def read_masque(self):
        OPER_COUNT = 12 # max. value: 16, max. radius of masque
        self.masks = {}
        for radius in range(1, OPER_COUNT + 1):
            size = 2 * radius + 1
            path = 'masque\\masque.{0}'.format(size)
            dt = np.dtype(np.float)
            self.masks[radius - 1] = np.fromfile(path, dtype=dt).reshape(10, size, size, size)
            #print path
            #print self.masks[radius - 1]

    def init_sin_cos(self):

        self.SINN = np.zeros(3601)
        self.COSS = np.zeros(3601)

        self.SIN = np.zeros(361)
        self.COS = np.zeros(361)
        self.ATANCOS = np.zeros(361)
        self.ATANSAC = np.zeros(361)

        self.SIN_2 = np.zeros(181)
        self.COS_2 = np.zeros(181)

        for i in range(0, 3601):
            self.SINN[i] = np.sin(np.pi * i/1800.0)
            self.COSS[i] = np.cos(np.pi * i/1800.0)

        for i in range(0, 361):
            self.SIN[i] = np.sin(np.pi*i/180.0)
            self.COS[i] = np.cos(np.pi*i/180.0)

            self.ATANCOS[i] = 180.0 * np.arctan(np.cos(np.pi*i/180.0)) / np.pi
            self.ATANSAC[i] = 180.0 * np.arctan(self.SIN[i] * np.cos(self.ATANCOS[i])) / np.pi

        for i in range(0, 181):
            self.SIN_2[i] = (1 - self.COS[2*i]) / 2
            self.COS_2[i] = (1 + self.COS[2*i]) / 2

    def track_vessel(self, start_point, parameters, recursion_level=0):    # fr: suivi_vais

        if not self.in_vessel_check(start_point, parameters):
            return -1

        current_mask_size = parameters["initial_mask_size"]
        current_vessel_level = parameters["initial_vessel_level"]
        current_bg_level = parameters["initial_bg_level"]

        vessel_point = self.estimate_vessel_cylinder(start_point, current_mask_size, current_vessel_level, current_bg_level, parameters)

        current_mask_size = vessel_point.mask_size
        current_vessel_level = vessel_point.vessel_level
        current_bg_level = vessel_point.bg_level

        direction = -1

        result_vessel_branch = [vessel_point]

        while direction <= 1:   # moments.cpp:2130
            #tracking in two opposing directions

            previous_point = vessel_point
            stop_tracking = False

            while not stop_tracking:
                step = current_mask_size * parameters["step_factor"]
                next_point_candidate = self.select_next_point(previous_point, step, direction)

                stop_tracking = self.in_vessel_check(next_point_candidate, parameters)

                if stop_tracking:
                    continue

                next_point = self.estimate_vessel_cylinder(next_point_candidate, current_mask_size, current_vessel_level, current_bg_level, parameters)

                if direction < 0:
                    result_vessel_branch.insert(0, next_point)
                else:
                    result_vessel_branch.append(next_point)

                current_mask_size = next_point.mask_size
                current_vessel_level = next_point.vessel_level
                current_bg_level = next_point.bg_level

                previous_point = next_point

            direction += 2

        pass

    def in_vessel_check(self, point, mask_size, bg_level, parameters):   # fr: dans_vaisseau
        mask_radius = int(mask_size/2.0)
        mean, var = self.mean_variance3D(point, mask_size, bg_level)

        return mean > parameters["mean_threshold"] and var > parameters["variance_threshold"] and mean > bg_level

    def mean_variance3D(self, point, mask_size, bg_level):
        ix = int(point.x)
        iy = int(point.y)
        iz = int(point.z)

        mask_radius = mask_size/2

        min_x = ix - mask_radius
        min_y = iy - mask_radius
        min_z = iz - mask_radius

        max_x = ix + mask_radius
        max_y = iy + mask_radius
        max_z = iz + mask_radius

        if min_x < 0: min_x = 0
        if min_y < 0: min_y = 0
        if min_z < 0: min_z = 0

        if max_x >= self.size_x: max_x = self.size_x
        if max_y >= self.size_y: max_y = self.size_y
        if max_z >= self.size_z: max_z = self.size_z

        m = np.mean(self.data[min_z:max_z+1, min_y:max_y+1, min_x:max_x+1]) # srednia z vokseli przykrytych przez maske w 3D
        v = np.var(self.data[min_z:max_z+1, min_y:max_y+1, min_x:max_x+1])

        return (m, v)

    def estimate_vessel_cylinder(self, point, mask_size, vessel_level, bg_level, parameters):  # fr: estim_cyl, calculates parameters of the vessel cylinder (center point, 3D orientation, radius, vessel and BG levels)
        if not point.x in range(0, self.size_x)  and not point.y in range(0, self.size_y) and not point.z in range(0, self.size_z):
            return None

        if parameters["calcification_removing"]:
            while True:
                calcification_seed_point = self.search_calcification_seed_point(point, 1.5*mask_size, parameters["max_vessel_level"])

                if calcification_seed_point is None:
                    break

                calcification = self.extract_calcification(calcification_seed_point, 1.5*mask_size, parameters["max_vessel_level"])

                for v in calcification.voxels:
                    self.calcification_voxels[v.z, v.y, v.x] = 1

        mask_radius = mask_size / 2.0

        cylinder = VesselPoint()

        recalculate_window = True
        while recalculate_window:
            sigma = 0.6666666666666666
            sigma = 2.0 * sigma * sigma

            old_point = point

            # mask centerage
            moments = self.calculate_moments(point, vessel_level, bg_level, mask_size)

            dx = mask_radius * moments[1] / moments[0]
            dy = mask_radius * moments[2] / moments[0]
            dz = mask_radius * moments[3] / moments[0]

            shift = np.sqrt(dx*dx + dy*dy + dz*dz)

            while True:
                old_shift = shift

                candidate_point = Point()
                candidate_point.x = old_point.x + int(dx * 0.7 * np.sign(dx))
                candidate_point.y = old_point.y + int(dy * 0.7 * np.sign(dy))
                candidate_point.z = old_point.z + int(dz * 0.7 * np.sign(dz))

                if candidate_point.equals(old_point):
                    break

                if not self.is_inside(candidate_point,int(mask_radius)):
                    return None

                moments = self.calculate_moments(candidate_point, vessel_level, bg_level, mask_size)

                dx = mask_radius * moments[1] / moments[0]
                dy = mask_radius * moments[2] / moments[0]
                dz = mask_radius * moments[3] / moments[0]

                shift = np.sqrt(dx*dx + dy*dy + dz*dz)

                if shift < old_shift:
                    old_point = candidate_point
                else:
                    break

            cylinder.x = old_point.x
            cylinder.y = old_point.y
            cylinder.z = old_point.z

            moments = self.calculate_moments(cylinder.int_point(), vessel_level, bg_level, mask_size)

            cylinder.dx = cylinder.x + (mask_radius * moments[1]/moments[0])
            cylinder.dy = cylinder.y + (mask_radius * moments[2]/moments[0])
            cylinder.dz = cylinder.z + (mask_radius * moments[3]/moments[0])

            cylinder.shift = self.moment_centerage(moments)
            cylinder.shift *= mask_radius

            cylinder.alpha, cylinder.beta = self.calculate_angles(moments)

            if parameters["adapting_vessel_level"]:
                new_vessel_level = self.calculate_vessel_level(cylinder, vessel_level)

                cylinder.vessel_level = np.round(0.75 * new_vessel_level + 0.25 * vessel_level)

            if parameters["adapting_bg_level"]:
                new_bg_level = self.calculate_BG_level(cylinder,vessel_level, bg_level)

                cylinder.bg_level = new_bg_level

            vessel_radius = self.calculate_radius(moments,cylinder.vessel_level,cylinder.bg_level)
            cylinder.radius = vessel_radius * mask_radius

            if parameters["adapting_window_size"]:
                new_mask_size = self.calculate_mask_radius(cylinder.radius, mask_size)

                if new_mask_size < 0 or new_mask_size/2 >= len(self.masks):
                    recalculate_window = False
                else:
                    mask_size = new_mask_size


        # TODO finish
        pass

    def is_inside(self, point, radius=0):
        return int(point.x) in range (radius, self.size_x - radius) and int(point.y) in range (radius, self.size_y - radius) and int(point.z) in range (radius, self.size_z - radius)

    def search_calcification_seed_point(self, point, mask_size, max_vessel_level):  # search in the neighbourhood for a voxel with its intensity level above max vessel level not yet detected as the calcficication
        max_x = min(point.x + mask_size / 2 + 1, self.size_x)
        max_y = min(point.y + mask_size / 2 + 1, self.size_y)
        max_z = min(point.z + mask_size / 2 + 1, self.size_z)

        min_x = max(point.x - mask_size / 2, 0)
        min_y = max(point.y - mask_size / 2, 0)
        min_z = max(point.z - mask_size / 2, 0)

        search_region = np.multiply(self.data[min_z:max_z][min_y:max_y][min_x:max_x], np.subtract(1, self.calcification_voxels[min_z:max_z][min_y:max_y][min_x:max_x]))
        if np.amax(search_region) <= max_vessel_level:
            return None

        max_level = -1

        for x in range(min_x, max_x + 1):
            for y in range(min_y, max_y + 1):
                for z in range(min_z, max_z + 1):
                    if search_region[z][y][x] > max_level:
                        max_level = search_region[z][y][x]
                        seed_x, seed_y, seed_z = x, y, z

        return Point(seed_x, seed_y, seed_z)

    def extract_calcification(self, calcification_seed_point, mask_size, max_vessel_level):    # region growing algorithm, TODO: second phase: detecting real calcification borders (deflection point)
        queue = deque()
        queue.append(calcification_seed_point)
        self.calcification_voxels[calcification_seed_point.z][calcification_seed_point.y][calcification_seed_point.x] = 1
        calcification = Calcification()
        calcification.voxels.append(calcification_seed_point)

        while len(queue) > 0:
            point = queue.popleft()

            for dx in (-1, 1):
                if point.x + dx < 0: continue
                if point.x + dx >= self.size_x: continue

                for dy in (-1, 1):
                    if point.y + dy < 0: continue
                    if point.y + dy >= self.size_y: continue

                    for dz in (-1, 1):
                        if point.z + dz < 0: continue
                        if point.z + dz >= self.size_z: continue

                        if self.calcification_voxels[point.z + dz][point.y + dy][point.x + dx] == 1: continue
                        if self.data[point.z + dz][point.y + dy][point.x + dx] > max_vessel_level:
                            self.calcification_voxels[point.z + dz][point.y + dy][point.x + dx] = 1

                            new_point = Point(point.x + dx, point.y + dy, point.z + dz)
                            calcification.voxels.append(new_point)
                            queue.append(new_point)

        return calcification

    def calculate_moments(self, point, vessel_level, bg_level, mask_size):    # fr: op_mnt, calculate values of the geometrical moments
        mask_radius = mask_size / 2

        if not self.is_inside(point, mask_radius): return None
        moment_values = np.zeros(11)

        subvolume_orig = self.data[point.z - mask_radius:point.z + mask_radius + 1, point.y - mask_radius:point.y + mask_radius + 1, point.x - mask_radius:point.x + mask_radius + 1]
        subvolume = np.copy(subvolume_orig)

        calcification_subvolume = self.calcification_voxels[point.z - mask_radius:point.z + mask_radius + 1, point.y - mask_radius:point.y + mask_radius + 1, point.x - mask_radius:point.x + mask_radius + 1]
        np.place(subvolume, calcification_subvolume == 1, vessel_level)

        for i in range(0, 10):
            moment_values[i] = np.sum(np.multiply(self.masks[mask_radius - 1][i], subvolume - bg_level))

        moment_values[10] = np.sum(np.multiply(self.masks[mask_radius - 1][0], subvolume))
        return moment_values

    def M8(self, a, b, moments):
        aa = a + a
        F = moments[7] * self.SIN_2[a] + moments[8] * self.COS_2[a] - moments[4] * self.SIN[aa]

        return F

    def M9(self, a, b, moments):
        aa = a + a
        bb = b + b

        F = (moments[7] * self.COS_2[a] + moments[8] * self.SIN_2[a] + moments[4] * self.SIN[aa]) * self.SIN_2[b]
        F += (moments[5] * self.COS[a] + moments[6] * self.SIN[a]) * self.SIN[bb]
        F += moments[9] * self.COS_2[b]

        return F

    def calculate_angles(self, moments):    # fr: caclul_angles, calculates 3D orientation of vessel from the given moments
        if moments[7] == moments[8]:
            alpha = 45.0
        else:
            alpha = 90.0/ np.pi * np.arctan((2.0 * moments[4] / (moments[7] - moments[8])))

        if alpha > 90.0:
            alpha -= 90
        if alpha < 0.0:
            alpha += 90.0

        A = np.round(alpha)

        if self.M8(A, 0, moments) > self.M8(A + 90, 0, moments):
            alpha += 90

        A = np.round(alpha)
        AA = A + A

        if moments[9] != 0:
            beta = 90.0 / np.pi * np.arctan(2.0 * (moments[5] * self.COS[A] + moments[6] * self.SIN[A]) / (moments[9] - moments[7] * self.COS_2[A] - moments[8] * self.SIN_2[A] - moments[4] * self.SIN[A]))
        else:
            beta = 45.0

        if beta > 90.0:
            beta -= 90.0
        if beta < 0.0:
            beta += 90

        if self.M9(A, np.round(beta), moments) < self.M9(A, np.round(beta + 90), moments):
            beta += 90.0

        return (alpha, beta)

    def calculate_radius(self, moments, vessel_level, bg_level):    # fr: calcul_rayon, calculates the vessel radius from the given moments
        vessel_level = vessel_level - bg_level
        bg_level = 0

        if moments[0] < vessel_level * 4 * np.pi / 3:
            radius = (moments[0] * 3 * np.pi / 4 - vessel_level) / (bg_level - vessel_level)
            radius = radius ** (2.0 / 3)

            if radius > 1.0:
                radius = 0
            else:
                radius = np.sqrt(1 - radius)
        else:
            radius = 1

        return radius

    def calculate_vessel_level(self, vessel_point, vessel_level):    # fr: estime_intensite, adapt the vessel level
        ix = int(vessel_point.dx)
        iy = int(vessel_point.dy)
        iz = int(vessel_point.dz)

        dx = vessel_point.dx - ix
        dy = vessel_point.dy - iy
        dz = vessel_point.dz - iz

        d_x = 1.0 - dx
        d_y = 1.0 - dy
        d_z = 1.0 - dz

        if ix + 1 >= self.size_x or iy + 1 >= self.size_y or iz + 1 >= self.size_z:
            return vessel_level

        if self.check_if_calcification(vessel_point):
            return vessel_level

        new_vessel_level = (d_x * d_y * d_z * self.data[iz, iy, ix] +
                            dx * d_y * d_z * self.data[iz, iy, ix + 1] +
                            d_x * dy * d_z * self.data[iz, iy + 1, ix] +
                            dx * dy * d_z * self.data[iz, iy + 1, ix + 1] +
                            d_x * d_y * dz * self.data[iz + 1, iy, ix] +
                            dx * d_y * dz * self.data[iz + 1, iy, ix + 1] +
                            d_x * dy * dz * self.data[iz + 1, iy + 1, ix] +
                            dx * dy * dz * self.data[iz + 1, iy + 1, ix + 1]
                            )
        return new_vessel_level

    def check_if_calcification(self, vessel_point):
        ix = int(vessel_point.dx)
        iy = int(vessel_point.dy)
        iz = int(vessel_point.dz)
        subvolume = self.data[iz:iz + 2, iy:iy + 2, ix:ix + 2]
        return np.sum(subvolume) > 0

    def calculate_BG_level(self, point, vessel_level, bg_level):    # fr: estime_fond8, adapt BG level
        # TODO finish

        return bg_level

    def calculate_mask_radius(self, vessel_radius, mask_size):   # fr: estime_fenetre, adapt the used mask size
        mask_radius = int(mask_size/2)

        if mask_radius - vessel_radius < 0.0:
            if mask_radius < len(self.masks):
                return mask_size + 2

        if mask_radius - vessel_radius > 1.25:
            if mask_radius > 1:
                return mask_size - 2

        return -1

    def calculate_center_point(self):   # fr: centrage, moves the mask center point to the vessel center point
        # TODO finish

        pass

    def moment_centerage(self, moments):    # fr: centrage, modyfying the moments value
        old_moments = np.copy(moments)
        shift = np.sqrt(moments[1] * moments[1] + moments[2] * moments[2] + moments[3] * moments[3])
        shift /= moments[0]

        moments[4] = old_moments[4] - old_moments[1] * old_moments[2] / old_moments[0]
        moments[5] = old_moments[5] - old_moments[1] * old_moments[3] / old_moments[0]
        moments[6] = old_moments[6] - old_moments[2] * old_moments[3] / old_moments[0]
        moments[7] = old_moments[7] - old_moments[1] * old_moments[1] / old_moments[0]
        moments[8] = old_moments[8] - old_moments[2] * old_moments[2] / old_moments[0]
        moments[9] = old_moments[9] - old_moments[3] * old_moments[3] / old_moments[0]

        return shift

    def select_next_point(self, previous_point, step, direction): # fr: selec_point, selects candidate for the next vessel point
        next_point = Point()

        next_point.x = np.round(previous_point.x + direction*step*self.SIN[np.round(previous_point.beta)]*self.COS[np.round(previous_point.alpha)])
        next_point.y = np.round(previous_point.y + direction*step*self.SIN[np.round(previous_point.beta)]*self.SIN[np.round(previous_point.alpha)])
        next_point.z = np.round(previous_point.z + direction*step*self.COS[np.round(previous_point.beta)])

        return next_point