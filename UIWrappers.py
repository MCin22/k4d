__author__ = 'Adam'

import PIL
import ImageMath
from PySide.QtCore import Qt, QPointF, QRectF
from PySide import QtGui, QtCore
from PySide.QtGui import QStandardItemModel, QStandardItem, QPixmap, QPalette, QTransform, \
    QListWidgetItem, QDialog
from PIL import Image
import numpy as np
import math

from DataController import PropertyBinder, ActionBinder

class PropertyPointPresenter(QtGui.QWidget, PropertyBinder):
    '''Class displays data from current selected segmentation point.'''

    def __init__(self, param):
        super(PropertyPointPresenter, self).__init__(param)
        PropertyBinder.__init__(self)
        self.results_point_x_label = PropertyLabel(self)
        self.results_point_x_label.setGeometry(QtCore.QRect(10, 0, 21, 21))

        self.results_point_x_value = PropertyLabel(self)
        self.results_point_x_value.setGeometry(QtCore.QRect(31, 0, 53, 21))

        self.results_point_y_label = PropertyLabel(self)
        self.results_point_y_label.setGeometry(QtCore.QRect(90, 0, 21, 21))

        self.results_point_y_value = PropertyLabel(self)
        self.results_point_y_value.setGeometry(QtCore.QRect(111, 0, 53, 21))

        self.results_point_z_label = PropertyLabel(self)
        self.results_point_z_label.setGeometry(QtCore.QRect(170, 0, 21, 21))

        self.results_point_z_value = PropertyLabel(self)
        self.results_point_z_value.setGeometry(QtCore.QRect(191, 0, 53, 21))

        self.results_point_alpha_label = PropertyLabel(self)
        self.results_point_alpha_label.setGeometry(QtCore.QRect(10, 30, 21, 21))

        self.results_point_alpha_value = PropertyLabel(self)
        self.results_point_alpha_value.setGeometry(QtCore.QRect(31, 30, 53, 21))

        self.results_point_beta_label = PropertyLabel(self)
        self.results_point_beta_label.setGeometry(QtCore.QRect(90, 30, 21, 21))

        self.results_point_beta_value = PropertyLabel(self)
        self.results_point_beta_value.setGeometry(QtCore.QRect(111, 30, 53, 21))

        self.results_point_radius_label = PropertyLabel(self)
        self.results_point_radius_label.setGeometry(QtCore.QRect(170, 30, 21, 21))

        self.results_point_radius_value = PropertyLabel(self)
        self.results_point_radius_value.setGeometry(QtCore.QRect(191, 30, 53, 21))

        self.results_point_bg_level_label = PropertyLabel(self)
        self.results_point_bg_level_label.setGeometry(QtCore.QRect(10, 60, 41, 21))

        self.results_point_bg_level_value = PropertyLabel(self)
        self.results_point_bg_level_value.setGeometry(QtCore.QRect(51, 60, 53, 21))

        self.results_point_vessel_level_label = PropertyLabel(self)
        self.results_point_vessel_level_label.setGeometry(QtCore.QRect(110, 60, 41, 21))

        self.results_point_vessel_level_value = PropertyLabel(self)
        self.results_point_vessel_level_value.setGeometry(QtCore.QRect(158, 60, 53, 21))

        self.results_point_x_label.setText("X:")
        self.results_point_y_label.setText("Y:")
        self.results_point_z_label.setText("Z:")
        self.results_point_alpha_label.setText("a:")
        self.results_point_beta_label.setText("b:")
        self.results_point_radius_label.setText("r:")
        self.results_point_bg_level_label.setText("BGlvl:")
        self.results_point_vessel_level_label.setText("VESlvl:")

        self.results_point_x_value.setText("0")
        self.results_point_y_value.setText("0")
        self.results_point_z_value.setText("0")
        self.results_point_alpha_value.setText("0")
        self.results_point_beta_value.setText("0")
        self.results_point_radius_value.setText("0")
        self.results_point_bg_level_value.setText("0")
        self.results_point_vessel_level_value.setText("0")

        self.add_property_handler(self.on_segmentationSelectedVeselPoint_update_ui, "segmentationSelectedVeselPoint")

    def on_segmentationSelectedVeselPoint_update_ui(self, newValueForUI, propertyController):
        if newValueForUI:
            activeSegmentationPoint = newValueForUI
            self.results_point_x_value.setText(str(activeSegmentationPoint.x))
            self.results_point_y_value.setText(str(activeSegmentationPoint.y))
            self.results_point_z_value.setText(str(activeSegmentationPoint.z))
            self.results_point_alpha_value.setText(str(activeSegmentationPoint.alpha))
            self.results_point_beta_value.setText(str(activeSegmentationPoint.beta))
            self.results_point_radius_value.setText(str(activeSegmentationPoint.radius))
            self.results_point_bg_level_value.setText(str(activeSegmentationPoint.bg_level))
            self.results_point_vessel_level_value.setText(str(activeSegmentationPoint.vessel_level))
        pass

class PropertyExportButton(QtGui.QPushButton, PropertyBinder):
    '''Wrapped class for QPushButton.'''
    def __init__(self, param, title):
        super(PropertyExportButton, self).__init__(param)
        PropertyBinder.__init__(self)
        self.title = title

    def on_unlink(self, parentController):
        self.clicked.disconnect(self.wrap)

    def on_link(self, parentController):
        self.clicked.connect(self.wrap)

    def wrap(self):
        filename, filter = QtGui.QFileDialog.getSaveFileName(None, self.title, '.')
        if filename is not None and filename != "":
            self.on_get_value_from_UI(filename) # send file path to core

class PropertyLoadFile(QtGui.QAction, PropertyBinder):
    '''Wrapped class for loading file binding.'''

    def __init__(self, param, title, isSaveDialog):
        super(PropertyLoadFile, self).__init__(param)
        PropertyBinder.__init__(self)
        self.title = title
        self.saveDialog = isSaveDialog

    def on_unlink(self, propertyController):
        self.triggered.disconnect(self.wrap)

    def on_link(self, propertyController):
        self.triggered.connect(self.wrap)

    def wrap(self):
        filename = None
        if self.saveDialog == True:
            filename, filter = QtGui.QFileDialog.getSaveFileName(None, self.title, '.')
        else:
            filename, filter = QtGui.QFileDialog.getOpenFileName(None, self.title, '.')
        if filename is not None and filename != "":
            self.on_get_value_from_UI(filename) # send file path to core




class PropertyRawFiles(QtGui.QAction, PropertyBinder):
    '''Wrapped class for loading file binding.'''

    def __init__(self, param, title, dialg):
        super(PropertyRawFiles, self).__init__(param)
        PropertyBinder.__init__(self)
        self.title = title
        self.multipleRAWDialog = dialg

    def on_unlink(self, propertyController):
        self.triggered.disconnect(self.wrap)

    def on_link(self, propertyController):
        self.triggered.connect(self.wrap)

    def wrap(self):
        filesList = []
        filename, filter = QtGui.QFileDialog.getOpenFileNames(None, self.title, '.')
        self.multipleRAWDialog.set_file_names(filename)

        if filename:
            # show second window
            result = self.multipleRAWDialog.exec_()
            if result == QDialog.Accepted:
                # get files list from dialog
                filesList = self.multipleRAWDialog.get_raw_files_list()
                # if list is not empty send data
                if filename is not None and filesList:
                    self.on_get_value_from_UI(filesList) # send files paths to core

class PropertyLoadFiles(QtGui.QAction, PropertyBinder):
    '''Wrapped class for loading files binding.'''

    def __init__(self, param, title):
        super(PropertyLoadFiles, self).__init__(param)
        PropertyBinder.__init__(self)
        self.title = title
        self.triggered.connect(self.wrap)

    def on_unlink(self, propertyController):
        self.triggered.disconnect(self.wrap)
        pass

    def on_link(self, propertyController):
        self.triggered.connect(self.wrap)
        pass

    def wrap(self):
        filenames = None
        filenames, filter = QtGui.QFileDialog.getOpenFileNames(None, self.title, '.')

        if filenames:
            self.on_get_value_from_UI(filenames) # send directory path to core

class PropertySlider(QtGui.QSlider, PropertyBinder):
    '''Wrapped class for slider binding.'''

    def __init__(self, param):
        '''
        :param param:
        :param sliderDimensionId: x=2 y=1 z=0
        :return:
        '''
        super(PropertySlider, self).__init__(param)
        PropertyBinder.__init__(self)
        self.add_property_handler(self.on_slider_update_ui)

    def on_unlink(self, propertyController):
        self.valueChanged[int].disconnect(self.wrap)
        pass

    def on_link(self, propertyController):
        self.valueChanged[int].connect(self.wrap)
        pass

    def wrap(self):
        self.on_get_value_from_UI(self.value())

    def on_slider_update_ui(self, newValueForUI, propertyController):
        self.setValue(newValueForUI)

class PropertySliderXYZ(QtGui.QSlider, PropertyBinder):
    '''Wrapped class for slider binding.'''

    def __init__(self, param, sliderDimensionId):
        '''
        :param param:
        :param sliderDimensionId: x=2 y=1 z=0
        :return:
        '''
        super(PropertySliderXYZ, self).__init__(param)
        PropertyBinder.__init__(self)
        self.id = sliderDimensionId
        self.point = None
        self.add_property_handler(self.on_sliderXYZ_update_ui)

    def on_unlink(self, propertyController):
        self.valueChanged[int].disconnect(self.wrap)
        pass

    def on_link(self, propertyController):
        self.valueChanged[int].connect(self.wrap)
        pass

    def wrap(self):
        self.point[self.id] = self.value()
        self.on_get_value_from_UI(self.point)

    def on_sliderXYZ_update_ui(self, newValueForUI, propertyController):
        self.point = newValueForUI
        self.setValue(self.point[self.id])

class PropertyReleaseSlider(QtGui.QSlider, PropertyBinder):
    '''Wrapped class for slider binding. This slider sets value after release '''

    def __init__(self, param):
        super(PropertyReleaseSlider, self).__init__(param)
        PropertyBinder.__init__(self)
        self.add_property_handler(self.on_releaseSlider_update_ui)

    def on_unlink(self, propertyController):
        self.sliderReleased.disconnect(self.wrap)
        #self.valueChanged[int].disconnect(self.wrap)
        pass

    def on_link(self, propertyController):
        self.sliderReleased.connect(self.wrap)
        #self.valueChanged[int].connect(self.wrap)
        pass

    def wrap(self):
        self.on_get_value_from_UI(self.value())

    def on_releaseSlider_update_ui(self, newValueForUI, propertyController):
        self.setValue(newValueForUI)


class PropertyQLine(QtGui.QLineEdit, PropertyBinder):
    '''Wrapped class for non editable text field binding.'''

    def __init__(self, param):
        super(PropertyQLine, self).__init__(param)
        PropertyBinder.__init__(self)
        self.add_property_handler(self.on_line_update_ui)

    def on_line_update_ui(self, newValueForUI, propertyController):
        self.setText(str(newValueForUI))

class PropertyCheckBox(QtGui.QCheckBox, PropertyBinder):
    '''Wrapped class for slider binding.'''

    def __init__(self, param):
        super(PropertyCheckBox, self).__init__(param)
        PropertyBinder.__init__(self)
        self.add_property_handler(self.on_checkBox_update_ui)

    def on_unlink(self, propertyController):
        self.stateChanged[int].disconnect(self.wrap)
        pass

    def on_link(self, propertyController):
        self.stateChanged[int].connect(self.wrap)
        pass

    def wrap(self):
        self.on_get_value_from_UI(self.isChecked())

    def on_checkBox_update_ui(self, newValueForUI, propertyController):
        if newValueForUI:
            state = 2
        else:
            state = 0

        state = QtCore.Qt.CheckState(state)
        self.setCheckState(state)

class PropertyLabel(QtGui.QLabel, PropertyBinder):
    '''Wrapped class for label binding.'''

    def __init__(self, param):
        super(PropertyLabel, self).__init__(param)
        PropertyBinder.__init__(self)
        self.add_property_handler(self.on_label_update_ui)

    def on_label_update_ui(self, newValueForUI, propertyController):
        self.setText(newValueForUI)

class PropertyQComboBox(QtGui.QComboBox, PropertyBinder):
    '''Wrapped class for combobox binding.'''

    def __init__(self, param):
        super(PropertyQComboBox, self).__init__(param)
        PropertyBinder.__init__(self)
        self.add_property_handler(self.on_comboBox_update_ui)

    def on_unlink(self, propertyController):
        self.currentIndexChanged.disconnect(self.wrap)
        pass

    def on_link(self, propertyController):
        self.currentIndexChanged.connect(self.wrap)
        pass

    def wrap(self, index):
        if index == 0:
            self.on_get_value_from_UI("8")
        elif index == 1:
            self.on_get_value_from_UI("16")

    def on_comboBox_update_ui(self, newValueForUI, propertyController):
        if newValueForUI == "8":
            self.setCurrentIndex(0)
        elif newValueForUI == "16":
            self.setCurrentIndex(1)




class PropertySpinBox(QtGui.QSpinBox, PropertyBinder):
    '''Wrapped class for spinbox binding.'''

    def __init__(self, param):
        super(PropertySpinBox, self).__init__(param)
        PropertyBinder.__init__(self)
        self.setMaximum(100000)
        self.add_property_handler(self.on_spinBox_update_ui)

    def on_unlink(self, propertyController):
        self.valueChanged[int].disconnect(self.wrap)
        pass

    def on_link(self, propertyController):
        self.valueChanged[int].connect(self.wrap)
        pass

    def wrap(self):
        self.on_get_value_from_UI(self.value())

    def on_spinBox_update_ui(self, newValueForUI, propertyController):
        self.setValue(newValueForUI)



class PropertySpinBoxXYZ(QtGui.QSpinBox, PropertyBinder):
    '''Wrapped class for spinbox xyz binding.'''

    def __init__(self, param, id):
        super(PropertySpinBoxXYZ, self).__init__(param)
        PropertyBinder.__init__(self)
        self.setMaximum(100000)
        self.id = id
        self.point = None
        self.add_property_handler(self.on_spinBoxXYZ_update_ui)

    def on_unlink(self, propertyController):
        self.valueChanged[int].disconnect(self.wrap)
        pass

    def on_link(self, propertyController):
        self.valueChanged[int].connect(self.wrap)
        pass

    def wrap(self):
        self.point[self.id] = self.value()
        self.on_get_value_from_UI(self.point)

    def on_spinBoxXYZ_update_ui(self, newValueForUI, propertyController):
        self.point = newValueForUI
        self.setValue(self.point[self.id])

class PropertyDoubleSpinBox(QtGui.QDoubleSpinBox, PropertyBinder):
    '''Wrapped class for spinbox binding.'''

    def __init__(self, param):
        super(PropertyDoubleSpinBox, self).__init__(param)
        PropertyBinder.__init__(self)
        self.add_property_handler(self.on_doubleSpinbox_update_ui)

    def on_unlink(self, propertyController):
        self.valueChanged[float].disconnect(self.wrap)
        pass

    def on_link(self, propertyController):
        self.valueChanged[float].connect(self.wrap)
        pass

    def wrap(self):
        self.on_get_value_from_UI(self.value())

    def on_doubleSpinbox_update_ui(self, newValueForUI, propertyController):
        self.setValue(newValueForUI)


class PropertyPointSpinBox(QtGui.QSpinBox, PropertyBinder):
    '''Wrapped class for spinbox binding.'''

    def __init__(self, param):
        super(PropertyPointSpinBox, self).__init__(param)
        PropertyBinder.__init__(self)
        self.setMaximum(100000)
        self.add_property_handler(self.on_currentVesselPointIndex_update_ui,"currentVesselPointIndex")
        self.add_property_handler(self.on_activeSegmentationVessel_update_ui,"activeSegmentationVessel")

    def on_unlink(self, propertyController):
        self.valueChanged[int].disconnect(self.wrap)
        pass

    def on_link(self, propertyController):
        self.valueChanged[int].connect(self.wrap)
        pass

    def wrap(self):
        self.on_get_value_from_UI(self.value())

    def on_currentVesselPointIndex_update_ui(self, newValueForUI, propertyController):
        if newValueForUI:
            self.setValue(newValueForUI)

    def on_activeSegmentationVessel_update_ui(self, newValueForUI, propertyController):
        if newValueForUI:
            maxValue = len(newValueForUI.pointList) - 1
            self.setMaximum(maxValue)
            if self.value() > maxValue:
                self.setValue(maxValue)
            self.on_get_value_from_UI(maxValue, "currentVesselPointIndex")



class ActionQAction(QtGui.QAction, ActionBinder):
    '''Wrapped class for QAction binding.'''

    def __init__(self, param):
        super(ActionQAction, self).__init__(param)

    def on_unlink(self, parentController):
        self.triggered.disconnect(parentController.invoke_action_performaed)

    def on_link(self, parentController):
        self.triggered.connect(parentController.invoke_action_performaed)


class ActionButton(QtGui.QPushButton, ActionBinder):
     '''Wrapped class for QPushButton.'''
     def __init__(self, param):
        super(ActionButton, self).__init__(param)

     def on_unlink(self, parentController):
        self.clicked.disconnect(parentController.invoke_action_performaed)

     def on_link(self, parentController):
        self.clicked.connect(parentController.invoke_action_performaed)

class ColorHelper(object):
    '''class with static color lookup table'''
    color_lokup_table = []
    for i in range(256):
        color_lokup_table.append(QtGui.qRgb(i,i,i))




class PropertyQTree(QtGui.QTreeView, PropertyBinder):

    def __init__(self, param):
        super(PropertyQTree, self).__init__(param)
        PropertyBinder.__init__(self)
        self.result = None
        self.selectedFrameIndex = 0
        self.model = QStandardItemModel()
        self.setModel(self.model)
        self.currentVesselId = 0
        self.header().setMovable(False)
        self.add_property_handler(self.on_currentFrameIndex_update_ui,"currentFrameIndex")
        self.add_property_handler(self.on_segmentationResult_update_ui,"segmentationResult")
        self.add_property_handler(self.on_vesselName_update_ui,"vesselName")

    def on_link(self, parentController):
        self.model.itemChanged = self.currentChanged
        pass # default do nothing, or you could enable mouse actions on tree here.

    def on_unlink(self, parentController):
        self.model.itemChanged = None
        # you could disable mouse actions here if you implement any
        pass

    def currentChanged(self, modelIndexNow, modelIndexLast):
        index = modelIndexNow.row()
        selectedItem = self.model.itemFromIndex(modelIndexNow)
        if self.result is None: return
        result3D = self.result[self.selectedFrameIndex]  # SegmentationResult3D
        if result3D is None: return

        if not selectedItem: return
        itemData = selectedItem.data()
        self.currentVesselId = itemData.vesselId

        # print selectedItem.text(), str(selectedItem.checkState())

        if selectedItem.checkState() == QtCore.Qt.Checked:
            condition = True
        else:
            condition = False
        if itemData.name != selectedItem.text() or itemData.isSelected != condition:
            itemData.name = selectedItem.text()
            if selectedItem.checkState() == QtCore.Qt.Checked:
                itemData.isSelected = True
            else:
                itemData.isSelected = False
            # refresh tree view
            self.on_get_value_from_UI(self.result, "segmentationResult") # send value to core
        self.on_get_value_from_UI(selectedItem.text(), "vesselName")

    def on_currentFrameIndex_update_ui(self, newValueForUI, propertyController):
        self.selectedFrameIndex = newValueForUI

    def on_segmentationResult_update_ui(self, newValueForUI, propertyController):
        self.result = newValueForUI
        if self.result is None:
            return
        self.model.clear()
        if not self.result:
            return
        else:
            if not self.result.has_key(self.selectedFrameIndex):
                return #result could not exist yet
            result3D = self.result[self.selectedFrameIndex]  # SegmentationResult3D
            if result3D is None:
                return
            for key, vesselBranch in result3D.vesselTree.iteritems():
                if vesselBranch.parent == None:
                    item = QStandardItem(vesselBranch.name)
                    if vesselBranch.isSelected:
                        item.setCheckState(QtCore.Qt.Checked)
                    else:
                        item.setCheckState(QtCore.Qt.Unchecked)
                    item.setData(vesselBranch)
                    item.setCheckable(True)
                    self.create_children(vesselBranch, item)
                    self.model.appendRow(item)

    def on_vesselName_update_ui(self, newValueForUI, propertyController):
        #modelIndex = self.model.itemFromIndex(self.currentIndex())
        if not self.result: return
        result3D = self.result[self.selectedFrameIndex]  # SegmentationResult3D
        if result3D is None: return
        # if modelIndex:
        #     index = self.currentVesselId
        #     data = result3D.vesselTree[index]
        #     data.name = newValueForUI
        #
        #     result3D.vesselTree[index] = data
        #     self.result[self.selectedFrameIndex] = result3D
        #     # refresh tree view
        #     self.on_get_value_from_UI(self.result, "segmentationResult") # send value to core
        if len(self.selectedIndexes()) > 0:
            index = self.selectedIndexes()[0]
            entry = index.model().itemFromIndex(index)
            entry.name = newValueForUI
            result3D.vesselTree[self.currentVesselId].name = newValueForUI
            self.on_get_value_from_UI(self.result, "segmentationResult") # send value to core

    def create_children(self, parent, parent_item):
        '''
        creates by recursion children node to parent node
        '''
        if parent is not None:
            for child in parent.children:
                item = QStandardItem(child.name)
                item.setCheckState(QtCore.Qt.Unchecked)
                item.setData(child)
                item.setCheckable(True)
                self.create_children(child, item)
                parent_item.appendRow(item)

class PropertyQListCalcifs(QtGui.QListWidget, PropertyBinder):
    def __init__(self, param):
        super(PropertyQListCalcifs, self).__init__(param)
        PropertyBinder.__init__(self)
        self.calcifEntries = []
        self.currentFrameIndex = -1
        self.add_property_handler(self.on_currentFrameIndex_update_ui, "currentFrameIndex")
        self.add_property_handler(self.on_segmentationResult_update_ui, "segmentationResult")

    def on_link(self, parentController):
        pass # default do nothing, or you could enable mouse actions on tree here.

    def on_unlink(self, parentController):
        # you could disable mouse actions here if you implement any
        pass

    def on_currentFrameIndex_update_ui(self, newValueForUI, propertyController):
        self.currentFrameIndex = newValueForUI
        self.setCurrentRow(self.currentFrameIndex)

    def on_segmentationResult_update_ui(self, newValueForUI, propertyController):
        self.result = newValueForUI
        if self.result is None:
            return
        self.clear()
        if not self.result:
            return
        else:
            if not self.result.has_key(self.currentFrameIndex):
                return # result could not exist yet
            result3D = self.result[self.currentFrameIndex]  # SegmentationResult3D
            if result3D is None:
                return
            for key, calcification in result3D.calcification_voxels_map.iteritems():
                item = QListWidgetItem("Calcif - X: " + str(key[2]) + ", Y: " + str(key[1]) + ", Z: " + str(key[0]))
                self.addItem(item)


class PropertyMultiRAWQTree(QtGui.QTreeView, PropertyBinder):

    def __init__(self, param):
        super(PropertyMultiRAWQTree, self).__init__(param)
        PropertyBinder.__init__(self)
        self.result = None
        self.model = QStandardItemModel()
        self.setModel(self.model)
        self.setHeaderHidden(True)
        self.add_property_handler(self.on_segmentationResult_update_ui, "rawDataFilesPth")

    def on_link(self, parentController):
        pass # default do nothing, or you could enable mouse actions on tree here.

    def on_unlink(self, parentController):
        # you could disable mouse actions here if you implement any
        pass

    def on_segmentationResult_update_ui(self, newValueForUI, propertyController):
        self.result = newValueForUI
        if self.result is None:
            return
        self.model.clear()
        if not self.result:
            return
        else:
            for filename in self.result:
                item = QStandardItem(filename)
                item.setData(filename)
                #item.setCheckState(QtCore.Qt.Checked)
                #item.setCheckable(True)
                self.model.appendRow(item)


class PropertyQList(QtGui.QListWidget, PropertyBinder):
    def __init__(self, param):
        super(PropertyQList, self).__init__(param)
        PropertyBinder.__init__(self)
        self.databaseNames = []
        self.currentFrameIndex = -1
        self.referenceFrameIndex = None
        self.add_property_handler(self.on_database3DframesNamesList_update_ui, "database3DframesNamesList")
        self.add_property_handler(self.on_currentFrameIndex_update_ui, "currentFrameIndex")
        self.add_property_handler(self.on_referenceFrameIndex_update_ui, "referenceFrameIndex")

    def on_link(self, parentController):
        pass # default do nothing, or you could enable mouse actions on tree here.

    def on_unlink(self, parentController):
        # you could disable mouse actions here if you implement any
        pass

    def on_database3DframesNamesList_update_ui(self, newValueForUI, propertyController):
        self.databaseNames = newValueForUI
        self.clear() #clear lit
        if len(self.databaseNames) == 1:
            # when only one default database without name
            if self.databaseNames[0] != "":
                item = QListWidgetItem(self.databaseNames[0])
            else:
                item = QListWidgetItem("Default")
            self.addItem(item)
        else:
            for name in self.databaseNames:
                item = QListWidgetItem(name)
                # Add the item to the model
                self.addItem(item)

    def on_currentFrameIndex_update_ui(self, newValueForUI, propertyController):
        self.currentFrameIndex = newValueForUI
        self.setCurrentRow(self.currentFrameIndex)

    def on_referenceFrameIndex_update_ui(self, newValueForUI, propertyController):
         self.referenceFrameIndex = newValueForUI
         self.clear() #clear list
         if len(self.databaseNames) == 1:
            character = ""
            if self.referenceFrameIndex == 0:
                character = "*"
            if self.databaseNames[0] != "":
                item = QListWidgetItem(self.databaseNames[0] + str(character))
            else:
                item = QListWidgetItem("Default" + str(character))
            self.addItem(item)
         else:
            index = 0
            for name in self.databaseNames:
                if index == self.referenceFrameIndex:
                    item = QListWidgetItem(name+"*")
                else:
                    item = QListWidgetItem(name)
                self.addItem(item)
                index=index+1

class PanelHolder(QtGui.QGraphicsView, PropertyBinder):
    ''' '''
    def __init__(self):
        super(PanelHolder, self).__init__()
        PropertyBinder.__init__(self)
        self.setTransformationAnchor(QtGui.QGraphicsView.AnchorUnderMouse)
        self.scene = QtGui.QGraphicsScene()
        self.setScene(self.scene)
        self.lastClickedImageNumber = -1
        self.currentIndex = -1
        self.panelList = []
        self.contentPanel = None
        self.lastDragPoint = (0,0)
        self.lastMousePoint = None
        self.zoomSpeed = 1.25 # 125%
        self.currentZoom = 1.0
        self.setFrameShape(QtGui.QFrame.WinPanel)
        self.setFrameShadow(QtGui.QFrame.Sunken)
        self.add_property_handler(self.on_zoomValue_update_ui, "zoomValue")
        self.add_property_handler(self.on_controlKeyPressed_update_ui, "controlKeyPressed")
        self.controlKeyPressed = False
        self.currentTransform = None
        self.scene.mousePressEvent = self.mousePressEventC
        self.scene.mouseMoveEvent = self.mouseMoveEventC

    def scrollContentsBy(self, dx, dy):
        super(PanelHolder, self).scrollContentsBy(dx, dy)
        self.scene.update()

    def add_panel(self, panel):
        '''add panel to scroll holder'''
        self.panelList.append(panel)
        pass

    def set_active_panel_index(self, index):
        '''change actually displayed panel to anel with specified index number'''
        size = len(self.panelList)
        if size<0 or index >= size:
            raise Exception('panel index out of bounds exception ',str(index))
        self.currentIndex = index
        if self.contentPanel is not None:
            self.scene.removeItem(self.contentPanel)
        self.contentPanel = self.panelList[index]
        self.scene.addItem(self.contentPanel)
        contentRect = self.contentPanel.boundingRect()
        self.scene.setSceneRect(contentRect)
        self.scene.update()
        pass

    def mousePressEventC(self, event):
        if self.contentPanel is None:
            return;
        point = event.scenePos()
        self.contentPanel.clickX = point.x()
        self.contentPanel.clickY = point.y()
        if event.button() == QtCore.Qt.RightButton:
            self.lastDragPoint = (point.x(), point.y())
        if self.contentPanel.displayResult == None:
            return # if we dont have data to display, do nothing
        if event.button() == QtCore.Qt.LeftButton:
            image_result = self.contentPanel.displayResult
            img1x = 0
            img1y = 0
            img2x = image_result[0].width() + 10
            img2y = 0
            img3x = 0
            img3y = image_result[0].height() + 10
            size1 = image_result[0].size()
            size2 = image_result[2].size()
            size3 = image_result[1].size()
            if self.contentPanel.contains(img1x, img1y, size1.width(), size1.height(), self.contentPanel.clickX, self.contentPanel.clickY):
                self.lastClickedImageNumber = 0
                self.contentPanel.center_image_click(self.contentPanel.clickX, self.contentPanel.clickY)
            elif self.contentPanel.contains(img2x, img2y, size2.width(), size2.height(), self.contentPanel.clickX, self.contentPanel.clickY):
                self.lastClickedImageNumber = 1
                self.contentPanel.right_image_click(self.contentPanel.clickX-img2x, self.contentPanel.clickY)
            elif self.contentPanel.contains(img3x, img3y, size3.width(), size3.height(), self.contentPanel.clickX, self.contentPanel.clickY):
                self.lastClickedImageNumber = 2
                self.contentPanel.bottom_image_click(self.contentPanel.clickX, self.contentPanel.clickY-img3y)

    def wheelEvent(self, event):
        if self.contentPanel.displayResult is None:
            return
        t = self.viewportTransform()
        t = t.inverted()[0]
        point = t.map(event.pos())
        if self.controlKeyPressed == True or self.contentPanel.on_mouse_wheel_move(point, event) is not None:
            if event.delta() < 0:
                self.currentZoom *= 2-self.zoomSpeed
            else:
                self.currentZoom *= self.zoomSpeed
            t = QtGui.QTransform.fromScale(self.currentZoom, self.currentZoom)
            self.setTransform(t)
            self.update()
            self.on_get_value_from_UI(self.currentZoom, "zoomValue")
        pass

    def mouseMoveEventC(self, event):
        if event.buttons() == QtCore.Qt.RightButton:
            point = event.scenePos()
            moveX = self.lastDragPoint[0] - point.x()
            moveY = self.lastDragPoint[1] - point.y()
            self.lastDragPoint = (point.x()+moveX, point.y()+moveY)
            self.verticalScrollBar().setValue(self.verticalScrollBar().value()+moveY)
            self.horizontalScrollBar().setValue(self.horizontalScrollBar().value()+moveX)

    def on_zoomValue_update_ui(self, newValueForUI, propertyController):
        ''' set panel zoom to specified
        :param zoomValue: number in floating point values form 0, 1 is default size
        '''
        self.currentZoom = newValueForUI
        if self.contentPanel is None:
            return
        image_result = self.contentPanel.displayResult
        if image_result is not None:
            t = QtGui.QTransform.fromScale(self.currentZoom,self.currentZoom)
            self.setTransform(t)
            self.update()

    def on_controlKeyPressed_update_ui(self, newValueForUI, propertyController):
        self.controlKeyPressed = newValueForUI


    def data_not_set_message_panel(self, context):
        oldFont = context.font()
        oldPen = context.pen()
        oldBrush = context.brush()
        context.setPen(QtGui.QColor(120, 120, 120))
        context.drawRect(10, 60, 340, 80)
        context.setPen(QtGui.QColor(40, 40, 40))
        context.setFont(QtGui.QFont('Arial', 20))
        context.drawText(20,100, " Data was not set... ")
        context.setFont(QtGui.QFont('Arial', 10))
        context.drawText(20,120, " or the segmentation process didn't start ")
        #context.setPen(QtGui.QColor(255, 255, 255))
        context.setBrush(QtGui.QColor(109, 19, 8))
        context.drawRect(0, 60, 10, 80)
        context.drawRect(350, 60, 100, 80)
        context.setFont(oldFont)
        context.setPen(oldPen)
        context.setBrush(oldBrush)
        pass

class UiPanel(QtGui.QGraphicsItem, PropertyBinder):
    '''Base class for all panel display data.'''
    def __init__(self, algorithm):
        super(UiPanel, self).__init__()
        PropertyBinder.__init__(self)
        self.isProcessing = False
        self.mouseEnabled = True
        self.algorithm = algorithm
        self.algorithm.onStart = self.on_start
        self.algorithm.on_processing_finish = self.on_processing_finish
        self.originalResult = None
        self.stretchtResult = None
        self.displayResult = None
        self.updateScroll = True
        self.clickX = 0
        self.clickY = 0

    def on_start(self):
        '''
        invoked by algorithm when processing is started
        '''
        self.isProcessing = True
        self.update()
        pass

    #@abstract
    def paint(self, painter, option, widget):
        '''
        abstract method for drawing panel content
        :param context: Grapgics context, use it for drawing
        '''
        pass

    #@abstract
    def boundingRect(self):
        if self.displayResult is not None:
            width = self.displayResult[0].width() + 10 + self.displayResult[2].width()
            height = self.displayResult[0].height() + 10 + self.displayResult[1].height()
            return QtCore.QRectF(0, 0, width, height)
        else:
            return QtCore.QRectF(0, 0, 1000, 1000)

    def on_processing_finish(self, snapshotDict, result):
        '''
        invoked by algorithm when data was calculated
        :param properties:
        :param snapshotDict:
        :param result:
        :return:
        '''
        self.originalResult = result
        self.stretchtResult = self.on_data_stretching(self.originalResult)
        self.displayResult = self.on_convert_result(self.stretchtResult)
        self.isProcessing = False
        self.update()
        pass

    #@abstract
    def on_convert_result(self, result):
        '''when we need to convert raw binary data to image.
            It is invoked only one time when processing is finished.
        '''
        return result

    #@abstract
    def on_data_stretching(self, result):
        '''
        Stretch data values to values from VL to VH.
        '''
        return result

    def on_mouse_wheel_move(self, point, event):
        '''mouse wheel event handling
        '''
        return False

    def invoke_Result_stretching(self):
        '''
        Update data to stretched values
        '''
        if self.originalResult is None:
            return
        self.stretchtResult = self.on_data_stretching(self.originalResult)
        self.displayResult = self.on_convert_result(self.stretchtResult)
        self.update()

    #@abstract
    def center_image_click(self, x, y):
        pass

    #@abstract
    def right_image_click(self, x, y):
        pass

    #@abstract
    def bottom_image_click(self, x, y):
        pass

    def calculateImageIndex(self,x,y):
        '''check witch image contains x,y point'''
        image_result = self.displayResult
        img1x = 0
        img1y = 0
        img2x = image_result[0].width() + 10
        img2y = 0
        img3x = 0
        img3y = image_result[0].height() + 10
        size1 = image_result[0].size()
        size2 = image_result[2].size()
        size3 = image_result[1].size()
        if self.contains(img1x, img1y, size1.width(), size1.height(), x, y):
            return 0
        elif self.contains(img2x, img2y, size2.width(), size2.height(), x, y):
            return 1
        elif self.contains(img3x, img3y, size3.width(), size3.height(), x, y):
            return 2
        else:
            return -1

    def contains(self, x,y,w,h, cx,cy):
        '''Checked image dimensions'''
        return x<=cx and (x+w)>cx and y<=cy and (y+h)>cy


class SegmentationPanel(UiPanel):
    '''segmentation panel with graphic segmentation result'''

    def __init__(self, segmentationAlgorithm):
        super(SegmentationPanel, self).__init__(segmentationAlgorithm)
        self.selectedPoint = None
        self.selectedPointIndex = None
        self.activeSegmentationVessel = None
        self.add_property_handler(self.on_database4D_update_ui, "database4D")
        self.add_property_handler(self.on_segmentationResult_update_ui, "segmentationResult")
        self.add_property_handler(self.on_currentFrameIndex_update_ui, "currentFrameIndex")
        self.add_property_handler(self.on_segmentationSelectedVeselPoint_update_ui, "segmentationSelectedVeselPoint")
        self.add_property_handler(self.on_displayResultPoints_update_ui, "displayResultPoints")
        self.add_property_handler(self.on_displayVisitedVoxels_update_ui, "displayVisitedVoxels")
        self.add_property_handler(self.on_displayPrevSegmentation_update_ui, "displayPrevSegmentation")
        self.add_property_handler(self.on_displayNextSegmentation_update_ui, "displayNextSegmentation")
        self.add_property_handler(self.on_displayReferenceSegmentation_update_ui, "displayNextSegmentation")
        self.add_property_handler(self.on_showCalcifs_update_ui, "showCalcifs")
        self.add_property_handler(self.on_currentVesselPointIndex_update_ui, "currentVesselPointIndex")
        self.add_property_handler(self.on_activeSegmentationVessel_update_ui, "activeSegmentationVessel")

    def on_unlink(self, propertyController):
        self.mouseEnabled = False
        pass

    def on_link(self, propertyController):
        self.mouseEnabled = True
        pass

    def on_database4D_update_ui(self, newValueForUI, propertyController):
        self.algorithm.setDataBase(newValueForUI)
        # if newValueForUI is not None:
        #     self.contentPanel.setMaximumSize(newValueForUI.get_x_size() + newValueForUI.get_z_size() + 20,
        #                                  newValueForUI.get_y_size() + newValueForUI.get_z_size() + 20)
        # if not self.isProcessing : self.invokeProcessing()

    def on_segmentationResult_update_ui(self, newValueForUI, propertyController):
        self.algorithm.snapshotDict = newValueForUI # only all data can be set at the same time
        self.refresh_a()

    def on_currentFrameIndex_update_ui(self, newValueForUI, propertyController):
        self.selected_frame_id = newValueForUI
        self.refresh_a()

    def on_segmentationSelectedVeselPoint_update_ui(self, newValueForUI, propertyController):
        self.selectedPoint = newValueForUI
        self.update()

    def on_displayResultPoints_update_ui(self, newValueForUI, propertyController):
        self.algorithm.drawVesselCenterVoxels = newValueForUI
        self.refresh_a()

    def on_displayVisitedVoxels_update_ui(self, newValueForUI, propertyController):
        self.algorithm.drawVisitedVoxels = newValueForUI
        self.refresh_a()

    def on_displayPrevSegmentation_update_ui(self, newValueForUI, propertyController):
        self.algorithm.prevResultCheck = newValueForUI
        self.refresh_a()

    def on_displayNextSegmentation_update_ui(self, newValueForUI, propertyController):
        self.algorithm.nextResultCheck = newValueForUI
        self.refresh_a()

    def on_displayReferenceSegmentation_update_ui(self, newValueForUI, propertyController):
        self.algorithm.referenceResultCheck = newValueForUI
        self.refresh_a()

    def on_showCalcifs_update_ui(self, newValueForUI, propertyController):
        self.algorithm.drawVesselCalcificationsVoxels = newValueForUI
        self.refresh_a()

    def on_currentVesselPointIndex_update_ui(self, newValueForUI, propertyController):
        if self.selectedPointIndex != newValueForUI:
            self.selectedPointIndex = newValueForUI
            vessel = self.activeSegmentationVessel
            if vessel:
                if vessel.activePointIndex != self.selectedPointIndex:
                    vessel.set_actve_point_index(self.selectedPointIndex)
                    point = vessel.get_active_point()
                    self.selectedPoint = point
                    self.on_get_value_from_UI(self.selectedPoint, "segmentationSelectedVeselPoint")

    def on_activeSegmentationVessel_update_ui(self, newValueForUI, propertyController):
        self.activeSegmentationVessel = newValueForUI
        self.refresh_a()

    def refresh_a(self):
        if not self.isProcessing :
            self.algorithm.refresh_view()

    def center_image_click(self, x, y):
        frameId = self.algorithm.selected_frame_id
        if not self.algorithm.snapshotDict.has_key(frameId): return
        result = self.algorithm.snapshotDict[frameId]
        if result == None: return
        # start
        # z = result.get_mip_z(x,y)
        frame3D = self.algorithm.data_base.get_frame(frameId)
        maxValue = -1
        index = None
        for key, value in result.voxelVisitedData.iteritems():
            if key[2]==int(x) and key[1]==int(y) and frame3D[key[0], key[1], key[2]]>maxValue:
                maxValue = frame3D[key[0], key[1], key[2]]
                index = key[0]
        z = index
        # end
        if z is not None:
            allResults = self.algorithm.snapshotDict
            if allResults:
                result3D = allResults[self.selected_frame_id]
                self.selectedPoint = result3D.get_segmentation_point_by_coordinates(int(z), int(y), int(x))
                self.on_get_value_from_UI(self.selectedPoint, "segmentationSelectedVeselPoint")
                vessel = result3D.get_vessel_by_point((int(z), int(y), int(x)))
                self.on_get_value_from_UI(vessel, "activeSegmentationVessel")
                pointIndex = vessel.get_segmentation_point_index_by_id(self.selectedPoint.pointId)
                self.on_get_value_from_UI(pointIndex, "currentVesselPointIndex")
        else :
            self.on_get_value_from_UI(None, "segmentationSelectedVeselPoint")
            self.on_get_value_from_UI(None, "activeSegmentationVessel")
        pass

    def right_image_click(self, x, y):
        frameId = self.algorithm.selected_frame_id
        if not self.algorithm.snapshotDict.has_key(frameId): return
        result = self.algorithm.snapshotDict[frameId]
        if result == None: return
        # start
        # xx = result.get_mip_x(y,x)
        frame3D = self.algorithm.data_base.get_frame(frameId)
        maxValue = -1
        index = None
        for key, value in result.voxelVisitedData.iteritems():
            if key[0]==int(x) and key[1]==int(y) and frame3D[key[0], key[1], key[2]]>maxValue:
                maxValue = frame3D[key[0], key[1], key[2]]
                index = key[2]
        xx = index
        # end
        if xx is not None:
            allResults = self.algorithm.snapshotDict
            if allResults:
                result3D = allResults[self.selected_frame_id]
                self.selectedPoint = result3D.get_segmentation_point_by_coordinates(int(x), int(y) ,xx)
                self.on_get_value_from_UI(self.selectedPoint, "segmentationSelectedVeselPoint")
                vessel = result3D.get_vessel_by_point((int(x), int(y) ,xx))
                self.on_get_value_from_UI(vessel, "activeSegmentationVessel")
                pointIndex = vessel.get_segmentation_point_index_by_id(self.selectedPoint.pointId)
                self.on_get_value_from_UI(pointIndex, "currentVesselPointIndex")
        else :
            self.on_get_value_from_UI(None, "segmentationSelectedVeselPoint")
            self.on_get_value_from_UI(None, "activeSegmentationVessel")
        pass

    def bottom_image_click(self, x, y):
        frameId = self.algorithm.selected_frame_id
        if not self.algorithm.snapshotDict.has_key(frameId): return
        result = self.algorithm.snapshotDict[frameId]
        if result == None: return
        # start
        # yy = result.get_mip_y(x,y)
        frame3D = self.algorithm.data_base.get_frame(frameId)
        maxValue = -1
        index = None
        for key, value in result.voxelVisitedData.iteritems():
            if key[0]==int(y) and key[2]==int(x) and frame3D[key[0], key[1], key[2]]>maxValue:
                maxValue = frame3D[key[0], key[1], key[2]]
                index = key[1]
        yy = index
        # end
        if yy is not None:
            allResults = self.algorithm.snapshotDict
            if allResults:
                result3D = allResults[self.selected_frame_id]
                self.selectedPoint = result3D.get_segmentation_point_by_coordinates(int(y),int(yy),int(x))
                self.on_get_value_from_UI(self.selectedPoint, "segmentationSelectedVeselPoint")
                vessel = result3D.get_vessel_by_point((int(y),int(yy),int(x)))
                self.on_get_value_from_UI(vessel, "activeSegmentationVessel")
                pointIndex = vessel.get_segmentation_point_index_by_id(self.selectedPoint.pointId)
                self.on_get_value_from_UI(pointIndex, "currentVesselPointIndex")
        else :
            self.on_get_value_from_UI(None, "segmentationSelectedVeselPoint")
            self.on_get_value_from_UI(None, "activeSegmentationVessel")
        pass

    def paint(self, painter, option, widget):
        image_result = self.displayResult
        xy0 = QtCore.QPoint(0, 0)
        xy1 = QtCore.QPoint(image_result[0].width() + 10, 0)
        xy2 = QtCore.QPoint(0, image_result[0].height() + 10)
        # #draw images
        painter.drawPixmap(xy0, image_result[0])
        painter.drawPixmap(xy1, image_result[2])
        painter.drawPixmap(xy2, image_result[1])

        # #draw selected vessel point
        if self.selectedPoint is not None:
            point = self.selectedPoint.int_point()
            painter.setPen(QtGui.QColor(250, 0, 0))
            painter.setRenderHint(QtGui.QPainter.Antialiasing)
            painter.drawEllipse(QtCore.QPoint(point.x, point.y), 2, 2)
            painter.drawEllipse(QtCore.QPoint(point.x+xy2.x(), point.z+xy2.y()), 2, 2)
            painter.drawEllipse(QtCore.QPoint(point.z+xy1.x(), point.y+xy1.y()), 2, 2)
        pass

    def on_convert_result(self, binaryResult):
        x0, y0 = binaryResult[0].shape
        img_xy = QtGui.QImage(binaryResult[0], x0, y0, QtGui.QImage.Format_RGB16)
        img_xy = QPixmap.fromImage(img_xy)

        x1, y1 = binaryResult[1].shape
        img_xz = QtGui.QImage(binaryResult[1], x1, y1, QtGui.QImage.Format_RGB16)
        img_xz = QPixmap.fromImage(img_xz)

        x2, y2 = binaryResult[2].shape
        img_yz = QtGui.QImage(binaryResult[2], x2, y2, QtGui.QImage.Format_RGB16)
        img_yz = QPixmap.fromImage(img_yz)

        #update segmentation result for core
        self.on_get_value_from_UI(self.algorithm.snapshotDict, "segmentationResult") # update result
        return (img_xy, img_xz, img_yz)


class MipPanel(UiPanel):
    '''Graphic MIP display panel.'''
    def __init__(self, mipAlgorithm):
        super(MipPanel, self).__init__(mipAlgorithm)
        self.algorithm.selected_mip_point = None
        self.algorithm.mipSelectionPointType = True
        self.algorithm.wl_level = 0
        self.algorithm.wh_level = 0
        self.add_property_handler(self.on_imageSeedPoint_update_ui, "imageSeedPoint")
        self.add_property_handler(self.on_database4D_update_ui, "database4D")
        self.add_property_handler(self.on_lastClickedImageNumber_update_ui, "lastClickedImageNumber")
        self.add_property_handler(self.on_mipSelectionPointType_update_ui, "mipSelectionPointType")
        self.add_property_handler(self.on_wl_level_update_ui, "wl_level")
        self.add_property_handler(self.on_wh_level_update_ui, "wh_level")
        self.add_property_handler(self.on_mipRemoveBackground_update_ui, "mipRemoveBackground")
        self.add_property_handler(self.on_currentFrameIndex_update_ui, "currentFrameIndex")

    def center_image_click(self, x, y):
        point = self.algorithm.get_mip_z(x, y)
        point = [point[0], point[1], point[2]] #we need table because tuples are immutable
        self.algorithm.selected_mip_point = point
        self.on_get_value_from_UI(point, "imageSeedPoint")
        self.on_get_value_from_UI(self.lastClickedImageNumber, "lastClickedImageNumber")

    def right_image_click(self, x, y):
        point = self.algorithm.get_mip_x(x, y)
        point = [point[0], point[1], point[2]]
        self.algorithm.selected_mip_point = point
        self.on_get_value_from_UI(point, "imageSeedPoint")
        self.on_get_value_from_UI(self.lastClickedImageNumber, "lastClickedImageNumber")

    def bottom_image_click(self, x, y):
        point = self.algorithm.get_mip_y(x, y)
        point = [point[0], point[1], point[2]]
        self.algorithm.selected_mip_point = point
        self.on_get_value_from_UI(point, "imageSeedPoint")
        self.on_get_value_from_UI(self.lastClickedImageNumber, "lastClickedImageNumber")



    def on_unlink(self, propertyController):
        self.mouseEnabled = False

    def on_link(self, propertyController):
        self.mouseEnabled = True

    def on_imageSeedPoint_update_ui(self, newValueForUI, propertyController):
        if newValueForUI is not None and self.algorithm.data_base != None:
            if self.algorithm.mipSelectionPointType:
                self.algorithm.selected_mip_point = newValueForUI
            else:
                if (self.lastClickedImageNumber==0 and self.get_mip_z(newValueForUI[2], newValueForUI[1])[0] == newValueForUI[0])\
                        or (self.lastClickedImageNumber==1 and self.get_mip_x(newValueForUI[0], newValueForUI[1])[2] == newValueForUI[2])\
                        or (self.lastClickedImageNumber==2 and self.get_mip_y(newValueForUI[2], newValueForUI[0])[1] == newValueForUI[1]):
                    self.selected_mip_point = newValueForUI
                else:
                    # do not display bad fake mip point mapped from slice
                    self.selected_mip_point = None
                # self.algorithm.selected_mip_point = newValueForUI
            self.update()

    def on_database4D_update_ui(self, newValueForUI, propertyController):
        self.algorithm.setDataBase(newValueForUI)
        self.algorithm.invokeProcessing()

    def on_lastClickedImageNumber_update_ui(self, newValueForUI, propertyController):
        self.lastClickedImageNumber = newValueForUI

    def on_mipSelectionPointType_update_ui(self, newValueForUI, propertyController):
        self.algorithm.mipSelectionPointType = newValueForUI

    def on_wl_level_update_ui(self, newValueForUI, propertyController):
        self.algorithm.wl_level = newValueForUI
        self.invoke_Result_stretching()

    def on_wh_level_update_ui(self, newValueForUI, propertyController):
        self.algorithm.wh_level = newValueForUI
        self.invoke_Result_stretching()

    def on_mipRemoveBackground_update_ui(self, newValueForUI, propertyController):
        self.algorithm.background_remove = newValueForUI
        self.algorithm.buffering_enabled = False
        self.algorithm.invokeProcessing()
        self.algorithm.buffering_enabled = True

    def on_currentFrameIndex_update_ui(self, newValueForUI, propertyController):
        self.algorithm.selected_frame_id = newValueForUI
        self.algorithm.invokeProcessing()


    def paint(self, painter, option, widget):
        '''Costum abstract method for drawing panel content
        :param context: Grapgics context, use it for drawing
        '''
        image_result = self.displayResult
        xy0 = QtCore.QPoint(0, 0)
        xy1 = QtCore.QPoint(image_result[0].width() + 10, 0)
        xy2 = QtCore.QPoint(0, image_result[0].height() + 10)
        # #draw images
        painter.drawPixmap(xy0.x(), xy0.y(), image_result[0])
        painter.drawPixmap(xy1.x(), xy1.y(), image_result[2])
        painter.drawPixmap(xy2.x(), xy2.y(), image_result[1])
        # #draw selected vessel point
        point = self.algorithm.selected_mip_point
        if point is not None:
            painter.setPen(QtGui.QColor(250, 0, 0))
            painter.setRenderHint(QtGui.QPainter.Antialiasing)
            painter.drawEllipse(QtCore.QPoint(point[2], point[1]), 1, 1)
            painter.drawEllipse(QtCore.QPoint(point[2]+xy2.x(), point[0]+xy2.y()), 1, 1)
            painter.drawEllipse(QtCore.QPoint(point[0]+xy1.x(), point[1]+xy1.y()), 1, 1)
        pass

    def on_convert_result(self, binaryResult):
        '''
        :param binaryResult: (pil image)
        :return: (qpixmap)
        '''
        a = np.asarray(binaryResult[0])
        x0, y0 = a.shape
        img_xy = QtGui.QImage(a, x0, y0, QtGui.QImage.Format_Indexed8)
        img_xy.setColorTable(ColorHelper.color_lokup_table)
        img_xy = QPixmap.fromImage(img_xy)

        b = np.asarray(binaryResult[1])
        x1, y1 = b.shape
        img_xz = QtGui.QImage(b, y1, x1, QtGui.QImage.Format_Indexed8)
        img_xz.setColorTable(ColorHelper.color_lokup_table)
        img_xz = QPixmap.fromImage(img_xz)

        c = np.asarray(binaryResult[2])
        x2, y2 = c.shape
        img_yz = QtGui.QImage(c, y2, x2, QtGui.QImage.Format_Indexed8)
        img_yz.setColorTable(ColorHelper.color_lokup_table)
        img_yz = QPixmap.fromImage(img_yz)
        return (img_xy, img_xz, img_yz)

    def on_data_stretching(self, result):
        '''
        Stretch data values to values from VL to VH.
        '''
        wl = self.algorithm.wl_level/16
        wh = self.algorithm.wh_level/16
        xy = self.imageStretching(result[0], wl, wh)
        xz = self.imageStretching(result[1], wl, wh)
        yz = self.imageStretching(result[2], wl, wh)
        return (xy, xz, yz)

    def imageStretching(self, img, wl, wh):
        x = PIL.Image.fromarray(img)
        x.mode = 'L'
        x = x.convert('F')
        x = ImageMath.eval("((a-b)/c)*255", a=x, b=float(wl), c=float(wh-wl))
        x = x.convert('L')
        return x


class SlicePanel(UiPanel):
    '''Panel class used for display database slices'''

    def __init__(self, sliceAlgorithm):
        super(SlicePanel, self).__init__(sliceAlgorithm)
        self.vesselCreator = None
        self.hintRadius = 4
        # list of explicit registered handling functions
        self.add_property_handler(self.on_imageSeedPoint_update_ui, "imageSeedPoint")
        self.add_property_handler(self.on_database4D_update_ui, "database4D")
        self.add_property_handler(self.on_wl_level_update_ui, "wl_level")
        self.add_property_handler(self.on_wh_level_update_ui, "wh_level")
        self.add_property_handler(self.on_currentFrameIndex_update_ui, "currentFrameIndex")
        self.add_property_handler(self.on_vesselCreator_update_ui, "vesselCreator")
        self.add_property_handler(self.on_displayVisitedVoxels_update_ui, "displayVisitedVoxels")
        self.add_property_handler(self.on_displayPrevSegmentation_update_ui, "displayPrevSegmentation")
        self.add_property_handler(self.on_displayNextSegmentation_update_ui, "displayNextSegmentation")
        self.add_property_handler(self.on_displayReferenceSegmentation_update_ui, "displayReferenceSegmentation")


    def on_unlink(self, propertyController):
        self.mouseEnabled = False
        pass

    def on_link(self, propertyController):
        self.mouseEnabled = True
        pass

    def on_imageSeedPoint_update_ui(self, newValueForUI, propertyController):
        self.algorithm.selected_slice_point = newValueForUI
        self.algorithm.invokeProcessing()

    def on_database4D_update_ui(self, newValueForUI, propertyController):
        self.algorithm.setDataBase(newValueForUI)
        self.algorithm.invokeProcessing()

    def on_wl_level_update_ui(self, newValueForUI, propertyController):
        self.algorithm.wl_level = newValueForUI
        self.invoke_Result_stretching()

    def on_wh_level_update_ui(self, newValueForUI, propertyController):
        self.algorithm.wh_level = newValueForUI
        self.invoke_Result_stretching()

    def on_currentFrameIndex_update_ui(self, newValueForUI, propertyController):
        self.algorithm.selected_frame_id = newValueForUI
        self.algorithm.invokeProcessing()

    def on_vesselCreator_update_ui(self, newValueForUI, propertyController):
        self.vesselCreator = newValueForUI
        self.update()

    def on_displayVisitedVoxels_update_ui(self, newValueForUI, propertyController):
        self.algorithm.drawVisitedVoxels = newValueForUI

    def on_displayPrevSegmentation_update_ui(self, newValueForUI, propertyController):
        self.algorithm.prevResultCheck = newValueForUI

    def on_displayNextSegmentation_update_ui(self, newValueForUI, propertyController):
        self.algorithm.nextResultCheck = newValueForUI

    def on_displayReferenceSegmentation_update_ui(self, newValueForUI, propertyController):
        self.algorithm.referenceResultCheck = newValueForUI

    def center_image_click(self, x, y):
        point = self.algorithm.selected_slice_point
        point[2] = x
        point[1] = y
        self.on_get_value_from_UI(point,"imageSeedPoint")

    def right_image_click(self, x, y):
        point = self.algorithm.selected_slice_point
        point[0] = x
        point[1] = y
        self.on_get_value_from_UI(point,"imageSeedPoint")

    def bottom_image_click(self, x, y):
        point = self.algorithm.selected_slice_point
        point[2] = x
        point[0] = y
        self.on_get_value_from_UI(point,"imageSeedPoint")

    def paint(self, painter, option, widget):
        image_result = self.displayResult
        if image_result is None:
            return
        img1x = 0
        img1y = 0
        img2x = image_result[0].width() + 10
        img2y = 0
        img3x = 0
        img3y = image_result[0].height() + 10
        #draw images
        painter.drawPixmap(img1x, img1y, image_result[0])
        painter.drawPixmap(img2x, img2y, image_result[2])
        painter.drawPixmap(img3x, img3y, image_result[1])
        #set color for all lines
        pen = QtGui.QPen(QtCore.Qt.red, 1, QtCore.Qt.SolidLine)
        painter.setPen(pen)
        z, y, x = self.algorithm.selected_slice_point
        #draw lines
        size1 = image_result[0].size()
        size2 = image_result[2].size()
        size3 = image_result[1].size()

        if self.contains(img1x, img1y, size1.width(), size1.height(), self.clickX, self.clickY) \
                or self.contains(img2x, img2y, size2.width(), size2.height(), self.clickX, self.clickY) \
                or self.contains(img3x, img3y, size3.width(), size3.height(), self.clickX, self.clickY):
            # if vessel creator is working draw
            # if self.vesselCreator is not None:
            #     xy0 = QtCore.QPoint(0, 0)
            #     xy1 = QtCore.QPoint(image_result[0].width() + 10, 0)
            #     xy2 = QtCore.QPoint(0, image_result[0].height() + 10)
            #     hints = self.vesselCreator.get_hint_points()
            #     if hints:
            #         for point in hints:
            #             #  draw hint points
            #             if point is not None:
            #                 qp.setPen(QtGui.QColor(250, 0, 0))
            #                 qp.setRenderHint(QtGui.QPainter.Antialiasing)
            #                 qp.drawEllipse(QtCore.QPoint(point[2], point[1]), self.hintRadius, self.hintRadius)
            #                 qp.drawEllipse(QtCore.QPoint(point[2]+xy2.x(), point[0]+xy2.y()), self.hintRadius, self.hintRadius)
            #                 qp.drawEllipse(QtCore.QPoint(point[0]+xy1.x(), point[1]+xy1.y()), self.hintRadius, self.hintRadius)
            #
            # qp.drawPoint()

            # vertical
            painter.drawLine(x+img1x, img1y, x+img1x, size1.height()+img1y)
            painter.drawLine(z+img2x, img2y, z+img2x, size2.height()+img2y)
            painter.drawLine(x+img3x, img3y, x+img3x, size3.height()+img3y)
            # horizontal
            painter.drawLine(img1x, y+img1y, size1.width()+img1x, y+img1y)
            painter.drawLine(img2x, y+img2y, size2.width()+img2x, y+img2y)
            painter.drawLine(img3x, z+img3y, size3.width()+img3x, z+img3y)
        pass

    def add_active_point(self):
        '''add active point from panel to new active vessel'''
        point = self.algorithm.selected_slice_point
        if point is None or not point: return
        self.vesselCreator.add_point(point[0], point[1], point[2])

    def on_mouse_wheel_move(self, point, event):
        '''mouse wheel event handling for images
        '''
        imageIndex = self.calculateImageIndex(point.x(), point.y())
        actualPoint = self.algorithm.selected_slice_point
        if imageIndex == 0:
            actualPoint[0] += event.delta()//120
            size = self.algorithm.data_base.get_z_size()-1
            if actualPoint[0]<0: actualPoint[0]=0
            elif actualPoint[0]>size: actualPoint[0]=size
        elif imageIndex == 1:
            actualPoint[2]+=event.delta()//120
            size = self.algorithm.data_base.get_x_size()-1
            if actualPoint[2]<0: actualPoint[2]=0
            elif actualPoint[2]>size: actualPoint[2]=size
        elif imageIndex == 2:
            actualPoint[1]+=event.delta()//120
            size = self.algorithm.data_base.get_y_size()-1
            if actualPoint[1]<0: actualPoint[1]=0
            elif actualPoint[1]>size: actualPoint[1]=size
        else:
            return True #enable scroll
        self.on_get_value_from_UI(actualPoint,"imageSeedPoint")

    def on_data_stretching(self, result):
        '''
        Stretch data values to values from VL to VH.
        '''
        wl = self.algorithm.wl_level/16
        wh = self.algorithm.wh_level/16
        xy = self.imageStretching(result[0], wl, wh)
        xz = self.imageStretching(result[1], wl, wh)
        yz = self.imageStretching(result[2], wl, wh)
        return (xy, xz, yz)

    def imageStretching(self, img, wl, wh):
        x = PIL.Image.fromarray(img)
        x.mode = 'L'
        x = x.convert('F')
        x = ImageMath.eval("((a-b)/c)*255", a=x, b=float(wl), c=float(wh-wl))
        x = x.convert('L')
        return x

    def on_convert_result(self, result):
        '''
        :param binaryResult: (pil image)
        :return: (qpixmap)
        '''
        a = np.asarray(result[0])
        x0, y0 = a.shape
        img_xy = QtGui.QImage(a, x0, y0, QtGui.QImage.Format_Indexed8)
        img_xy.setColorTable(ColorHelper.color_lokup_table)
        img_xy = QPixmap.fromImage(img_xy)

        b = np.asarray(result[1])
        x1, y1 = b.shape
        img_xz = QtGui.QImage(b, y1, x1, QtGui.QImage.Format_Indexed8)
        img_xz.setColorTable(ColorHelper.color_lokup_table)
        img_xz = QPixmap.fromImage(img_xz)

        c = np.asarray(result[2])
        x2, y2 = c.shape
        img_yz = QtGui.QImage(c, y2, x2, QtGui.QImage.Format_Indexed8)
        img_yz.setColorTable(ColorHelper.color_lokup_table)
        img_yz = QPixmap.fromImage(img_yz)
        return (img_xy, img_xz, img_yz)

