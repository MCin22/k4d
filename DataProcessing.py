import collections
import numpy as np
from DataController import AbstractAppController


__author__ = 'Adam'



class ProcessingAlgorithm(object):
    #__metaclass__ = ABCMeta
    def __init__(self):
        super(ProcessingAlgorithm, self).__init__()
        self.data_base = None
        self.snapshotDict = {} # on start is empty, no snapshots
        self.onSettingsChangeListener = None
        self.core = AbstractAppController.get_core_instance() # default core data access
        self.selected_frame_id = 0
        self.algorithmEnabled = True

    def invokeProcessing(self):
        ''' Run processing and invoke listener when finished.'''
        if self.algorithmEnabled == False:
            return
        if(self.data_base == None):
            # clear algorithm cache
            self.snapshotDict = {}
            return
            #raise Exception('database_4D is None, insert instance of Database_4D')
        self.onStart()
        binaryResult = self.onProcessData(self.data_base, self.snapshotDict)
        if binaryResult is not None:
            self.on_processing_finish(self.snapshotDict, binaryResult)

    #method for external use as callback
    def onStart(self):
        '''when we want to start processing'''
        pass

    #@abstractmethod
    def onProcessData(self, database_4D, snapshotDict):
        '''Process data using database_4D data, properties - settings for algorithm,
           and snapshotManager for save metadata
        '''
        pass

    #@abstractmethod - method for external use as callback
    def on_processing_finish(self, snapshotDict, result):
        '''When processing is finished'''
        pass

    def setDataBase(self, database):
        '''Set current readable database, clear cashed data.'''
        self.data_base = database
        self.snapshotDict = {}  # clear cashed data
        if self.onSettingsChangeListener != None : self.onSettingsChangeListener(self) # notify about settings change

    def set_current_frame(self, frameIndex):
        self.selected_frame_id = frameIndex

    def set_algorithm_enabled(self, isTrue):
        '''enable invoke processing action or dissable all calls to it'''
        self.algorithmEnabled = isTrue

class Point(object):
    def __init__(self, z=0, y=0, x=0):
        super(Point, self ).__init__()
        self.x = x
        self.y = y
        self.z = z

    def equals(self, p):
        return self.x == p.x and self.y == p.y and self.z == p.z

    def int_point(self):
        return Point(int(self.z), int(self.y), int(self.x))

    def __str__(self):
         return "x: ",self.x, "y: ", self.y, "z: ",self.z
    def __eq__(self,obj):  #IMPORTANT to 'contains' working !!!
         return isinstance(obj,Point) and obj.x==self.x and obj.y==self.y and obj.z==self.z

class VesselPoint(Point):
    generatePointId = 0
    def __init__(self, z=0, y=0, x=0):
        super(VesselPoint, self).__init__(z, y, x)
        VesselPoint.generatePointId +=1
        self.pointId = VesselPoint.generatePointId
        self.radius = 0
        self.vessel_level = 0
        self.bg_level = 0
        self.alpha = 0
        self.beta = 0
        self.mask_size = 0

    def __str__(self):
         return "x: ",self.x, " y: ", self.y, " z: ",self.z, " alpha: ",self.alpha," beta: ",self.beta


class VesselBranch(object):
    def __init__(self):
        super(VesselBranch, self).__init__()
        self.activePointIndex = 0
        self.isSelected = False
        self.name = None
        self.vesselId = -1 # generated value, if after generation -1 than is error or vessel branch not added to result 3D
        self.parent = None #parent vessel branch
        self.children = []
        self.parentResult = None # parent vessel result set
        self.pointList = []
        self.lastAddedSegmentationPoint = None


    def set_calcification_voxel(self, z, y, x):
        self.parentResult.calcification_voxels_map[(z,y,x)] = self.vesselId

    def add_child(self, child):
        '''
        adds child vessel branch to parent vessel branch and set parent as root of child
        '''
        self.children.append(child)
        child.parent = self

    def set_actve_point_index(self, index):
        '''
        :param index: index of segmentation point in this vessel
        :return:
        '''
        if index<0:
            self.activePointIndex = 0
        elif index >= len(self.pointList):
            self.activePointIndex = len(self.pointList)-1
        else:
            self.activePointIndex = index

    def set_next_actve_point(self):
        if self.activePointIndex+1 >= len(self.pointList):
            self.activePointIndex = len(self.pointList)-1
        else:
            self.activePointIndex = self.activePointIndex+1

    def set_previous_actve_point(self):
        if self.activePointIndex-1 <0 :
            self.activePointIndex = 0
        else:
            self.activePointIndex = self.activePointIndex-1

    def get_active_point(self):
        if not self.pointList:
            return None
        return self.pointList[self.activePointIndex]


    def add_first(self, point):
        '''add point to vessel branch on begining, only possible if vessel branch added to result 3D'''
        self.lastAddedSegmentationPoint = point
        self.pointList.insert(0, point)


    def add_last(self, point):
        '''add point to vessel branch on the end, only possible if vessel branch added to result 3D'''
        self.lastAddedSegmentationPoint = point
        self.pointList.append(point)

    def get_last(self):
        if not self.pointList: return None
        return self.pointList[-1]

    def get_first(self):
        if not self.pointList: return None
        return self.pointList[0]

    def mark_point(self, z, y, x):
        self.parentResult.voxelVisitedData[(z, y, x)] = (self.vesselId, self.lastAddedSegmentationPoint.pointId)

    def contains(self, point):
        return (point.z, point.y, point.x) in self.parentResult.voxelVisitedData

    def get_segmentation_point_by_id(self, pointId):
        for point in self.pointList:
            if point.pointId == pointId:
                return point
        return None

    def get_segmentation_point_index_by_id(self, pointId):
        index = 0
        for point in self.pointList:
            if point.pointId == pointId:
                return index
            index+=1
        return None

    def clear_vessel_data(self):
        # clear visited data
        for key, val in self.parentResult.voxelVisitedData.iteritems():
            if val[0] == self.vesselId:
                del self.parentResult.voxelVisitedData[key]
        # clear segmentation points
        self.pointList = []
        # clear calcifications
        clacificationsToRemove = []
        for key, val in self.parentResult.calcification_voxels_map.iteritems():
            if val == self.vesselId:
                clacificationsToRemove.append(key)
        for val in clacificationsToRemove:
            del self.parentResult.calcification_voxels_map[val]

    def invert_points(self):
        self.pointList = self.pointList[::-1]

# class Calcification(object):
#     def __init__(self):
#         super(Calcification, self ).__init__()
#         self.max_x = self.max_y = self.max_z = -1
#         self.min_x = self.min_y = self.min_z = np.inf
#         self.voxels = [] #?


class SegmentationResult3D(object):
    def __init__(self, frameIndex):
        super(SegmentationResult3D, self).__init__()
        self.frame3DIndex = frameIndex
        self.calcification_voxels_map = {}   #key= Z, Y, X  value = id
        self.generateId = 0 # generate ids for vessels (start from 0)
        self.vesselTree = {} #dictionary containing all vessels
        self.voxelVisitedData = collections.OrderedDict() # all voxels belonging to all vessels (visited) Z,Y,X

    def get_calcifications_ndarray_in_range(self, minZ, maxZ, minY, maxY, minX, maxX):
        '''
        creates and returns ndaray with params specified size and correct params range.
        maxX, maxY, maxZ are not included !!!
        values are always 0 or 1   and dimensions of table (z,y,x)
        :return:
        '''
        lenZ = maxZ-minZ
        lenY = maxY-minY
        lenX = maxX-minX
        ndArray = np.zeros((lenZ, lenY, lenX))
        for z in range(0, lenZ):
            for y in range(0, lenY):
                for x in range(0, lenX):
                    if self.calcification_voxels_map.has_key((z+minZ,y+minY,x+minX)):
                        ndArray[z, y, x] = 1
        return ndArray

    def is_calcification_voxel(self, z, y, x):
        return self.calcification_voxels_map.has_key((z,y,x))

    def contains(self, z, y, x):
        '''Check if segmentation result contains specified point. Return True or False.'''
        return self.voxelVisitedData.has_key((z, y, x))

    def contains_point(self, point):
        '''Check if segmentation result (visited voxels) contains specified point. Return True or False.'''
        if point is None: return
        return self.voxelVisitedData.has_key((point.z, point.y, point.x))

    def get_value(self, pointVector):
        '''Returns value for point with specified coordinates 3D or null'''
        return self.voxelVisitedData[pointVector]

    def add_vessel(self, vessel):
        '''Add new vesel to this segmentation 3D database result and generate default name'''
        #generate unique name for veselbranch
        self.generateId += 1
        generatedName = "vessel_"+str(self.generateId)
        vessel.name = generatedName
        vesselId = self.generateId
        vessel.vesselId = vesselId
        vessel.parentResult = self
        self.vesselTree[vesselId] = vessel

    def get_vessel_by_id(self, id):
        '''returns vessel object marked by speified id'''
        if self.vesselTree.has_key(id): return self.vesselTree[id]
        return None

    def get_vessel_id(self, vesselPoint):
        '''returns vessel id behind specified point in 3D space. point is tuple(z,y,x)'''
        return self.voxelVisitedData[vesselPoint][0]

    def get_vessel_by_point(self, vesselPoint):
        '''Returns vessel object behind specified point in 3D space, or none if not in vessel, point is tuple(z,y,x)'''
        vessel_id = self.get_vessel_id(vesselPoint)
        if vessel_id == None: return None
        return self.get_vessel_by_id(vessel_id)

    def remove_vessel_by_id(self, id):
        '''
        :param id: remove vessel and all children
        '''
        vessel = self.get_vessel_by_id(id)
        if vessel.children:
            for child in vessel.children:
                self.remove_vessel_by_id(child.vesselId)
        vessel.clear_vessel_data()
        del self.vesselTree[id]

    def merge_vessels_by_id(self, idList):
        '''
        :param idList: list of vessel ids to merge
        '''
        newCommonId = idList[0]
        # move voxel data to first vessel
        for i in range(1, len(idList)):
            id = idList[i]
            for key, value in self.voxelVisitedData.iteritems():
                 if value[0] == id:
                    self.voxelVisitedData[key] = (newCommonId, value[1])
        # move list point to first vessel
        firstVessel = self.get_vessel_by_id(idList[0])
        for i in range(1, len(idList)):
            vesselToExtract = self.get_vessel_by_id(idList[i])
            firstVessel.pointList.extend(vesselToExtract.pointList)
            #move children vessels objects
            firstVessel.children += vesselToExtract.children
            vesselToExtract.children = []
        # delete empty objects
        for i in range(1, len(idList)):
            self.remove_vessel_by_id(idList[i])

    def cut_vessel_by_point(self, cutPoint):
        '''
        :param point: segmentation point, where the vessel will be cut
        '''
        intPoint = cutPoint
        vessel = self.get_vessel_by_point((intPoint.z, intPoint.y, intPoint.x))
        newVessel = VesselBranch()
        self.add_vessel(newVessel)
        # data from 0 to vessel cut point will be moved to new vessel
        for i in range(0, vessel.activePointIndex):
            segmentationPoint = vessel.pointList[i]
            for key, value in self.voxelVisitedData.iteritems():
                if value[1] == segmentationPoint.pointId:
                    # when point to move
                    self.voxelVisitedData[key] = (newVessel.vesselId, value[1])
            newVessel.add_last(segmentationPoint)
        # remove points from old vessel
        vessel.pointList = vessel.pointList[vessel.activePointIndex : len(vessel.pointList)]
        pass

    def get_segmentation_point_by_coordinates(self, z, y, x):
        '''Get segmentation point for specified cordinates'''
        voxelData = self.voxelVisitedData[(z, y, x)]
        vessel = self.vesselTree[voxelData[0]]
        return vessel.get_segmentation_point_by_id(voxelData[1])


class SegmentationAlgorithm(ProcessingAlgorithm):
     def __init__(self):
        super(SegmentationAlgorithm, self ).__init__()
        # read masques
        OPER_COUNT = 12 # max. value: 16, max. radius of masque
        self.masks = []
        for radius in range(1, OPER_COUNT + 1):
            size = 2 * radius + 1
            path = 'masque/masque.{0}'.format(size)
            dt = np.dtype(np.float)
            masques=np.fromfile(path, dtype=dt).reshape(10, size, size, size)
            masques_copy=np.copy(masques)
            # w pliku maski w porzadku x-y-z !!! - proba zmiany
            for m in range(10):
                for x in range(size):
                    for y in range(size):
                        for z in range(size):
                            masques[m][x][y][z]=masques_copy[m][z][y][x]
            self.masks.append(masques)
        self.init_sin_cos()
        self.size_x = 0
        self.size_y = 0
        self.size_z = 0
        self.bpv = 16# bits_per_voxel
        self.useCache = False
        self.drawVisitedVoxels = True
        self.drawVesselCenterVoxels = False
        self.drawVesselCalcificationsVoxels = False
        self.prevResultCheck = True
        self.nextResultCheck = True
        self.referenceResultCheck = True
        self.recalculateViewOnly = True
        self.data = None

     def refresh_view(self):
        '''if we dont want recalculate algorithm and add new wessel'''
        self.recalculateViewOnly = True
        self.invokeProcessing()
        self.recalculateViewOnly = False

     def onProcessData(self, database4D, snapshotManager):
         frameIndex = self.core.get_property_value('currentFrameIndex')
         frame3D = database4D.get_frame(frameIndex)

         if self.recalculateViewOnly == False:
             if not snapshotManager.has_key(frameIndex):
                 # calculate first time
                 result = SegmentationResult3D(frameIndex) #create new result
                 snapshotManager[frameIndex] = self.segmentation_impl(frameIndex, database4D, self.core, result)
             else:
                 # calculate more times
                 if self.useCache == True:
                    #  use existing result from cache
                    result = snapshotManager[frameIndex]
                 else:
                    snapshotManager[frameIndex] = self.segmentation_impl(frameIndex, database4D, self.core, snapshotManager[frameIndex])

         # calculate images bounds
         ix = (database4D.get_x_size()+3)//4*4
         iy = (database4D.get_y_size()+3)//4*4
         iz = (database4D.get_z_size()+3)//4*4

         # create empty images
         nd_array_xy = np.zeros((ix*iy,), np.uint16)  # only in this form it working correctly, do not use array 2D
         nd_array_xz = np.zeros((ix*iz,), np.uint16)
         nd_array_yz = np.zeros((iz*iy,), np.uint16)

         # get result
         if snapshotManager.has_key(frameIndex):
             result = snapshotManager[frameIndex]
         else:
             result = None

         # get previous result
         prevResult = None
         if snapshotManager.has_key(frameIndex - 1) and self.prevResultCheck:
             prevResult = snapshotManager[frameIndex - 1]

         # get next result
         nextResult = None
         if snapshotManager.has_key(frameIndex + 1) and self.nextResultCheck:
             nextResult = snapshotManager[frameIndex + 1]

         # get reference result
         referenceResult = None
         referenceIndex = self.core.get_property_value('referenceFrameIndex')
         if referenceIndex is not None:
            if snapshotManager.has_key(referenceIndex) and self.referenceResultCheck:
                referenceResult = snapshotManager[referenceIndex]

         if prevResult is not None:
             if self.drawVisitedVoxels == True: # if algorithm not fail
                 # map data to images
                 pointColorValue = self.core.get_property_value("SegmentationPrevVoxelsValue")
                 for key, vesselId in prevResult.voxelVisitedData.iteritems():
                     if vesselId[0]<0: continue # paint only vessel voxels, no calcifications id < 0
                     vessel = prevResult.get_vessel_by_id(vesselId[0])
                     if vessel is None : continue
                     nd_array_xy[key[2]+ix*key[1]] = pointColorValue
                     nd_array_xz[key[2]+ix*key[0]] = pointColorValue
                     nd_array_yz[key[0]+iz*key[1]] = pointColorValue
             # points version
             if self.drawVesselCenterVoxels == True: # if algorithm not fail
                  # map data to images
                  pointColorValue = self.core.get_property_value("SegmentationPrevPointsValue")
                  for vesselId, vessel1 in prevResult.vesselTree.iteritems():
                      if vesselId<0: continue # paint only vessel voxels, no calcifications id < 0
                      for point in vessel1.pointList:
                         nd_array_xy[point.x+ix*point.y] = pointColorValue
                         nd_array_xz[point.x+ix*point.z] = pointColorValue
                         nd_array_yz[point.z+iz*point.y] = pointColorValue

         if nextResult is not None:
             if self.drawVisitedVoxels == True: # if algorithm not fail
                 # map data to images
                 pointColorValue = self.core.get_property_value("SegmentationNextVoxelsValue")
                 for key, vesselId in nextResult.voxelVisitedData.iteritems():
                     if vesselId[0]<0: continue # paint only vessel voxels, no calcifications id < 0
                     vessel = nextResult.get_vessel_by_id(vesselId[0])
                     if vessel is None : continue
                     nd_array_xy[key[2]+ix*key[1]] = pointColorValue
                     nd_array_xz[key[2]+ix*key[0]] = pointColorValue
                     nd_array_yz[key[0]+iz*key[1]] = pointColorValue
             # points version
             if self.drawVesselCenterVoxels == True: # if algorithm not fail
                  # map data to images
                  pointColorValue = self.core.get_property_value("SegmentationNextPointsValue")
                  for vesselId, vessel1 in nextResult.vesselTree.iteritems():
                      if vesselId<0: continue # paint only vessel voxels, no calcifications id < 0
                      for point in vessel1.pointList:
                         nd_array_xy[point.x+ix*point.y] = pointColorValue
                         nd_array_xz[point.x+ix*point.z] = pointColorValue
                         nd_array_yz[point.z+iz*point.y] = pointColorValue

         if referenceResult is not None:
             if self.drawVisitedVoxels == True: # if algorithm not fail
                 # map data to images
                 pointColorValue = self.core.get_property_value("SegmentationReferenceVoxelsValue")
                 for key, vesselId in referenceResult.voxelVisitedData.iteritems():
                     if vesselId[0]<0: continue # paint only vessel voxels, no calcifications id < 0
                     vessel = referenceResult.get_vessel_by_id(vesselId[0])
                     if vessel is None : continue
                     nd_array_xy[key[2]+ix*key[1]] = pointColorValue
                     nd_array_xz[key[2]+ix*key[0]] = pointColorValue
                     nd_array_yz[key[0]+iz*key[1]] = pointColorValue
             # points version
             if self.drawVesselCenterVoxels == True: # if algorithm not fail
                  # map data to images
                  pointColorValue = self.core.get_property_value("SegmentationReferencePointsValue")
                  for vesselId, vessel1 in referenceResult.vesselTree.iteritems():
                      if vesselId<0: continue # paint only vessel voxels, no calcifications id < 0
                      for point in vessel1.pointList:
                         nd_array_xy[point.x+ix*point.y] = pointColorValue
                         nd_array_xz[point.x+ix*point.z] = pointColorValue
                         nd_array_yz[point.z+iz*point.y] = pointColorValue

         if result is not None:
             if self.drawVisitedVoxels == True: # if algorithm not fail
                 # map data to images
                 selectedPointColorValue = self.core.get_property_value("SegmentationSelectedVoxelsValue")
                 pointColorValue = self.core.get_property_value("SegmentationVoxelsValue")
                 for key, vesselId in result.voxelVisitedData.iteritems():
                     if vesselId[0]<0: continue # paint only vessel voxels, no calcifications id < 0
                     vessel = result.get_vessel_by_id(vesselId[0])
                     if vessel is None : continue
                     if vessel.isSelected :    # selected voxels, paint on different color
                         nd_array_xy[key[2]+ix*key[1]] = selectedPointColorValue
                         nd_array_xz[key[2]+ix*key[0]] = selectedPointColorValue
                         nd_array_yz[key[0]+iz*key[1]] = selectedPointColorValue
                     else :
                         nd_array_xy[key[2]+ix*key[1]] = pointColorValue
                         nd_array_xz[key[2]+ix*key[0]] = pointColorValue
                         nd_array_yz[key[0]+iz*key[1]] = pointColorValue
             # points version
             if self.drawVesselCenterVoxels == True: # if algorithm not fail
                  # map data to images
                  pointColorValue = self.core.get_property_value("SegmentationPointsValue")
                  for vesselId, vessel1 in result.vesselTree.iteritems():
                      if vesselId<0: continue # paint only vessel voxels, no calcifications id < 0
                      for point in vessel1.pointList:
                         nd_array_xy[point.x+ix*point.y] = pointColorValue
                         nd_array_xz[point.x+ix*point.z] = pointColorValue
                         nd_array_yz[point.z+iz*point.y] = pointColorValue
             # calcifications
             if self.drawVesselCalcificationsVoxels == True: # if algorithm not fail
                  pointColorValue = self.core.get_property_value("SegmentationCalcificationsValue")
                  for key, calcId in result.calcification_voxels_map.iteritems():
                      nd_array_xy[key[2]+ix*key[1]] = pointColorValue
                      nd_array_xz[key[2]+ix*key[0]] = pointColorValue
                      nd_array_yz[key[0]+iz*key[1]] = pointColorValue

         # set dimensions for images
         nd_array_xy = nd_array_xy.reshape((ix, iy))
         nd_array_xz = nd_array_xz.reshape((ix, iz))
         nd_array_yz = nd_array_yz.reshape((iz, iy))
         return (nd_array_xy, nd_array_xz, nd_array_yz)


     def in_vessel_voxel(self, Pim1, rim1, Pi, ri, P):
         V_Pim1_Pi=[ Pi[i]-Pim1[i] for i in range(3) ]
         V_Pim1_P = [ P[i]-Pim1[i] for i in range(3) ]
         V_Pi_P = [ P[i]-Pi[i] for i in range(3) ]

         square_len_V_Pim1_P = V_Pim1_P[0]*V_Pim1_P[0] + V_Pim1_P[1]*V_Pim1_P[1] + V_Pim1_P[2]*V_Pim1_P[2]
         #if square_len_V_Pim1_P <= rim1*rim1: return True # point P in distance <= rim1 from Pim1

         square_len_V_Pi_P = V_Pi_P[0]*V_Pi_P[0] + V_Pi_P[1]*V_Pi_P[1] + V_Pi_P[2]*V_Pi_P[2]
         #if square_len_V_Pi_P <= ri*ri: return True # point P in distance <= ri from Pi

         # scalar_prod = V_Pi_Pim1 * V_P_Pim1 = |V_Pi_Pim1| |V_P_Pim1| cos <(V_Pi_Pim1,V_Pi_Pim1)
         scalar_prod = V_Pim1_Pi[0]*V_Pim1_P[0] + V_Pim1_Pi[1]*V_Pim1_P[1] + V_Pim1_Pi[2]*V_Pim1_P[2]
         #print scalar_prod
         # proj_distance = cos< * | V_Pim1_P | = scalar_prod / (|V_Pim1_Pi| |V_Pim1_P|) * |V_Pim1_P | = scalar_prod / | V_Pim1_Pi |
         # proj_distance_0_1 = proj_distance / | V_Pim1_Pi | = scalar_prod / | V_Pim1_Pi |^2
         square_len_V_Pim1_Pi = V_Pim1_Pi[0]*V_Pim1_Pi[0] + V_Pim1_Pi[1]*V_Pim1_Pi[1] + V_Pim1_Pi[2]*V_Pim1_Pi[2]

         #print square_len_V_Pim1_Pi
         proj_distance_0_1 = 1.0 * scalar_prod / square_len_V_Pim1_Pi
         if proj_distance_0_1 < 0: return False # projection not between Pim1 and Pi
         if proj_distance_0_1 > 1: return False # projection not between Pim1 and Pi

         Pproj=[ Pim1[i] + (Pi[i]-Pim1[i]) * proj_distance_0_1 for i in range(3) ]

         V_Pproj_P = [ P[i] - Pproj[i] for i in range(3) ]
         square_len_V_Pproj_P =  V_Pproj_P[0]*V_Pproj_P[0] + V_Pproj_P[1]*V_Pproj_P[1] + V_Pproj_P[2]*V_Pproj_P[2]

         rproj = rim1+(ri-rim1)*proj_distance_0_1
         if square_len_V_Pproj_P <= rproj*rproj: return True
         return False


     def segmentation_impl(self, frameIndex, database4D, coreInstance, result):
         '''Method invoked when we need recalculate segmentation for single frame 3D.'''

         # prepare data
         frame3D = database4D.get_frame(frameIndex)
         self.data = frame3D
         self.size_x = database4D.get_x_size()
         self.size_y = database4D.get_y_size()
         self.size_z = database4D.get_z_size()
         self.bpv = database4D.get_bits_per_voxel()

         z, y, x = coreInstance.get_property_value("imageSeedPoint")
         start_point = Point(z, y, x)
         parameters = {}
         parameters["adapting_vessel_level"] = coreInstance.get_property_value("adapting_vessel_level")
         parameters["adapting_bg_level"] = coreInstance.get_property_value("adapting_bg_level")
         parameters["calcification_removing"] = coreInstance.get_property_value("calcification_removing")
         parameters["adapting_window_size"] = coreInstance.get_property_value("adapting_window_size")
         parameters["initial_bg_level"] = coreInstance.get_property_value("initial_bg_level")
         parameters["max_vessel_level"] = coreInstance.get_property_value("max_vessel_level")
         parameters["initial_mask_size"] = coreInstance.get_property_value("initial_mask_size")
         parameters["initial_vessel_level"] = coreInstance.get_property_value("initial_vessel_level")
         parameters["min_vessel_level"] = coreInstance.get_property_value("min_vessel_level")
         parameters["step_factor"] = coreInstance.get_property_value("step_factor")
         parameters["frame_3D_index"] = frameIndex

         #parameters["mean_threshold"] = 1100
         #parameters["variance_threshold"] = 1100
         result = self.track_vessel(start_point, parameters, result)
         return result


     def track_vessel(self, startPoint, parameters, result):
         '''
          fr: suivi_vais
         :param startPoint: Point() object
         :param parameters:
         :param result:
         :return:
         '''
         # startPoint.x = 113
         # startPoint.y = 216
         # startPoint.z = 72

         if startPoint.x==0 or startPoint.y==0 or startPoint.z==0:
             return result

         parameters["mean_threshold"], parameters["variance_threshold"] = self.estimate_threshold(parameters["initial_vessel_level"], parameters["initial_bg_level"])
                 # create new vessel branch in current result
         vesselBranch = VesselBranch() #add data points to this vessel
         result.add_vessel(vesselBranch) # add vessels to this result (only one per track_vessel invocation)
         if self.in_vessel_check(startPoint, parameters["initial_mask_size"], parameters["initial_bg_level"], parameters) is False:
             result.remove_vessel_by_id(vesselBranch.vesselId) # remove if empty
             return result # return start state result because this point is not in vessel
         start_vessel_point = self.estimate_vessel_cylinder(startPoint, parameters["initial_mask_size"], parameters["initial_vessel_level"], parameters["initial_bg_level"], parameters, vesselBranch, result)
         # check if this point belongs already to segmented vessel
         if start_vessel_point is None or result.contains_point(start_vessel_point):
             result.remove_vessel_by_id(vesselBranch.vesselId) # remove if empty
             return result

         # add start point to vessel branch
         vesselBranch.add_first(start_vessel_point)#result.vessel_branch

         print " >>> Point added ", str(start_vessel_point.x)," ",str(start_vessel_point.y)," ",str(start_vessel_point.z)," ",str(start_vessel_point.alpha)," ",str(start_vessel_point.beta)

         pointCounter = 0
         for direction in [-1, 1]: #tracking in two opposing directions
            previous_point = start_vessel_point
            current_mask_size = start_vessel_point.mask_size
            current_vessel_level = start_vessel_point.vessel_level
            current_bg_level = start_vessel_point.bg_level
            while True:
                # calculate next step
                step = current_mask_size * parameters["step_factor"]
                # get next point candidate
                prev_prev_point=None
                if len(vesselBranch.pointList)>1:
                    if direction<0:
                        if len(vesselBranch.pointList)>1:
                            prev_prev_point=vesselBranch.pointList[1]
                        else:
                            prev_prev_point=vesselBranch.pointList[-2]
                next_point_candidate = self.select_next_point(previous_point, step, direction,prev_prev_point)

                # TEMPORARY!!!!!!!!!!!
                parameters["mean_threshold"], parameters["variance_threshold"] = self.estimate_threshold(current_vessel_level, current_bg_level)

                # if not in vessel continue tracking another point
                # if not self.in_vessel_check(next_point_candidate, current_mask_size, current_bg_level, parameters):
                #     break

                # calculate next point parameters and additional data
                extracted_vessel_point = self.estimate_vessel_cylinder(next_point_candidate, current_mask_size, current_vessel_level, current_bg_level, parameters, vesselBranch, result, previous_point)

                if extracted_vessel_point is None:
                    break

                # check if the branch already contains extracted vessel point (it's temporary solution)
                if vesselBranch.contains(extracted_vessel_point):
                    break

                # add next vessel point to end or begin of vessel
                if direction < 0:
                    self.mark_visited(extracted_vessel_point, vesselBranch.get_first(), vesselBranch)
                    vesselBranch.add_first(extracted_vessel_point)
                else:
                    self.mark_visited(extracted_vessel_point, vesselBranch.get_last(), vesselBranch)
                    vesselBranch.add_last(extracted_vessel_point)
                # save current point values for calculate next point
                current_mask_size = extracted_vessel_point.mask_size
                current_vessel_level = extracted_vessel_point.vessel_level
                current_bg_level = extracted_vessel_point.bg_level
                previous_point = extracted_vessel_point
                if not extracted_vessel_point is None: 
                    print " >>> Point added x=", str(int(extracted_vessel_point.dx*100)/100.0)," y=",str(int(extracted_vessel_point.dy*100)/100.0)," z=",str(int(extracted_vessel_point.dz*100)/100.0)," r=",str(int(extracted_vessel_point.radius*1000)/1000.0)," a=",str(extracted_vessel_point.alpha)," b=",str(extracted_vessel_point.beta), "VL=",str(extracted_vessel_point.vessel_level)," BL=",str(extracted_vessel_point.bg_level), "mask=",str(extracted_vessel_point.mask_size)
         return result

     def mark_visited(self, pointA, pointB, vessel):
         '''
         Mark all visited points in cylinder between two given points
         :param pointA:
         :param pointB:
         :param vessel:
         :return:
         '''
         if pointA is None or pointB is None:
             return
         maxRadius = pointA.radius
         if maxRadius < pointB.radius:
             maxRadius = pointB.radius
         xMin, xMax = self.calculate_range(pointA.x, pointB.x, maxRadius)
         yMin, yMax = self.calculate_range(pointA.y, pointB.y, maxRadius)
         zMin, zMax = self.calculate_range(pointA.z, pointB.z, maxRadius)
         for x in range(xMin, xMax):
             for y in range(yMin, yMax):
                 for z in range(zMin, zMax):
                    if self.in_vessel_voxel((pointA.dx,pointA.dy,pointA.dz),pointA.radius, (pointB.dx,pointB.dy,pointB.dz), pointB.radius, (x,y,z)) == True:
                        vessel.mark_point(z,y,x)
         pass

     def calculate_range(self, valA, valB, maxRadius):
         ''' helper method for mark_visited
         :param valA:
         :param valB:
         :param maxRadius:
         :return:
         '''
         if  valA < valB:
             min = valA - maxRadius
             max = valB + maxRadius
         else:
             min = valB - maxRadius
             max = valA + maxRadius
         return (int(min), int(max+1))

     def estimate_vessel_cylinder(self, point, mask_size, vessel_level, bg_level, parameters, vesselBranch, result, previous_point=None):  # fr: estim_cyl, calculates parameters of the vessel cylinder (center point, 3D orientation, radius, vessel and BG levels)
        if parameters["calcification_removing"]: #calculate voxels belonging to calcification
            while True:
                #if point is in calcifications do nothing on calcifications
                #if result.is_calcification_voxel(point.z, point.y, point.x):#code added
                #    break
                calcification_seed_point = self.search_calcification_seed_point(point, 1.5*mask_size, parameters["max_vessel_level"], result)
                if calcification_seed_point is None:
                    break
                # while we have calcification
                # calcification = self.extract_calcification(calcification_seed_point, 1.5*mask_size, parameters["max_vessel_level"], result)
                self.extract_calcification(calcification_seed_point, 1.5*mask_size, parameters["max_vessel_level"],vesselBranch, result)
                # add voxel data to calcifications
                # for v in calcification.voxels:
                #     vesselBranch.set_calcification_voxel(v.z, v.y, v.x)
                #break - can be more than 1 calc in neighborhood


        #prepare initial mask radius and cylinder values for data to return
        mask_radius = mask_size / 2.0
        cylinder = VesselPoint()
        cylinder.bg_level = bg_level #set initial background level (could be changed during recalculate window pass)
        cylinder.vessel_level = vessel_level # set initial vessel level (could be changed during recalculate window pass)
        cylinder.mask_size = mask_size

        recalculate_window = True
        examined_masks=[mask_size]
        while recalculate_window:   # adaptation of mask size
            old_point = point # only start window recalculating from the same start point

            # mask centerage
            moments = self.calculate_moments(point.int_point(), vessel_level, bg_level, mask_size, result)
            if moments is None or moments[0]==0:
                return None
            dx = mask_radius * moments[1] / moments[0]
            dy = mask_radius * moments[2] / moments[0]
            dz = mask_radius * moments[3] / moments[0]


            shift = np.sqrt(dx*dx + dy*dy + dz*dz)
            while True:     # centrage of the vessel point
                old_shift = shift
                candidate_point = VesselPoint()
                candidate_point.x = old_point.x + int(dx + (0.7 * np.sign(dx)))
                candidate_point.y = old_point.y + int(dy + (0.7 * np.sign(dy)))
                candidate_point.z = old_point.z + int(dz + (0.7 * np.sign(dz)))
                if candidate_point.equals(point):
                    break
                if candidate_point.equals(old_point):
                    break

                if not self.is_inside(candidate_point, int(mask_radius)):
                    return None
                # calculate moments (should always return table of numbers)
                moments = self.calculate_moments(candidate_point.int_point(), vessel_level, bg_level, mask_size, result)
                dx = mask_radius * moments[1] / moments[0]
                dy = mask_radius * moments[2] / moments[0]
                dz = mask_radius * moments[3] / moments[0]
                if previous_point is not None:
                    v1=[point.x-previous_point.x,point.y-previous_point.y,point.z-previous_point.z]
                    v2=[point.x-candidate_point.x,point.y-candidate_point.y,point.z-candidate_point.z]
                    v1len=np.sqrt(v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2])
                    v2len=np.sqrt(v2[0] * v2[0] + v2[1] * v2[1] + v2[2] * v2[2])
                    if (v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]) / v1len / v2len > 0.7:
                        break

                shift = np.sqrt(dx*dx + dy*dy + dz*dz)
                if shift < old_shift:
                    old_point = candidate_point
                else:
                    break

            # update cylinder point calculated using moments
            cylinder.x = old_point.x
            cylinder.y = old_point.y
            cylinder.z = old_point.z
            moments = self.calculate_moments(cylinder.int_point(), vessel_level, bg_level, mask_size, result)
            # calculate vessel center
            cylinder.dx = cylinder.x + (mask_radius * moments[1]/moments[0])
            cylinder.dy = cylinder.y + (mask_radius * moments[2]/moments[0])
            cylinder.dz = cylinder.z + (mask_radius * moments[3]/moments[0])
            cylinder.shift = self.moment_centerage(moments)
            cylinder.shift *= mask_radius
            cylinder.alpha, cylinder.beta = self.calculate_angles(moments)

            # if we want recalculate vessel level on every window recalculating pass
            new_vessel_level = self.calculate_vessel_level(cylinder, vessel_level,result)
            if new_vessel_level<parameters["min_vessel_level"]:
                return None #outside vessel - intensity level below minimum
            if parameters["adapting_vessel_level"]:
                cylinder.vessel_level = np.round((0.75 * new_vessel_level) + (0.25 * vessel_level))

            # if we want recalculate background level on every window recalculation.
            if parameters["adapting_bg_level"]:
                #new_bg_level = self.calculate_BG_level(cylinder, vessel_level, bg_level, parameters)
                #cylinder.bg_level = new_bg_level #set new recalculated background level
                new_bg_level = self.calculate_BG_level(cylinder, mask_size,parameters["min_vessel_level"])
                if not new_bg_level is None:
                    cylinder.bg_level = new_bg_level #set new recalculated background level

            # calculate radius for result cylinder
            vessel_radius = self.calculate_radius(moments,cylinder.vessel_level,cylinder.bg_level)
            cylinder.radius = vessel_radius * mask_radius

            # if adapting_window_size is true continue algorithm, but if false do only one algorithm pass
            if parameters["adapting_window_size"]:
                new_mask_size = self.calculate_mask_radius(cylinder.radius, mask_size)
                # check if new calculated mask has size with correct bounds
                if new_mask_size in examined_masks or new_mask_size < 0 or new_mask_size/2 >= len(self.masks):
                    recalculate_window = False # if new calculated mask size is incorrect stop processing
                else:
                    # recalculate algorithm step witch new calculated mask size
                    mask_size = new_mask_size
                    examined_masks.append(mask_size)
                    cylinder.mask_size = mask_size
                    mask_radius = mask_size / 2.0
            else:
                # stop window recalculating after single pass
                recalculate_window = False
        return cylinder

     def calculate_moments(self, point, vessel_level, bg_level, mask_size, result):
        '''
        fr: opMnt, calculate values of the geometrical moments
        :param point: Point() object
        :param vessel_level:
        :param bg_level:
        :param mask_size:
        :return:  moments: list with 11 numbers
        '''
        mask_radius = mask_size / 2
        # check, whether method should calculate moment values
        if not self.is_inside(point, mask_radius): return None
        moment_values = np.zeros(11)
        # extract subvolume of point with mask radius and calcification subvolume
        subvolume_orig = self.data[point.z - mask_radius:point.z + mask_radius + 1, point.y - mask_radius:point.y + mask_radius + 1, point.x - mask_radius:point.x + mask_radius + 1]
        subvolume = np.copy(subvolume_orig)

        calcification_subvolume = result.get_calcifications_ndarray_in_range(point.z - mask_radius, point.z + mask_radius + 1, point.y - mask_radius, point.y + mask_radius + 1, point.x - mask_radius, point.x + mask_radius + 1)
        # replace all values, where point with the same coordinates in calcification subvolume == 1 with vessel level value, to erase calcifications from extracted subvolume
        np.place(subvolume, calcification_subvolume == 1, vessel_level)
        subvolume[subvolume<bg_level] = bg_level
        ingredientB = subvolume - bg_level
        # calculate first moment to ninth
        for i in range(0, 10):
            ingredientA = (self.masks[mask_radius - 1])[i]
            r = np.multiply(ingredientA, ingredientB)
            moment_values[i] = np.sum(r)
        # calculate last needed moment - tenth
        moment_values[10] = np.sum(np.multiply((self.masks[mask_radius - 1])[0], subvolume))
        return moment_values

     def moment_centerage(self, moments):
        '''
        fr: centerage, modifying the moments values
        :param moments: list with will be modified
        :return:  shift - parameter
        '''
        old_moments = np.copy(moments)
        shift = np.sqrt(moments[1] * moments[1] + moments[2] * moments[2] + moments[3] * moments[3])
        shift /= moments[0]
        moments[4] = old_moments[4] - old_moments[1] * old_moments[2] / old_moments[0]
        moments[5] = old_moments[5] - old_moments[1] * old_moments[3] / old_moments[0]
        moments[6] = old_moments[6] - old_moments[2] * old_moments[3] / old_moments[0]
        moments[7] = old_moments[7] - old_moments[1] * old_moments[1] / old_moments[0]
        moments[8] = old_moments[8] - old_moments[2] * old_moments[2] / old_moments[0]
        moments[9] = old_moments[9] - old_moments[3] * old_moments[3] / old_moments[0]
        return shift


     def calculate_angles(self, moments):
        '''Calcul_angles, calculates 3D orientation of vessel from the given moments
         :param moments: list of 11 numbers
         :return: tuple with alpha and beta angles
        '''
        if moments[7] == moments[8]:
            alpha = 45.0
        else:
            alpha = 90.0/ np.pi * np.arctan((2.0 * moments[4] / (moments[7] - moments[8])))
        if alpha > 90.0:
            alpha -= 90
        if alpha < 0.0:
            alpha += 90.0
        A = np.round(alpha)
        if self.M8(A, 0, moments) > self.M8(A + 90, 0, moments):
            alpha += 90
        A = np.round(alpha)
        AA = A + A
        if moments[9] != 0:
            beta = 90.0 / np.pi * np.arctan(2.0 * (moments[5] * self.COS[A] + moments[6] * self.SIN[A]) / (moments[9] - moments[7] * self.COS_2[A] - moments[8] * self.SIN_2[A] - moments[4] * self.SIN[A]))
        else:
            beta = 45.0
        if beta > 90.0:
            beta -= 90.0
        if beta < 0.0:
            beta += 90
        if self.M9(A, np.round(beta), moments) < self.M9(A, np.round(beta + 90), moments):
            beta += 90.0
        return (alpha, beta)

     def M8(self, a, b, moments):
        ''' used for calculating alpha angle value
        :param a:
        :param b:
        :param moments: list with data
        :return: ?
        '''
        aa = a + a
        F = moments[7] * self.SIN_2[a] + moments[8] * self.COS_2[a] - moments[4] * self.SIN[aa]
        return F

     def M9(self, a, b, moments):
        ''' used for calculate beta angle value
        :param a:
        :param b:
        :param moments: list with data
        :return: ?
        '''
        aa = a + a
        bb = b + b
        F = (moments[7] * self.COS_2[a] + moments[8] * self.SIN_2[a] + moments[4] * self.SIN[aa]) * self.SIN_2[b]
        F += (moments[5] * self.COS[a] + moments[6] * self.SIN[a]) * self.SIN[bb]
        F += moments[9] * self.COS_2[b]
        return F

     def calculate_radius(self, moments, vessel_level, bg_level):
        '''  fr: calcul_rayon, calculates the vessel radius from the given moments
        :param moments:
        :param vessel_level:
        :param bg_level:
        :return: returns vessel radius for given moments
        '''
        vessel_level = vessel_level - bg_level
        bg_level = 0
        if moments[0] < (vessel_level * 4 * np.pi) / 3:
            radius = ((moments[0] * 3) / (np.pi * 4) - vessel_level) / (-vessel_level)
            radius = radius ** (2.0 / 3.0)
            if radius > 1.0: radius = 0
            else: radius = np.sqrt(1 - radius)
        else: radius = 1.0
        return radius

     def in_vessel_check(self, point, mask_size, bg_level, parameters):
        '''
        fr: dans_vaisseau, it checks if specified point belongs to vessel with given background and vessel levels
        :param point: Point() object or its subclasses
        :param mask_size:
        :param bg_level:
        :param parameters:
        :return: True if in vessel, False otherwise
        '''
        mask_radius = int(mask_size/2.0)
        ix = int(point.x)
        iy = int(point.y)
        iz = int(point.z)

        # calculate minimum and maximum values for the point (by adding/substracting mask radius)
        min_x = ix - mask_radius
        min_y = iy - mask_radius
        min_z = iz - mask_radius

        max_x = ix + mask_radius
        max_y = iy + mask_radius
        max_z = iz + mask_radius

        # check, whether min and max values are outside of min and max range of base
        if min_x < 0: min_x = 0
        if min_y < 0: min_y = 0
        if min_z < 0: min_z = 0

        if max_x >= self.size_x: max_x = self.size_x
        if max_y >= self.size_y: max_y = self.size_y
        if max_z >= self.size_z: max_z = self.size_z

        # calculate means and variance from data that is covered by mask
        mean = np.mean(self.data[min_z:max_z+1, min_y:max_y+1, min_x:max_x+1])
        variance = np.var(self.data[min_z:max_z+1, min_y:max_y+1, min_x:max_x+1])
        return mean > parameters["mean_threshold"] \
               and variance > parameters["variance_threshold"]\
               and mean > bg_level

     def select_next_point(self, previous_point, step, direction,prev_prev_point=None):
        '''
        fr. selec_point, Select next point candidate for next vessel point
        :param previous_point: last selected point: VesselPoint() object
        :param step:
        :param direction:
        :return: selected Point() object
        '''
        next_point = Point()
        vector=Point()
        # oryginalna wersja, ktora jednak wydawala sie nie dzialac z tajemniczych powodow
        vector.x = direction*step*self.SIN[np.round(previous_point.beta)]*self.COS[np.round(previous_point.alpha)]
        vector.y = direction*step*self.SIN[np.round(previous_point.beta)]*self.SIN[np.round(previous_point.alpha)]
        vector.z = direction*step*self.COS[np.round(previous_point.beta)]
        if not prev_prev_point is None:
            prev_vector=Point(x=previous_point.x-prev_prev_point.x,y=previous_point.y-prev_prev_point.y,z=previous_point.z-prev_prev_point.z)
            if prev_vector.x*vector.x+prev_vector.y*vector.y+prev_vector.z*vector.z < 0: # tracking back
                vector.x=-vector.x;vector.y=-vector.y;vector.z=-vector.z
        next_point.x = np.round(previous_point.x + vector.x)
        next_point.y = np.round(previous_point.y + vector.y)
        next_point.z = np.round(previous_point.z + vector.z)

        #next_point.x = np.round(previous_point.x + direction*step*self.COS[np.round(previous_point.beta)]*self.COS[np.round(previous_point.alpha)])
        #next_point.y = np.round(previous_point.y + direction*step*self.COS[np.round(previous_point.beta)]*self.SIN[np.round(previous_point.alpha)])
        #next_point.z = np.round(previous_point.z + direction*step*self.SIN[np.round(previous_point.beta)])
        return next_point

     def is_inside(self, point, radius=0):
        '''
        inside 3D image and not closer to edge than mask radius size
        :param point:
        :param radius:
        :return: True or False
        '''
        return int(point.x) in range(radius, self.size_x - radius) and int(point.y) in range(radius, self.size_y - radius) and int(point.z) in range(radius, self.size_z - radius)

     def init_sin_cos(self):
        '''
        Calculate sin, cos values for speed up algorithm calculations
        '''
        self.SINN = np.zeros(3601)
        self.COSS = np.zeros(3601)

        self.SIN = np.zeros(361)
        self.COS = np.zeros(361)
        self.ATANCOS = np.zeros(361)
        self.ATANSAC = np.zeros(361)

        self.SIN_2 = np.zeros(181)
        self.COS_2 = np.zeros(181)

        for i in range(0, 3601):
            self.SINN[i] = np.sin(np.pi * i/1800.0)
            self.COSS[i] = np.cos(np.pi * i/1800.0)

        for i in range(0, 361):
            self.SIN[i] = np.sin(np.pi*i/180.0)
            self.COS[i] = np.cos(np.pi*i/180.0)

            self.ATANCOS[i] = 180.0 * np.arctan(np.cos(np.pi*i/180.0)) / np.pi
            self.ATANSAC[i] = 180.0 * np.arctan(self.SIN[i] * np.cos(self.ATANCOS[i])) / np.pi

        for i in range(0, 181):
            self.SIN_2[i] = (1 - self.COS[2*i]) / 2
            self.COS_2[i] = (1 + self.COS[2*i]) / 2


     def calculate_vessel_level(self, vessel_point, vessel_level,result):
        '''
        fr: estime_intensite, adapt the vessel level
        :param vessel_point: point for which new vessel level is being calculated
        :param vessel_level: old vessel level
        :param result: to check if it is not calcification point (not vessel level adaptation then
        :return: new_vessel_level
        '''
        ix = int(vessel_point.dx)
        iy = int(vessel_point.dy)
        iz = int(vessel_point.dz)

        dx = vessel_point.dx - ix
        dy = vessel_point.dy - iy
        dz = vessel_point.dz - iz

        d_x = 1.0 - dx
        d_y = 1.0 - dy
        d_z = 1.0 - dz
        # point must bee inside image 3D
        if ix + 1 >= self.size_x or iy + 1 >= self.size_y or iz + 1 >= self.size_z:
            return vessel_level
        # if point belongs to calcification ignore
        if self.check_if_calcification(vessel_point,result):
            return vessel_level
        # calculate average ?
        new_vessel_level = (d_x * d_y * d_z * self.data[iz, iy, ix] +
                            dx * d_y * d_z * self.data[iz, iy, ix + 1] +
                            d_x * dy * d_z * self.data[iz, iy + 1, ix] +
                            dx * dy * d_z * self.data[iz, iy + 1, ix + 1] +
                            d_x * d_y * dz * self.data[iz + 1, iy, ix] +
                            dx * d_y * dz * self.data[iz + 1, iy, ix + 1] +
                            d_x * dy * dz * self.data[iz + 1, iy + 1, ix] +
                            dx * dy * dz * self.data[iz + 1, iy + 1, ix + 1]
                            )
        return new_vessel_level

     def is_calcification(self, point, radius=0):
        '''
        checks if the point (integer coordinates) and its naighbourhood is calcification
        :param point: point to check (coordinates are turned to integers
        :param neighbourhood: size of the neighbourhood
        '''
        #TODO !!!!!!!
        return False

     def get_intensity_from_real(self, point):
        '''
        interpolate intensity for the real coordinates
        :param point: point with real coordinates x, y, z
        '''

        ix=np.floor(point.x); ixp1=ix+1
        iy=np.floor(point.y); iyp1=iy+1
        iz=np.floor(point.z); izp1=iz+1
        # a bit conservative check - to accelerate
        if not self.is_inside(Point(iz,iy,ix),1):
            return None
        tempVesselPoint = VesselPoint(iz, iy, ix)
        tempVesselPoint.dz = iz
        tempVesselPoint.dy = iy
        tempVesselPoint.dx = ix
        if self.is_calcification(Point(iz,iy,ix),1):
            return None
        dx=ixp1-point.x
        dy=iyp1-point.y
        dz=izp1-point.z

        value=dz*dy*dx*self.data[iz,iy,ix]+\
               (1-dz)*dy*dx*self.data[izp1,iy,ix]+\
               dz*(1-dy)*dx*self.data[iz,iyp1,ix]+\
               (1-dz)*(1-dy)*dx*self.data[izp1,iyp1,ix]+\
               dz*dy*(1-dx)*self.data[iz,iy,ixp1]+\
               (1-dz)*dy*(1-dx)*self.data[izp1,iy,ixp1]+\
               dz*(1-dy)*(1-dx)*self.data[iz,iyp1,ixp1]+\
               (1-dz)*(1-dy)*(1-dx)*self.data[izp1,iyp1,ixp1]
        return value

     #def calculate_BG_level(self, point, vessel_level, bg_level, params): old
     def calculate_BG_level(self, vessel_point, mask_size, min_vessel_level):
        '''
        fr: estime_fond8 - calculates the current background level analyzing 8 probiles orthogonal to the vessel local direction
        :param vessel_point: current position of the vessel, filled up with the vessel angles, radius and intensity
        :param mask_size: simply mask size
        '''
        #orientations (multiplied by 10) of 4 profiles
        alpha=vessel_point.alpha
        beta=vessel_point.beta
        scope = np.round(mask_size*3)
        alphas10 = (int(alpha*10+0.5),
                    int((alpha+90 - self.ATANCOS[int(beta+0.5)])*10+0.5),
                    int((alpha+90)*10+0.5),
                    int((alpha+90 + self.ATANCOS[(int)(beta+0.5)])*10+0.5))
        betas10 = (int((beta+90)*10+0.5),
                   int((90 + self.ATANSAC[int(beta+0.5)])*10+0.5),
                   900,
                   int((90 - self.ATANSAC[int(beta+0.5)])*10+0.5))
        profiles=[]
        if not self.is_inside(vessel_point, scope): return None
        for nr in range(4):
            profile1=[]
            profile2=[]
            profiles.append(profile1)
            profiles.append(profile2)
            for step in range(scope):

            # delta = Point( x=step*self.SINN[betas10[nr]]*self.COSS[alphas10[nr]],
            # y=step*self.SINN[betas10[nr]]*self.SINN[alphas10[nr]],
            # z=step*self.COSS[betas10[nr]] )
                delta = Point( z=step*self.SINN[betas10[nr]],
                               y=step*self.COSS[betas10[nr]]*self.SINN[alphas10[nr]],
                               x=step*self.COSS[betas10[nr]]*self.COSS[alphas10[nr]])
                profile1.append(self.get_intensity_from_real(Point(z=vessel_point.z+delta.z, y=vessel_point.y+delta.y,x=vessel_point.x+delta.x)))
                profile2.append(self.get_intensity_from_real(Point(z=vessel_point.z+delta.z, y=vessel_point.y+delta.y,x=vessel_point.x+delta.x)))
        # taking the aproximated background level for each direction
        # finding the longest slope, its start point will be the start point for background
        # level seeking
        max_bg_level=None
        for profile in profiles:
            j = max_slope_start = 0
            max_diff = 0
            while j<scope-1:
               slope_start = j
               while (j < scope-1) and (profile[j]>profile[j+1]):
                   j+=1    #going down with the slope
               if max_diff < (profile[slope_start]-profile[j]):
                   #checking if the slope is maximum
                   max_slope_start = slope_start
                   max_diff = profile[slope_start]-profile[j]
               while (j < scope-1) and (profile[j]<=profile[j+1]):
                   j+=1    #going up with the slope
            #taking the aproximated background level for each direction
            max_diff = 0
            for  j in range(max_slope_start,scope-1):
               diff = profile[j]-profile[j+1]
               if np.abs(diff) > max_diff:
                   max_diff = np.abs(diff)
               if (np.abs(diff) < 0.2*max_diff) or (diff <= 0):
                   #end of profil - or the value difference is low in comparison with its maximum value
                   #                  or the value difference is not greater then 0 after registring value > 0
                   # taking the maximum in range of iRayon/2
                   if profile[j]<=min_vessel_level and (max_bg_level is None or max_bg_level<profile[j]):
                       max_bg_level=profile[j]
                       break
                   # ?????
                   #for (k = j+1 ; k < iRayon/PROFIL_FENETRE_RATIO ; ++ k)
                   #    if (twProfil[0][k] > twFond[i])
                   #        twFond[i] = twProfil[0][k];
                   #break
        return max_bg_level

     def calculate_mask_radius(self, vessel_radius, mask_size):
        '''
        fr: estime_fenetre, adapt the used mask size
        :param vessel_radius:
        :param mask_size:
        :return: new mask size or -1
        '''
        mask_radius = int(mask_size/2)
        if mask_radius-1 - vessel_radius < 0.0:
            if mask_radius < len(self.masks):
                return mask_size + 2
        if mask_radius-1 - vessel_radius > 1.25:
            if mask_radius > 1:
                return mask_size - 2
        return -1

     def check_if_calcification(self, vessel_point,result):
        '''
        check, whether in place of vessel_point there is calcification
        :param vessel_point:
        :return:
        '''
        ix = int(vessel_point.dx)
        iy = int(vessel_point.dy)
        iz = int(vessel_point.dz)
        #subvolume = self.data[iz:iz + 2, iy:iy + 2, ix:ix + 2]
        #return np.sum(subvolume) > 0
        return result.calcification_voxels_map.has_key((iz,iy,ix))

     def extract_calcification(self, calcification_seed_point, mask_size, max_vessel_level, vesselBranch, result):
        '''
        region growing algorithm
        :param calcification_seed_point:
        :param mask_size:
        :param max_vessel_level:
        :param result:
        :return:
        '''
        # TODO: second phase: detecting real calcification borders (deflection point)
        max_calc_radius=20
        queue = collections.deque()
        queue.append(calcification_seed_point)
        vesselBranch.set_calcification_voxel(calcification_seed_point.z, calcification_seed_point.y, calcification_seed_point.x)
        # calcification = Calcification()
        # calcification.voxels.append(calcification_seed_point)

        while len(queue) > 0:
            point = queue.popleft()
            for dx in (-1,0, 1):
                if point.x + dx < 0: continue
                if point.x + dx >= self.size_x: continue
                if np.abs(point.x + dx-calcification_seed_point.x)>max_calc_radius: continue

                for dy in (-1,0, 1):
                    if point.y + dy < 0: continue
                    if point.y + dy >= self.size_y: continue
                    if np.abs(point.y + dy-calcification_seed_point.y)>max_calc_radius: continue

                    for dz in (-1,0, 1):
                        if point.z + dz < 0: continue
                        if point.z + dz >= self.size_z: continue
                        if np.abs(point.z + dz-calcification_seed_point.z)>max_calc_radius: continue


                        if result.is_calcification_voxel(point.z + dz, point.y + dy, point.x + dx): continue
                        if self.data[point.z + dz, point.y + dy, point.x + dx] > max_vessel_level:
                            print "Calc:(x=",str(point.x + dx),",y=", str(point.y + dy), "z=",str(point.z + dz),") V=",str(self.data[point.z + dz, point.y + dy, point.x + dx])
                            vesselBranch.set_calcification_voxel(point.z + dz, point.y + dy, point.x + dx)

                            new_point = Point(x=point.x + dx, y=point.y + dy, z=point.z + dz)
                            # calcification.voxels.append(new_point)
                            queue.append(new_point)

        # return calcification

     def search_calcification_seed_point(self, point, mask_size, max_vessel_level, result):
        '''
         search in the neighbourhood for a voxel with its intensity level above max vessel level not yet detected as the calcficication
         :param point: Point() object
         :param mask_size:
         :param max_vessel_level:
         :return: calcification Seed point Point() object
        '''
        # calculate region bounds max
        max_x = int(min(point.x + (mask_size // 2) + 1, self.size_x))
        max_y = int(min(point.y + (mask_size // 2) + 1, self.size_y))
        max_z = int(min(point.z + (mask_size // 2) + 1, self.size_z))
        # calculate region bounds min
        min_x = int(max(point.x - mask_size // 2, 0))
        min_y = int(max(point.y - mask_size // 2, 0))
        min_z = int(max(point.z - mask_size // 2, 0))
        # for all numbers from calcification_voxels one subtract value (one or zero) it will be 0 or 1
        calcificationMaskRegion = result.get_calcifications_ndarray_in_range(min_z, max_z, min_y, max_y, min_x, max_x)
        ingredientB = np.subtract(1, calcificationMaskRegion) #invert values operation
        # reset values already belonging to calcification by multiplying
        search_region = np.multiply(self.data[min_z:max_z, min_y:max_y, min_x:max_x], ingredientB) #values masked by 0 will be 0
        # check if max vessel level in this region has been reached (it means calcification) or not
        if np.amax(search_region) <= max_vessel_level:
            return None # if not reached return None
        # search_region = region - values belonging to calcification (zeros)
        # get index (x,y,z) of voxel with max value
        max_level = -1
        for x in range(min_x, max_x):
            for y in range(min_y, max_y):
                for z in range(min_z, max_z):
                    if search_region[z-min_z, y-min_y, x-min_x] > max_level:
                        max_level = search_region[z-min_z, y-min_y, x-min_x]
                        seed_x, seed_y, seed_z = x, y, z
        return Point(seed_z, seed_y, seed_x)


     def estimate_threshold(self, vessel_level, bg_level):
         ''' fr: estime_seuil Calculate new Thresholds
         :return: mean, variance
         '''
         minR = 0.1
         if vessel_level <= bg_level:
             return 65535, 65535

         mean = (vessel_level - bg_level) * (np.pi * minR * minR) + bg_level
         variance = ((vessel_level - mean) ** 2) * (np.pi * minR * minR) + ((bg_level - mean) ** 2) * (1.0 - np.pi * minR * minR)
         variance = np.sqrt(variance)

         return mean, variance



class MipAlgorithm(ProcessingAlgorithm):
    def __init__(self):
        super(MipAlgorithm, self).__init__()
        self.buffering_enabled = True
        self.background_remove = None


    def onProcessData(self, database4D, snapshotManager):
        frame_id = self.selected_frame_id
        if self.buffering_enabled == True and frame_id in snapshotManager:
            cashed_images = snapshotManager[frame_id]
            return cashed_images
        else:
            frame_3D = database4D.get_frame(frame_id).copy()
            if self.background_remove != None:
                # change specified background color with 0 values (because bright background interferes MIP)
                frame_3D[frame_3D==self.background_remove] = 0

            #get ndArray 3D data
            frame_3D = frame_3D.reshape(database4D.get_z_size(), database4D.get_y_size(), database4D.get_x_size())
            nd_array_xy = np.amax(frame_3D, axis=0).copy()
            # start
            nd_array_xy /= 16
            nd_array_xy = nd_array_xy.reshape(-1)
            nd_array_xy.dtype = np.uint8
            nd_array_xy = nd_array_xy[::2].reshape((database4D.get_x_size(), database4D.get_y_size()))
            # stop

            frame_3D = frame_3D.reshape(database4D.get_z_size(), database4D.get_y_size(), database4D.get_x_size())
            nd_array_yz = np.amax(frame_3D, axis=2).copy()
            # start
            nd_array_yz /= 16
            nd_array_yz = nd_array_yz.reshape(-1)
            nd_array_yz.dtype = np.uint8
            nd_array_yz = nd_array_yz[::2].reshape((database4D.get_z_size(), database4D.get_y_size()))
            # stop
            nd_array_yz = nd_array_yz.transpose().copy()
            # row, col = nd_array_yz.shape
            # new_col = (col+3)/4*4
            # new_nd_array_yz = np.zeros((row, new_col), dtype=nd_array_yz.dtype)
            # for r in range(row):
            #     new_nd_array_yz[r, :col] = nd_array_yz[r]
            # new_nd_array_yz = new_nd_array_yz.transpose().copy()

            frame_3D = frame_3D.reshape(database4D.get_z_size(), database4D.get_y_size(), database4D.get_x_size())
            nd_array_xz = np.amax(frame_3D, axis=1).copy()
            # start
            nd_array_xz /= 16
            nd_array_xz = nd_array_xz.reshape(-1)
            nd_array_xz.dtype = np.uint8
            nd_array_xz = nd_array_xz[::2].reshape((database4D.get_z_size(), database4D.get_x_size()))
            # stop

            #add data to cache
            if self.buffering_enabled == True:
                snapshotManager[frame_id] = (nd_array_xy, nd_array_xz, nd_array_yz)
        return (nd_array_xy, nd_array_xz, nd_array_yz)


    def get_mip_x(self, z, y):
         ''' return point extracted from database 3D. If any point does not exist return None'''
         frame3D = self.data_base.get_frame(self.selected_frame_id)
         index = frame3D[z, y, ::].argmax(axis=0)
         return (z, y, index)

    def get_mip_y(self, x, z):
         frame3D = self.data_base.get_frame(self.selected_frame_id)
         index = frame3D[z, ::, x].argmax(axis=0)
         return (z, index, x)

    def get_mip_z(self, x, y):
         frame3D = self.data_base.get_frame(self.selected_frame_id)
         index = frame3D[::, y, x].argmax(axis=0)
         return (index, y, x)


class SliceAlgorithm(ProcessingAlgorithm):

    def __init__(self):
        super(SliceAlgorithm, self ).__init__()
        self.selected_slice_point = [1,1,1] #z,y,x
        self.drawVisitedVoxels = True
        self.prevResultCheck = True
        self.nextResultCheck = True
        self.referenceResultCheck = True

    def onProcessData(self, database_4D, snapshot_manager):
        frame_id = self.selected_frame_id
        frame_3D = database_4D.get_frame(frame_id)

        segmentationSnapshots = self.core.get_property_value('segmentationResult')

        frame_3D = frame_3D.reshape(database_4D.get_z_size(), database_4D.get_y_size(), database_4D.get_x_size())
        nd_array_xy = frame_3D[self.selected_slice_point[0],:,:].copy()
        # start
        nd_array_xy /= 16
        nd_array_xy = nd_array_xy.reshape(-1)
        nd_array_xy.dtype = np.uint8
        # stop

        frame_3D = frame_3D.reshape(database_4D.get_z_size(), database_4D.get_y_size(), database_4D.get_x_size())
        nd_array_yz = frame_3D[:,:,self.selected_slice_point[2]].copy()
        # start
        nd_array_yz /= 16
        nd_array_yz = nd_array_yz.reshape(-1)
        nd_array_yz.dtype = np.uint8
        # stop

        frame_3D = frame_3D.reshape(database_4D.get_z_size(), database_4D.get_y_size(), database_4D.get_x_size())
        nd_array_xz = frame_3D[:,self.selected_slice_point[1],:].copy()
        # start
        nd_array_xz /= 16
        nd_array_xz = nd_array_xz.reshape(-1)
        nd_array_xz.dtype = np.uint8
        # stop

        nd_array_xy = nd_array_xy[::2]
        nd_array_yz = nd_array_yz[::2]
        nd_array_xz = nd_array_xz[::2]

        # calculate images bounds
        ix = (database_4D.get_x_size()+3)//4*4
        iy = (database_4D.get_y_size()+3)//4*4
        iz = (database_4D.get_z_size()+3)//4*4


        if segmentationSnapshots.has_key(frame_id) and self.drawVisitedVoxels:
            result = segmentationSnapshots[frame_id]
        else:
            result = None

        # get previous result
        prevResult = None
        if segmentationSnapshots.has_key(frame_id - 1) and self.prevResultCheck and self.drawVisitedVoxels:
            prevResult = segmentationSnapshots[frame_id - 1]

        # get next result
        nextResult = None
        if segmentationSnapshots.has_key(frame_id + 1) and self.nextResultCheck and self.drawVisitedVoxels:
            nextResult = segmentationSnapshots[frame_id + 1]

        # get next result
        referenceResult = None
        referenceIndex = self.core.get_property_value('referenceFrameIndex')
        if referenceIndex is not None:
            if segmentationSnapshots.has_key(referenceIndex) and self.referenceResultCheck and self.drawVisitedVoxels:
                referenceResult = segmentationSnapshots[referenceIndex]

        if prevResult is not None:
            # map data to images
            pointColorValue = self.core.get_property_value("SlicePrevVoxelsValue")
            for key, vesselId in prevResult.voxelVisitedData.iteritems():
                if vesselId[0] < 0: continue # paint only vessel voxels, no calcifications id < 0
                vessel = prevResult.get_vessel_by_id(vesselId[0])
                if vessel is None : continue
                # selected voxels, paint on different color
                if key[0] == self.selected_slice_point[0]:
                    nd_array_xy[key[2]+database_4D.get_x_size()*key[1]] = pointColorValue
                if key[1] == self.selected_slice_point[1]:
                    nd_array_xz[key[2]+database_4D.get_x_size()*key[0]] = pointColorValue
                if key[2] == self.selected_slice_point[2]:
                    nd_array_yz[key[1]+database_4D.get_y_size()*key[0]] = pointColorValue

        if nextResult is not None:
            # map data to images
            pointColorValue = self.core.get_property_value("SliceNextVoxelsValue")
            for key, vesselId in nextResult.voxelVisitedData.iteritems():
                if vesselId[0] < 0: continue # paint only vessel voxels, no calcifications id < 0
                vessel = nextResult.get_vessel_by_id(vesselId[0])
                if vessel is None : continue
                # selected voxels, paint on different color
                if key[0] == self.selected_slice_point[0]:
                    nd_array_xy[key[2]+database_4D.get_x_size()*key[1]] = pointColorValue
                if key[1] == self.selected_slice_point[1]:
                    nd_array_xz[key[2]+database_4D.get_x_size()*key[0]] = pointColorValue
                if key[2] == self.selected_slice_point[2]:
                    nd_array_yz[key[1]+database_4D.get_y_size()*key[0]] = pointColorValue

        if referenceResult is not None:
            # map data to images
            pointColorValue = self.core.get_property_value("SliceReferenceVoxelsValue")
            for key, vesselId in referenceResult.voxelVisitedData.iteritems():
                if vesselId[0] < 0: continue # paint only vessel voxels, no calcifications id < 0
                vessel = referenceResult.get_vessel_by_id(vesselId[0])
                if vessel is None : continue
                # selected voxels, paint on different color
                if key[0] == self.selected_slice_point[0]:
                    nd_array_xy[key[2]+database_4D.get_x_size()*key[1]] = pointColorValue
                if key[1] == self.selected_slice_point[1]:
                    nd_array_xz[key[2]+database_4D.get_x_size()*key[0]] = pointColorValue
                if key[2] == self.selected_slice_point[2]:
                    nd_array_yz[key[1]+database_4D.get_y_size()*key[0]] = pointColorValue

        if result is not None:
            # map data to images
            pointColorValue = self.core.get_property_value("SliceVoxelsValue")
            for key, vesselId in result.voxelVisitedData.iteritems():
                if vesselId[0] < 0: continue # paint only vessel voxels, no calcifications id < 0
                vessel = result.get_vessel_by_id(vesselId[0])
                if vessel is None : continue
                # selected voxels, paint on different color
                if key[0] == self.selected_slice_point[0]:
                    nd_array_xy[key[2]+database_4D.get_x_size()*key[1]] = pointColorValue
                if key[1] == self.selected_slice_point[1]:
                    nd_array_xz[key[2]+database_4D.get_x_size()*key[0]] = pointColorValue
                if key[2] == self.selected_slice_point[2]:
                    nd_array_yz[key[1]+database_4D.get_y_size()*key[0]] = pointColorValue


        nd_array_xy = nd_array_xy.reshape((database_4D.get_x_size(), database_4D.get_y_size()))
        nd_array_yz = nd_array_yz.reshape((database_4D.get_z_size(), database_4D.get_y_size()))
        nd_array_xz = nd_array_xz.reshape((database_4D.get_z_size(), database_4D.get_x_size()))
        new_nd_array_yz = nd_array_yz.transpose().copy() #additionaly transpose

        return (nd_array_xy, nd_array_xz, new_nd_array_yz)

    def getSliceVoxels(self, result):
        pass

        '''pil_img = None
        index = self.property_dictionary['index_xy']
        if self.property_dictionary['orientation'] == 'xy':
            pil_img = PIL.Image.fromarray(frame_3D[self.property_dictionary['index_2D']])
        elif self.property_dictionary['orientation'] == 'xz':
            pil_img = PIL.Image.fromarray(frame_3D[::1][::1][index])
            pass
        elif self.property_dictionary['orientation'] == 'yz':
            pil_img = PIL.Image.fromarray(frame_3D[::1][index][::1])
            pass


        #konwersja numpy ndarray do pil image
        #pil_img = PIL.Image.fromarray(numpy_array_data.copy()) # kopia nie jest kopia gleboka?

        #testowo operacje na obrazie
        #pil_img2 = ndimage.gaussian_filter(pil_img, 7) #rozmycie Gaussa
        #pil_img2 = ndimage.grey_erosion(pil_img, size=(5,5))
        #pil_img2 = ndimage.grey_dilation(pil_img, size=(5,5))
        #pil_img2 = ndimage.grey_closing(pil_img, size=(50,50))
        #pil_img2 = ndimage.grey_opening(pil_img, size=(50,50))

        #filtrowanie
        c = np.array([[0, -1, 0],
                      [-1, 4, -1],
                      [0, -1, 0]])
        #pil_img2 =  ndimage.convolve(pil_img, c, mode='nearest')

        #pil_img2 = pil_img

        nad = np.asarray(pil_img)#konwersja pil Image do ndArray
        '''
        #return (nd_array_xy, nd_array_xz, nd_array_yz)



class VesselCreator(object):

    def __init__(self, segmentationAlgorithm):
        super(VesselCreator, self).__init__()
        self.currentVessel = None
        self.segmentationAlgorithm = segmentationAlgorithm
        self.currentResult4D = None
        self.currentFrame3DIndex = 0
        self.testList = []
        self.currentHandVessel = None
        self.x=0
        self.y=0
        self.z=0


    def set_current_result_4D(self, result, frame3DIndex):
        '''result from current database 4D and frame index, need actual object for processing'''
        self.currentResult4D = result
        self.currentFrame3DIndex = frame3DIndex

    def add_point(self, z, y, x):
        ''' add point to vessel
        :param z:
        :param y:
        :param x:
        '''
        self.x=x
        self.y=y
        self.z=z

        parameters = {}

        if self.currentHandVessel:
            coreInstance = self.segmentationAlgorithm.core
            #stepFactor = coreInstance.get_property_value("step_factor")

            self.segmentationAlgorithm.size_x = self.segmentationAlgorithm.data_base.get_x_size()
            self.segmentationAlgorithm.size_y = self.segmentationAlgorithm.data_base.get_y_size()
            self.segmentationAlgorithm.size_z = self.segmentationAlgorithm.data_base.get_z_size()

            parameters["adapting_vessel_level"] = coreInstance.get_property_value("adapting_vessel_level")
            parameters["adapting_bg_level"] = coreInstance.get_property_value("adapting_bg_level")
            parameters["calcification_removing"] = False
            parameters["adapting_window_size"] = coreInstance.get_property_value("adapting_window_size")
            parameters["initial_bg_level"] = coreInstance.get_property_value("initial_bg_level")
            parameters["max_vessel_level"] = coreInstance.get_property_value("max_vessel_level")
            parameters["initial_mask_size"] = coreInstance.get_property_value("initial_mask_size")
            parameters["initial_vessel_level"] = coreInstance.get_property_value("initial_vessel_level")
            parameters["min_vessel_level"] = coreInstance.get_property_value("min_vessel_level")
            parameters["step_factor"] = coreInstance.get_property_value("step_factor")

            if self.x==0 or self.y==0 or self.z==0:
                return

            startPoint = Point(self.z, self.y, self.x)

            parameters["mean_threshold"], parameters["variance_threshold"] = self.segmentationAlgorithm.estimate_threshold(parameters["initial_vessel_level"], parameters["initial_bg_level"])

            if not self.segmentationAlgorithm.data:
                frame3D = self.segmentationAlgorithm.data_base.get_frame(self.currentFrame3DIndex)
                self.segmentationAlgorithm.data = frame3D

            if self.segmentationAlgorithm.in_vessel_check(startPoint, parameters["initial_mask_size"], parameters["initial_bg_level"], parameters) is False:
                return

            start_vessel_point = self.segmentationAlgorithm.estimate_vessel_cylinder(startPoint, parameters["initial_mask_size"], parameters["initial_vessel_level"], parameters["initial_bg_level"], parameters, self.currentHandVessel, self.currentResult4D)
            # check if this point belongs already to segmented vessel
            #if result.contains_point(start_vessel_point) == True:
            #    return result

            # add point to vessel branch
            self.currentHandVessel.add_last(start_vessel_point)#result.vessel_branch
            print start_vessel_point

        print "add "
        pass


    def create_new_vessel(self):
        '''
        create new empty vessel for points
        '''
        if self.currentResult4D:
            # if not empty dict
            if not self.currentResult4D.has_key(self.currentFrame3DIndex):
                # result does not exist, create one
                result3D = SegmentationResult3D(self.currentFrame3DIndex)
                self.currentResult4D[self.currentFrame3DIndex] = result3D
            else:
                # get result3D from 4D
                result3D = self.currentResult4D[self.currentFrame3DIndex]
            # add new vessel to result 3D
            newVessel = VesselBranch()
            result3D.add_vessel(newVessel)
            self.currentHandVessel = newVessel
        else:
            # result does not exist, create one
            result3D = SegmentationResult3D(self.currentFrame3DIndex)
            self.currentResult4D[self.currentFrame3DIndex] = result3D
            # add new vessel to result 3D
            newVessel = VesselBranch()
            result3D.add_vessel(newVessel)
            self.currentHandVessel = newVessel

        print "create vessel "
        pass

    def save_actual_vessel(self):
        '''
        end propagation vessel points
        '''
        self.currentHandVessel = None
        print "save actual vessel "
        # nie konieczne do dzialania, chyba
        pass

    def get_hint_points(self):
        '''calculate and return hint points list, hint point is list ! not tuple [z,y,x]'''

        # wybrac punkty nad i pod aktualnym (w 3D)i zwrocic punkty

        #return [[100,123,113], [17,56,23]]
        return