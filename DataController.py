from PySide import QtGui
import pickle

__author__ = 'Adam'
from DataFactory import DICOM_4D
from DataFactory import RAW_4D
import numpy as np

class AbstractAppController(object):
    '''Abstract base class for all application implementations, contains base core logic
    which will not be changed.
    '''
    this = None

    def __init__(self):
        super(AbstractAppController, self).__init__()
        self.applicationPropertiesDictionary = {}
        self.applicationActionsDictionary = {}
        AbstractAppController.this = self

    def setup_app(self):
        '''Method it is equal to start all aplication'''
        AbstractAppController.this = self
        self.on_create_logic() #create all application logic.
        self.on_create_UI() #create all UI and bindings.
        self.on_initialize_properties()

    def on_create_logic(self):
        '''Create all properties.'''
        pass

    #@abstractmethod
    def on_create_UI(self):
        '''Create all UI and bind components to logic'''
        pass

    def on_initialize_properties(self):
        # for actions
        for key, action in self.applicationActionsDictionary.iteritems():
            for binder in action.get_binder_list(): # initially link all the properties to its binders
                binder.on_link(action)
         # for roperties
        for key, property in self.applicationPropertiesDictionary.iteritems():
            for binder in property.get_binder_list():
                #set Default Value to all binders from property
                binder.on_update_ui(property.get_value(), property)

        for key, property in self.applicationPropertiesDictionary.iteritems():
            for binder in property.get_binder_list(): # initially link all the properties to its binders
                binder.on_link(property)

    def add_property(self, serializable, resettable, name, defaultValue, changeListener=None):
        property = PropertyController(name, resettable, defaultValue, serializable)
        property.set_event_listener(changeListener)
        self.applicationPropertiesDictionary[name] = property

    def add_action(self,name, actionListener):
        action = ActionController(name)
        action.set_event_listener(actionListener)
        self.applicationActionsDictionary[name] = action

    def add_property_binder(self, propertyName, binder):
        if self.applicationPropertiesDictionary.has_key(propertyName) == True :
            self.applicationPropertiesDictionary[propertyName].register_binder(binder, self.applicationPropertiesDictionary)
        else : raise Exception("Cannot add binder to property: ",str(propertyName),", property does not exist")

    def add_action_binder(self, actionName, binder):
        if self.applicationActionsDictionary.has_key(actionName) == True :
            self.applicationActionsDictionary[actionName].register_binder(binder, self.applicationActionsDictionary)
        else : raise Exception("Cannot add binder to action: ",str(actionName),", action does not exist")

    def get_property(self, name):
        return self.applicationPropertiesDictionary[name]

    def get_property_value(self, name):
        return self.applicationPropertiesDictionary[name].get_value()

    def set_property_value(self, name, value):
        self.applicationPropertiesDictionary[name].set_value(value)

    def invoke_action(self, actionName, params=None):
        self.applicationActionsDictionary[actionName].invoke_action_performaed(params)

    def import_properties(self, file):
        '''Method will load values for properties from file'''
        fo = open(file, "r+")
        stringToRead = str(fo.read())
        map = pickle.loads(stringToRead)
        fo.close()

        # merge two dictionaries
        for key, value in map.iteritems():
            self.applicationPropertiesDictionary[key].contentValue = value

        resultExist = True # if we have existing result
        if resultExist:
            dataType = self.get_property_value("dataSourceType")
            if dataType == 1:
                database = DICOM_4D(self.get_property_value("dicomDataFilePth"), 1)
                self.set_property_value("database4D", database)#load DICOM database
            elif dataType == 2:
                params = {}
                params["x_size"] = int(self.get_property_value("x_size"))
                params["y_size"] = int(self.get_property_value("y_size"))
                params["z_size"] = int(self.get_property_value("z_size"))
                params["bits_per_voxel"] = int(self.get_property_value("bits_per_voxel"))
                self.set_property_value("database4D", RAW_4D(self.get_property_value("rawDataFilesPth"), 1, params))
        # else:
        #     self.set_property_value("database4D", None) # clear database setting empty space

        del map["rawDataFilesPth"] # delete because no need to update
        del map["dicomDataFilePth"]

        #update all other properties
        for key, value in map.iteritems():
            self.set_property_value(key, map[key])

        #display properties
        # for key, property in self.applicationPropertiesDictionary.iteritems():
        #     print property.name, "  ", property.contentValue

        print "core properties imported from: ", file
        pass

    def exoprt_properties(self, file):
        '''Method will write values for properties to file'''
        fo = open(file, "w+")
        map = {}
        for key, property in self.applicationPropertiesDictionary.iteritems():
            if property.serializable == True:
                map[property.name]=property.contentValue
        picklestring = pickle.dumps(map)
        fo.write(picklestring)
        fo.close()
        print "core properties exported to: ",file
        pass


    def set_default_property_values(self):
        '''set start initial registered values for all actions (clean all application)'''
        for key, property in self.applicationPropertiesDictionary.iteritems():
            #reset all properties values if possible
            if property.resettable == True:
                property.set_to_default_value()
        #refresh view
        # self.refresh_application_view()

    def refresh_application_view(self):
        '''refresh all properties registered wrappers'''
        for key, property in self.applicationPropertiesDictionary.iteritems():
            if property.serializable == True:
                property.set_value(property.contentValue)

    @staticmethod
    def get_core_instance():
        return AbstractAppController.this


class CoreAppController(AbstractAppController):
    '''This class is used only for create application core logic.'''

    def __init__(self):
        super(CoreAppController, self).__init__()

    def on_create_logic(self):
        # register all actions
        self.add_action("exitApplication", self.on_application_exit)
        self.add_action("selectAllVessels", self.on_select_all_vessels)
        self.add_action("deselectAllVessels", self.on_deselect_all_vessels)
        self.add_action("inverseSelectionVessels", self.on_inverse_vessel_selection)
        self.add_action("removeSelectedVessels", self.on_remove_selected_vesels)
        self.add_action("invertPoints", self.on_invert_points)
        self.add_action("cutVessel", self.on_cut_vessel)
        self.add_action("mergeVessels", self.on_merge_vessels)
        self.add_action("detectVessel", self.on_detect_current_vessel)
        self.add_action("detectVesselAllFrames", self.on_detect_vessels_all_frames)
        self.add_action("saveProject", self.on_save_project)
        self.add_action("selectMipBackgroundToRemove", self.on_mip_remove_background)
        self.add_action("nextDatabase", self.on_next_database_3d)
        self.add_action("previousDatabase", self.on_previous_database_3d)
        self.add_action("resetZoom", self.on_reset_zoom)
        self.add_action("addHandPoint", self.on_next_hand_point)
        self.add_action("saveHandVessel", self.on_save_hand_vessel)
        self.add_action("addNewHandVessel", self.on_add_new_hand_vessel)
        self.add_action("setReferenceSegmentationIndex", self.on_set_reference_segmentation_index)

        # register all available properties
        self.add_property(False,False, "controlKeyPressed", False)
        self.add_property(True,True, "segmentationSelectedVeselPoint", None)
        self.add_property(True,True, "currentFrameIndex", 0, self.on_current_frame_index_change)
        self.add_property(True,True, "referenceFrameIndex", None)
        self.add_property(True,True, "currentFrameName", "Default")
        self.add_property(True,True, "segmentationResult", {}, self.on_segmentation_result_change) #dictionary with all segmentation results for all frames 3D
        self.add_property(True,True,  "mainMenuTabIndex", 1, self.on_main_menu_tab_index_change)
        self.add_property(False,True, "database4D", None, self.on_set_database_4d)
        self.add_property(False,False, "MipAlgorithm", None)
        self.add_property(False,False, "sliceAlgorithm", None)
        self.add_property(False,False, "segmentationAlgorithm", None)
        self.add_property(False,False, "vesselCreator", None)
        self.add_property(True,True, "lastClickedImageNumber", -1)
        self.add_property(True,True, "dataSourceType", 0) # dicom = 1, Raw = 2, empty = 0
        self.add_property(True,True, "dicomDataFilePth", None, self.on_load_dicom)
        self.add_property(True,True, "rawDataFilesPth", [], self.on_load_multiple_raw)
        self.add_property(False,True, "saveAsProjectFilePath",None, self.on_save_as_project)
        self.add_property(False,True, "exportBinarySegmentationFilePath", None, self.on_export_binary_segmentation)
        self.add_property(False,True, "exportTextSegmentationFilePath", None, self.on_export_text_segmentation)
        self.add_property(False,True, "loadProjectFilePath",None, self.on_load_project)
        self.add_property(True,True, "sliceMousePointerCurrentValue", 0)
        self.add_property(True,True, "2D3D_HU_Value", 0, self.on_hu_cos_tam_change)
        self.add_property(True,True, "imageSeedPoint", [1, 1, 1], self.on_image_sed_point_change) # point on slice and the same point on mip (seed for segmetation)
        self.add_property(True,True, "mipSelectionPointType", True)
        self.add_property(True,True, "zoomValue", 1.0, self.on_zoomValue_change) # zoom, scroll bar x, scrollbar y
        self.add_property(True,True, "mipRemoveBackground", None)  #default no remove
        self.add_property(True,True, "database3DframesNamesList", [])  #default empty names list
        self.add_property(True,False, "displayResultPoints", False)
        self.add_property(True,False, "displayVisitedVoxels", True)
        self.add_property(True,True, "vesselName", "")
        self.add_property(True,False, "showCalcifs", False, self.on_show_calcifs)
        self.add_property(True,True, "activeSegmentationVessel", None, self.on_activeSegmentationVessel_change)
        self.add_property(True,False, "displayPrevSegmentation", True)
        self.add_property(True,False, "displayNextSegmentation", True)
        self.add_property(True,False, "displayReferenceSegmentation", True)
        self.add_property(False,False, "application", None)
        self.add_property(True,True, "currentVesselPointIndex", 0)

        #register current vessel point properties
        self.add_property(True,True, "currentVesselPointX", "0")
        self.add_property(True,True, "currentVesselPointY", "0")
        self.add_property(True,True, "currentVesselPointZ", "0")
        self.add_property(True,True, "currentVesselPointAlpha", "0")
        self.add_property(True,True, "currentVesselPointBeta", "0")
        self.add_property(True,True, "currentVesselPointRadius", "0")
        self.add_property(True,True, "currentVesselPointBGLevel", "0")
        self.add_property(True,True, "currentVesselPointVesselLevel", "0")

        #register segmentation params properties
        self.add_property(True,False, "adapting_vessel_level", True)
        self.add_property(True,False, "adapting_bg_level", True)
        self.add_property(True,False, "calcification_removing", True)
        self.add_property(True,False, "adapting_window_size", True)
        self.add_property(True,False, "initial_bg_level", 1100)
        self.add_property(True,False, "max_vessel_level", 1500)
        self.add_property(True,False, "initial_mask_size", 3)
        self.add_property(True,False, "initial_vessel_level", 1300)
        self.add_property(True,False, "min_vessel_level", 1125)
        self.add_property(True,False, "step_factor", 0.25)

        #settings properties
        self.add_property(True,False, "wl_level", 0)
        self.add_property(True,False, "wh_level", 4095)

        #view settings properties
        self.add_property(True,True, "SegmentationPrevVoxelsValue", 25000)
        self.add_property(True,True, "SegmentationPrevPointsValue", 40000)
        self.add_property(True,True, "SegmentationNextVoxelsValue", 2500)
        self.add_property(True,True, "SegmentationNextPointsValue", 60000)
        self.add_property(True,True, "SegmentationReferenceVoxelsValue", 50000)
        self.add_property(True,True, "SegmentationReferencePointsValue", 30000)
        self.add_property(True,True, "SegmentationSelectedVoxelsValue", 3000)
        self.add_property(True,True, "SegmentationVoxelsValue", 8000)
        self.add_property(True,True, "SegmentationPointsValue", 50000)
        self.add_property(True,True, "SegmentationCalcificationsValue", 30000)
        self.add_property(True,True, "SlicePrevVoxelsValue", 24000)
        self.add_property(True,True, "SliceNextVoxelsValue", 26000)
        self.add_property(True,True, "SliceReferenceVoxelsValue", 54000)
        self.add_property(True,True, "SliceVoxelsValue", 0)

        #default values for RAW loading
        self.add_property(True,False, "x_size", 200)
        self.add_property(True,False, "y_size", 250)
        self.add_property(True,False, "z_size", 300)
        self.add_property(True,False, "bits_per_voxel", "16")

    # Properties section. Warning, do not set newValue variable to old value from property variable.

    def on_zoomValue_change(self, property, newValue):
        '''for debuging urposes'''
        print newValue

    def on_activeSegmentationVessel_change(self, property, newValue):
        if property.contentValue == newValue:
            return False
        result3D = self.get_current_result()
        if result3D is None: return False
        if self.get_property_value("controlKeyPressed") is False:
            for key, value in result3D.vesselTree.iteritems():#clear all selections
                value.isSelected = False
        #select Vessel
        if newValue != None:
            newValue.isSelected = True
        return True

    def on_segmentation_result_change(self, property, newValue):
        activeFrameIndex = self.get_property_value("currentFrameIndex")
        result4D = self.get_property_value("segmentationResult")
        self.get_property_value("vesselCreator").set_current_result_4D(result4D, activeFrameIndex)
        return True

    def on_set_database_4d(self, property, newValue):
        if newValue is not None:
            self.set_property_value("database3DframesNamesList", newValue.frames3DnamesList)
            if newValue.frames3DnamesList[0] != "":
                self.set_property_value("currentFrameName", newValue.frames3DnamesList[0])
                mainWindow = self.get_property_value("application").mainWindow
                mainWindow.setWindowTitle("K4D - " + newValue.frames3DnamesList[0])
        return True

    def on_current_frame_index_change(self, property, newValue):
        # curIndex = self.get_property_value("currentFrameIndex")
        # names = self.get_property_value("database3DframesNamesList")
        # if names:
        #     self.set_property_value("currentFrameName", names[curIndex])
        return True

    def on_save_as_project(self, property, newValue):
        self.exoprt_properties(newValue)
        return True

    def on_export_binary_segmentation(self, property, newValue):
        '''Method exports segmentation result in binary format'''
        database = self.get_property_value("database4D")
        if database is None:
            return
        shape = (database.get_z_size(), database.get_y_size(), database.get_x_size())
        dbToExport = np.zeros(shape, dtype=np.uint8)
        result = self.get_property_value("segmentationResult")
        currentFrame3DResult = self.get_current_result()
        if currentFrame3DResult is None:
            return
        for key, voxel in currentFrame3DResult.voxelVisitedData.iteritems():
            dbToExport[key[0],key[1],key[2]] = 100
        for key, calcif in currentFrame3DResult.calcification_voxels_map.iteritems():
            dbToExport[key[0],key[1],key[2]] = 200
        dbToExport.tofile(newValue)

    def on_export_text_segmentation(self, property, newValue):
        '''Method exports segmentation result in text format'''
        result = self.get_property_value("segmentationResult")
        currentFrame3DResult = self.get_current_result()
        if currentFrame3DResult is None:
            return
        fo = open(newValue, "w+")
        for key, vesselBranch in currentFrame3DResult.vesselTree.iteritems():
            for point in vesselBranch.pointList:
                parentId = "-1"
                if vesselBranch.parent: parentId = str(vesselBranch.parent.vesselId)
                pointString = "Point - VesselID: " + str(vesselBranch.vesselId) + " ParentID: " + parentId + " PointID: " + str(point.pointId) + " X: " + str(point.x) + " Y: " + str(point.y) + \
                              " Z: " + str(point.z) + " Radius: " + str(point.radius) + " Alpha: " + str(point.alpha) + " Beta: " + str(point.beta) + " VLvl: " + str(point.vessel_level) +\
                              " BGLvl: " + str(point.bg_level) + "\n"
                fo.write(pointString)

        for key, calcif in currentFrame3DResult.calcification_voxels_map.iteritems():
            calcifString = "Calcif - X: " + str(key[2]) + " Y: " + str(key[1]) + " Z: " + str(key[0]) + "\n"
            fo.write(calcifString)
        fo.close()

    def on_load_project(self, property, newValue):
        self.set_default_property_values()
        self.enable_algorithms(False)
        self.import_properties(newValue)
        self.get_property("saveAsProjectFilePath").contentValue = newValue # set value for property without update
        # set base name on the title bar
        curIndex = self.get_property_value("currentFrameIndex")
        names = self.get_property_value("database3DframesNamesList")
        mainWindow = self.get_property_value("application").mainWindow
        mainWindow.setWindowTitle("K4D - " + names[curIndex])
        self.enable_algorithms(True)
        self.get_property_value("segmentationAlgorithm").repaint()
        return True

    def on_load_multiple_raw(self, property, newValue):
        rawFilePath = newValue
        if rawFilePath is not None and rawFilePath:
            self.set_default_property_values()
            self.enable_algorithms(False)
            params = {}
            params["x_size"] = int(self.get_property_value("x_size"))
            params["y_size"] = int(self.get_property_value("y_size"))
            params["z_size"] = int(self.get_property_value("z_size"))
            params["bits_per_voxel"] = int(self.get_property_value("bits_per_voxel"))
            self.set_property_value("database4D", RAW_4D(rawFilePath, 1, params))
            self.set_property_value("currentFrameIndex", 0)
            self.get_property("dataSourceType").contentValue = 2 # Raw id
            self.enable_algorithms(True)
            self.get_property_value("segmentationAlgorithm").update()
        return True

    def on_image_sed_point_change(self, property, newValue):
        db4d = self.get_property_value("database4D")
        if db4d is not None:
            db3d = db4d.get_frame(self.get_property_value("currentFrameIndex"))
            pointVal = db3d[newValue[0], newValue[1], newValue[2]]
            self.set_property_value("sliceMousePointerCurrentValue", pointVal)
            return True
        return False

    def on_show_calcifs(self, property, newValue):
        pass

    def on_main_menu_tab_index_change(self, property, newValue):
        if newValue == 2: # if tab is: segmentation result, then additionally check if need to
            pass

    def on_load_dicom(self, property, newValue):
        if newValue is None: return False
        if newValue != "":
            self.set_default_property_values()
            self.enable_algorithms(False)
            database = DICOM_4D(newValue, 1)
            self.set_property_value("database4D", database) #load DICOM database
            self.set_property_value("currentFrameIndex", 0)
            self.get_property("dataSourceType").contentValue = 1 # dicom id
            self.enable_algorithms(True)
            self.get_property_value("segmentationAlgorithm").update()
            return True    # true - confirm change, false - cancel change
        else: return False # cancel file path change

    def on_hu_cos_tam_change(self, property, newValue):
        #add yours code here
        return True

    # actions section

    def on_next_hand_point(self, params):
        point = self.get_property_value("imageSeedPoint")
        if point is not None and point:
            self.get_property_value("vesselCreator").add_point(point[0], point[1], point[2])
        pass

    def on_add_new_hand_vessel(self, params):
        vesselCreator = self.get_property_value("vesselCreator")
        vesselCreator.create_new_vessel()
        self.set_property_value("vesselCreator", vesselCreator)  # for update
        pass

    def on_save_hand_vessel(self, params):
        self.get_property_value("vesselCreator").save_actual_vessel()
        pass

    def on_cut_vessel(self, params):
        result = self.get_property_value("segmentationResult")
        currentFrame3DResult = self.get_current_result()
        if currentFrame3DResult is None:
            return
        if self.get_property_value("segmentationSelectedVeselPoint") is not None:
            vesselToCut = self.get_property_value("activeSegmentationVessel")
            pointIndexToCut = vesselToCut.activePointIndex
            # cut vessels objects
            currentFrame3DResult.cut_vessel_by_point(vesselToCut.get_active_point())
            self.set_property_value("segmentationResult", result)
            #reset current selected point
            self.set_property_value("segmentationSelectedVeselPoint", None)

    def on_merge_vessels(self, params):
        result = self.get_property_value("segmentationResult")
        currentFrame3DResult = self.get_current_result()
        if currentFrame3DResult is not None: # select all vessels
            sellectedVesselsKeys = []
            for id, vessel in currentFrame3DResult.vesselTree.iteritems():
                if vessel.isSelected :
                    sellectedVesselsKeys.append(id)
            if len(sellectedVesselsKeys) < 2 :return
            # merge vessels objects
            currentFrame3DResult.merge_vessels_by_id(sellectedVesselsKeys) # list of vessel ids to merge
            # self.set_property_value("segmentationResult", result)
            #reset current selected pointonly for update
            self.set_property_value("segmentationSelectedVeselPoint", None)

    def on_invert_points(self, params):
        result = self.get_property_value("segmentationResult")
        currentFrame3DResult = self.get_current_result()
        if currentFrame3DResult is not None: # select all vessels
            selectedVesselsKeys = []
            for id, vessel in currentFrame3DResult.vesselTree.iteritems():
                if vessel.isSelected :
                    vessel.invert_points()

    def on_detect_current_vessel(self, params):
        """segmentation for current frame 3D start"""
        if self.get_property_value("database4D") is not None:
            self.get_property_value("segmentationAlgorithm").algorithm.invokeProcessing() # run algorithm
            self.get_property_value("sliceAlgorithm").algorithm.invokeProcessing() # run algorithm
            self.set_property_value("mainMenuTabIndex", 2)# go to segmentaton panel after processing

    def on_detect_vessels_all_frames(self, params):
        """segmentation for all frames 3D start"""
        # set properties earlier
        self.get_property_value("segmentationAlgorithm").invokeProcessing() # run algorithm
        # could go to segmentaton panel if needed, for now is not doing anything after processing

    def on_select_all_vessels(self, params):
        self.set_selected_all_vessels(True)

    def on_deselect_all_vessels(self, params):
        self.set_selected_all_vessels(False)

    def set_selected_all_vessels(self, isSelected):
        result = self.get_property_value("segmentationResult")
        currentFrameIndex = self.get_property_value("currentFrameIndex")
        if result is None or currentFrameIndex is None : return
        currentFrame3DResult = result[currentFrameIndex]
        if currentFrame3DResult is not None: # select all vessels
            vesselDict = currentFrame3DResult.vesselTree
            for id, vessel in vesselDict.iteritems():
                vessel.isSelected = isSelected
        self.set_property_value("segmentationResult", result)

    def on_inverse_vessel_selection(self, params):
        result = self.get_property_value("segmentationResult")
        currentFrameIndex = self.get_property_value("currentFrameIndex")
        if result is None or currentFrameIndex is None : return
        currentFrame3DResult = result[currentFrameIndex]
        if currentFrame3DResult is not None: # select all vessels
            vellelDict = currentFrame3DResult.vesselTree
            for id, vessel in vellelDict.iteritems():
                vessel.isSelected = not vessel.isSelected
        self.set_property_value("segmentationResult", result)

    def on_remove_selected_vesels(self, params):
        result = self.get_property_value("segmentationResult")
        currentFrame3DResult = self.get_current_result()
        if currentFrame3DResult is not None: # select all vessels
            sellectedVesselsKeys = []
            for id, vessel in currentFrame3DResult.vesselTree.iteritems():
                if vessel.isSelected :
                    sellectedVesselsKeys.append(id)
            # remove vessel objects
            for key in sellectedVesselsKeys:
                 currentFrame3DResult.remove_vessel_by_id(key)
            if sellectedVesselsKeys:
                self.set_property_value("segmentationResult", result)
                #reset current selected point
                self.set_property_value("segmentationSelectedVeselPoint", None)
                self.set_property_value("vesselName", "")

    def on_mip_remove_background(self, params):
        selectedValue = self.get_property_value("sliceMousePointerCurrentValue")
        if selectedValue is not None and selectedValue !=0:
            self.set_property_value("mipRemoveBackground", selectedValue)
        pass

    def on_previous_database_3d(self, params):
        database = self.get_property_value("database4D")
        if database is not None:
            maxFrame = database.get_frames_3d_count()
            curIndex = self.get_property_value("currentFrameIndex")
            curIndex -= 1
            if curIndex < 0:
                curIndex = 0
            if curIndex >= maxFrame:
                curIndex = maxFrame - 1
            self.set_property_value("currentFrameIndex", curIndex)
            names = self.get_property_value("database3DframesNamesList")
            mainWindow = self.get_property_value("application").mainWindow
            if names and names[curIndex] != "":
                self.set_property_value("currentFrameName", names[curIndex])
                mainWindow.setWindowTitle("K4D - " + names[curIndex])

    def on_next_database_3d(self, params):
        database = self.get_property_value("database4D")
        if database is not None:
            maxFrame = database.get_frames_3d_count()
            curIndex = self.get_property_value("currentFrameIndex")
            curIndex += 1
            if curIndex < 0:
                curIndex = 0
            if curIndex >= maxFrame:
                curIndex = maxFrame - 1
            self.set_property_value("currentFrameIndex", curIndex)
            names = self.get_property_value("database3DframesNamesList")
            mainWindow = self.get_property_value("application").mainWindow
            if names and names[curIndex] != "":
                self.set_property_value("currentFrameName", names[curIndex])
                mainWindow.setWindowTitle("K4D - " + names[curIndex])

    def on_application_exit(self, params):
        print 'exit '
        pass

    def on_save_project(self, params):
        filePath = self.get_property_value("saveAsProjectFilePath")
        if filePath is not None:
            self.exoprt_properties(filePath)
        pass

    def on_set_reference_segmentation_index(self, params):
        curIndex = self.get_property_value("currentFrameIndex")
        self.set_property_value("referenceFrameIndex", curIndex)

    def on_reset_zoom(self, params):
        self.set_property_value("zoomValue", 1.0)

    # utils section
    def get_current_result(self):
        '''Get current selected segmentation result'''
        result = self.get_property_value("segmentationResult")
        currentFrameIndex = self.get_property_value("currentFrameIndex")
        if result is None or currentFrameIndex is None or len(result)==0: return None
        return result[currentFrameIndex]

    def enable_algorithms(self, isEnabled):
        self.get_property_value("MipAlgorithm").algorithm.set_algorithm_enabled(isEnabled)
        self.get_property_value("sliceAlgorithm").algorithm.set_algorithm_enabled(isEnabled)
        self.get_property_value("segmentationAlgorithm").algorithm.set_algorithm_enabled(isEnabled)
        if isEnabled == True:
            self.get_property_value("MipAlgorithm").algorithm.invokeProcessing()
            self.get_property_value("sliceAlgorithm").algorithm.invokeProcessing()
            self.get_property_value("segmentationAlgorithm").algorithm.refresh_view()




class AbstractEventController(object):
    '''Base class for all application events'''

    def __init__(self, name):
        super(AbstractEventController, self).__init__()
        self.name = name
        self.binderList = []
        self.eventListener = None
        self.updateUIEventListener = None

    def register_binder(self, binder, allPropertiesDict):
        binder.add_parent_controller(self, allPropertiesDict)
        self.binderList.append(binder)

    def set_event_listener(self, eventListener):
        self.eventListener = eventListener

    def set_UI_update_listener(self, UIListener):
        self.updateUIEventListener = UIListener

    def get_UI_update_listener(self):
        return self.updateUIEventListener

    def get_binder_list(self):
        return self.binderList


class ActionController(AbstractEventController):
    ''' Class - implementation for handling actions.'''

    def __init__(self, name):
        super(ActionController, self).__init__(name)

    def invoke_action_performaed(self, params=None):
        if self.eventListener == None: raise Exception("Action not binded with any method - action useless")
        self.eventListener(params) # invoke process event
        if self.updateUIEventListener != None: self.updateUIEventListener()# upodate UI if updater registered



class PropertyController(AbstractEventController):
    '''Class - implementation for handling property changes.'''

    def __init__(self, name, resettable, initialValue, serializable):
        super(PropertyController, self).__init__(name)
        self.resettable = resettable
        self.defaultValue = initialValue
        self.contentValue = initialValue
        self.serializable = serializable

    def register_binder(self, binder, allPropertiesDict):
        binder.add_parent_controller(self, allPropertiesDict)
        self.binderList.append(binder)
        binder.on_update_ui(self.contentValue, self)

        #self.set_value(self.contentValue) # update UI

    def get_value(self):
        return self.contentValue

    def set_to_default_value(self):
        self.contentValue = self.defaultValue

    def set_value(self, newValue):
        '''Invoke this method from your UI control component listener method, when you want to
         change value for this property.
        '''
        if self.eventListener is not None :
            if self.eventListener(self, newValue) is False: return# check if we can continue operation or cancel otherwise
        self.contentValue = newValue# if we can continue, change current value
        # unlink all
        for binder in self.binderList:
            binder.on_unlink(self)
        for binder in self.binderList: # update all values
            binder.on_update_ui(newValue, self)
        # update UI if needed
        if self.updateUIEventListener is not None: self.updateUIEventListener()# upodate UI if updater registered
        # link all again
        for binder in self.binderList:
            binder.on_link(self)



class ActionBinder(object):
    #__metaclass__ = ABCMeta

    def __init__(self):
        super(ActionBinder, self).__init__()
        self.defaultParentController = None # place for register parent propertyController
        self.parentDictionary = {}# dictionary for parents for this binder

    #@abstractmethod
    def on_unlink(self, parentController):
        pass

    #@abstractmethod
    def on_link(self, parentController):
        pass

    def add_parent_controller(self, parentController, allProperties):
        '''Add  parent to this binder, last added parent is default parent'''
        self.defaultParentController = parentController
        #print "property added ", parentController.name, parentController
        self.parentDictionary = allProperties

    def get_parent_controller(self, parentName=None):
        '''Returns parent controller for this binder, if binder have more than one
            parent you must specify name of parent witch one you want to get.
        '''
        if parentName is None :
           return self.defaultParentController
        else : return self.parentDictionary[parentName]




class PropertyBinder(ActionBinder):
    #__metaclass__ = ABCMeta


    def __init__(self):
        super(PropertyBinder, self).__init__()
        self.handlerDict = {}

    def on_get_value_from_UI(self, newValue, parentControllerName=None):
        if self.defaultParentController is None : raise Exception("Binder not added to any propertyController!")
        if parentControllerName is None :
            self.defaultParentController.set_value(newValue)
        else : self.get_parent_controller(parentControllerName).set_value(newValue)


    def add_property_handler(self, handler, propertyHandlerName = None):
        '''Register handler function for core property change.'''
        if propertyHandlerName == None:
            self.handlerDict["default"] = handler
        else:
            self.handlerDict[propertyHandlerName] = handler
        pass

    def on_update_ui(self, newValueForUI, parentController):
        '''Update wrapper ui for given parent property'''
        if len(self.handlerDict) == 1: #default handling: without name checking
            handlerFunction = self.handlerDict.items()[0][1]
            handlerFunction(newValueForUI, parentController)
        elif parentController.name in self.handlerDict:
            handlerFunction = self.handlerDict[parentController.name]
            handlerFunction(newValueForUI, parentController)
        pass