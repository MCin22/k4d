import os
import re

from DataController import PropertyBinder


__author__ = 'MCin22'

from PySide.QtCore import QPoint
from PySide.QtGui import QDialog, QStandardItem
from UIWrappers import ActionButton, PropertySpinBox, PropertyQComboBox, PropertyMultiRAWQTree

# -*- coding: utf-8 -*-

from PySide import QtCore, QtGui

class Ui_DialogMultipleRAWLoader(QDialog, PropertyBinder):


    def __init__(self, parent = None):
        super(Ui_DialogMultipleRAWLoader, self).__init__(parent)

        self.returnRawList = []

        self.resize(370, 279)
        self.setWindowTitle("Load RAW files...")

        self.gridLayout = QtGui.QGridLayout(self)
        self.horizontalLayout = QtGui.QHBoxLayout()

        self.OKButton = ActionButton(self)
        # parent.add_action_binder("loadMultipleRawDialogOk", self.OKButton)
        self.OKButton.setText("OK")
        self.horizontalLayout.addWidget(self.OKButton)
        self.OKButton.clicked.connect(self.accept)
        #self.OKButton.clicked.connect(self.on_OK_button_pressed)

        self.cancelButton = QtGui.QPushButton(self)
        self.cancelButton.setText("Cancel")
        self.horizontalLayout.addWidget(self.cancelButton)
        self.cancelButton.clicked.connect(self.reject)

        self.gridLayout.addLayout(self.horizontalLayout, 1, 0, 1, 1)
        self.formLayout = QtGui.QFormLayout()
        self.formLayout.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)

        self.fileNameLabel = QtGui.QLabel(self)
        self.fileNameLabel.setText("File name:")
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.fileNameLabel)

        self.currentFileNamesBox = PropertyMultiRAWQTree(self)
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.currentFileNamesBox)
        # parent.add_property_binder("rawDataFilesPth", self.currentFileNamesBox)

        self.parseTextField = QtGui.QLineEdit(self)
        self.parseTextField.setText("w*[_-]([0-9]+)*[_-]([0-9]+)*[_-]([0-9]+).*$")
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.parseTextField)

        self.parseTextButton = ActionButton(self)
        self.parseTextButton.setText("Parse field to fill data")
        self.parseTextButton.clicked.connect(self.parseText)
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.parseTextButton)

        self.xSizeLabel = QtGui.QLabel(self)
        self.xSizeLabel.setText("X size:")
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.xSizeLabel)

        self.xSizeField = PropertySpinBox(self)
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.xSizeField)
        parent.add_property_binder("x_size", self.xSizeField)

        self.ySizeLabel = QtGui.QLabel(self)
        self.ySizeLabel.setText("Y size:")
        self.formLayout.setWidget(3, QtGui.QFormLayout.LabelRole, self.ySizeLabel)

        self.ySizeField = PropertySpinBox(self)
        self.formLayout.setWidget(3, QtGui.QFormLayout.FieldRole, self.ySizeField)
        parent.add_property_binder("y_size", self.ySizeField)

        self.zSizeLabel = QtGui.QLabel(self)
        self.zSizeLabel.setText("Z size:")
        self.formLayout.setWidget(4, QtGui.QFormLayout.LabelRole, self.zSizeLabel)

        self.zSizeField = PropertySpinBox(self)
        self.formLayout.setWidget(4, QtGui.QFormLayout.FieldRole, self.zSizeField)
        parent.add_property_binder("z_size", self.zSizeField)

        self.anisoRatioLabel = QtGui.QLabel(self)
        self.anisoRatioLabel.setText("Aniso ratio:")
        self.formLayout.setWidget(5, QtGui.QFormLayout.LabelRole, self.anisoRatioLabel)

        self.anisoRatioField = QtGui.QLineEdit(self)
        self.anisoRatioField.setEnabled(False)
        self.anisoRatioField.setText("1")
        self.formLayout.setWidget(5, QtGui.QFormLayout.FieldRole, self.anisoRatioField)

        self.bitsPerVoxelLabel = QtGui.QLabel(self)
        self.bitsPerVoxelLabel.setText("Bits per voxel:")
        self.formLayout.setWidget(6, QtGui.QFormLayout.LabelRole, self.bitsPerVoxelLabel)

        self.bitsPerVoxelComboBox = PropertyQComboBox(self)
        self.bitsPerVoxelComboBox.addItem("8")
        self.bitsPerVoxelComboBox.addItem("16")
        self.formLayout.setWidget(6, QtGui.QFormLayout.FieldRole, self.bitsPerVoxelComboBox)
        self.gridLayout.addLayout(self.formLayout, 0, 0, 1, 1)
        parent.add_property_binder("bits_per_voxel", self.bitsPerVoxelComboBox)

        QtCore.QMetaObject.connectSlotsByName(self)
        self.setModal(True)

    def parseText(self):
        fullFileName = ""
        if self.currentFileNamesBox.selectedIndexes():
            fullFileName = self.currentFileNamesBox.selectedIndexes()[0].data()
        else:
            fullFileName = self.currentFileNamesBox.indexAt(QPoint(0, 0)).data()

        fileName = os.path.basename(fullFileName)
        #fileName = os.path.splitext(fileName)[0]

        output = re.search(self.parseTextField.text(), fileName)

        if output is not None:
            self.xSizeField.setValue(int(output.group(1)))
            self.ySizeField.setValue(int(output.group(2)))
            self.zSizeField.setValue(int(output.group(3)))
        pass

    # def setParams(self, params):
    #     pass
    #
    #
    # def on_OK_button_pressed(self):
    #     self.on_get_value_from_UI(self.getUpdatedParams())

    # def on_set_value_to_UI(self, newValueForUI, propertyController):
    #     # self.setParams(newValueForUI)
    #     pass

    def get_raw_files_list(self):
        return self.returnRawList

    def set_file_names(self, namesList):
        self.returnRawList = namesList

        self.currentFileNamesBox.model.clear()
        if not self.returnRawList:
            return
        else:
            for filename in self.returnRawList:
                item = QStandardItem(filename)
                item.setData(filename)
                self.currentFileNamesBox.model.appendRow(item)
